using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Hawa
{
	/// <summary>
	/// Summary description for rptHeader.
	/// </summary>
	public class rptHeader : DataDynamics.ActiveReports.ActiveReport3
	{
		private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.Picture Logo;
        private DataDynamics.ActiveReports.Label lblLogo;
        private DataDynamics.ActiveReports.Label lbHeadHawa;
        private DataDynamics.ActiveReports.Label lbHeadReportname;
        private DataDynamics.ActiveReports.PageFooter PageFooter;

        public Alpha.Page page; // to get the strings
        public string langId = "de";
        public string reportName = "bezugsquellen";

		public rptHeader()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			}
			base.Dispose( disposing );
		}

		#region Report Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.Logo = new DataDynamics.ActiveReports.Picture();
            this.lblLogo = new DataDynamics.ActiveReports.Label();
            this.lbHeadHawa = new DataDynamics.ActiveReports.Label();
            this.lbHeadReportname = new DataDynamics.ActiveReports.Label();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHeadHawa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHeadReportname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 2F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Logo,
            this.lblLogo,
            this.lbHeadHawa,
            this.lbHeadReportname});
            this.PageHeader.Height = 3.229167F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Logo
            // 
            this.Logo.Border.BottomColor = System.Drawing.Color.Black;
            this.Logo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Logo.Border.LeftColor = System.Drawing.Color.Black;
            this.Logo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Logo.Border.RightColor = System.Drawing.Color.Black;
            this.Logo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Logo.Border.TopColor = System.Drawing.Color.Black;
            this.Logo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Logo.Height = 0.75F;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.ImageData = ((System.IO.Stream)(resources.GetObject("Logo.ImageData")));
            this.Logo.Left = 5.8125F;
            this.Logo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Logo.LineWeight = 0F;
            this.Logo.Name = "Logo";
            this.Logo.Top = 0F;
            this.Logo.Width = 1.625F;
            // 
            // lblLogo
            // 
            this.lblLogo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblLogo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblLogo.Border.RightColor = System.Drawing.Color.Black;
            this.lblLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblLogo.Border.TopColor = System.Drawing.Color.Black;
            this.lblLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblLogo.Height = 0.9375F;
            this.lblLogo.HyperLink = null;
            this.lblLogo.Left = 5.5F;
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Style = "ddo-char-set: 1; font-size: 8pt; font-family: Arial; ";
            this.lblLogo.Text = "Hawa";
            this.lblLogo.Top = 0.75F;
            this.lblLogo.Width = 1.890256F;
            // 
            // lbHeadHawa
            // 
            this.lbHeadHawa.Border.BottomColor = System.Drawing.Color.Black;
            this.lbHeadHawa.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadHawa.Border.LeftColor = System.Drawing.Color.Black;
            this.lbHeadHawa.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadHawa.Border.RightColor = System.Drawing.Color.Black;
            this.lbHeadHawa.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadHawa.Border.TopColor = System.Drawing.Color.Black;
            this.lbHeadHawa.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadHawa.Height = 0.2411418F;
            this.lbHeadHawa.HyperLink = null;
            this.lbHeadHawa.Left = 0F;
            this.lbHeadHawa.Name = "lbHeadHawa";
            this.lbHeadHawa.Style = "ddo-char-set: 1; font-weight: bold; font-size: 16pt; ";
            this.lbHeadHawa.Text = "lbHeadHawa";
            this.lbHeadHawa.Top = 1.989583F;
            this.lbHeadHawa.Width = 4.1875F;
            // 
            // lbHeadReportname
            // 
            this.lbHeadReportname.Border.BottomColor = System.Drawing.Color.Black;
            this.lbHeadReportname.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadReportname.Border.LeftColor = System.Drawing.Color.Black;
            this.lbHeadReportname.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadReportname.Border.RightColor = System.Drawing.Color.Black;
            this.lbHeadReportname.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadReportname.Border.TopColor = System.Drawing.Color.Black;
            this.lbHeadReportname.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbHeadReportname.Height = 0.1875F;
            this.lbHeadReportname.HyperLink = null;
            this.lbHeadReportname.Left = 4.8125F;
            this.lbHeadReportname.Name = "lbHeadReportname";
            this.lbHeadReportname.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; ";
            this.lbHeadReportname.Text = "lbHeadReportname";
            this.lbHeadReportname.Top = 2.01419F;
            this.lbHeadReportname.Width = 2.4375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0.25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.489583F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHeadHawa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHeadReportname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
		#endregion

        private void PageHeader_Format(object sender, EventArgs e)
        {

        }
	}
}
