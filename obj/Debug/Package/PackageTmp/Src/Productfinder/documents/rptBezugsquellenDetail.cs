using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Web;
using System.Configuration;

namespace Hawa
{
	/// <summary>
	/// Summary description for rptBezugsquellenDetail.
	/// </summary>
	public class rptBezugsquellenDetail : DataDynamics.ActiveReports.ActiveReport3
	{
		private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.SubReport SubReport1;
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.PageBreak PageBreak;
        private DataDynamics.ActiveReports.TextBox lblTitle;
        private DataDynamics.ActiveReports.Picture Picture;
        private DataDynamics.ActiveReports.PageFooter PageFooter;
        private DataDynamics.ActiveReports.TextBox Footer;
        private DataDynamics.ActiveReports.TextBox TextBox;
        private DataDynamics.ActiveReports.Label lblSeite;
        private DataDynamics.ActiveReports.TextBox Seite;
        private DataDynamics.ActiveReports.GroupHeader GroupHeader2;
        private DataDynamics.ActiveReports.Label lblFirma;
        private DataDynamics.ActiveReports.Label lblStrasse;
        private DataDynamics.ActiveReports.Label lblPLZ;
        private DataDynamics.ActiveReports.Label lblCountry;
        private DataDynamics.ActiveReports.Label lblPhone;
        private DataDynamics.ActiveReports.Label lblFax;
        private DataDynamics.ActiveReports.Label lblWebseite;
        private DataDynamics.ActiveReports.Line Line;
        private DataDynamics.ActiveReports.GroupFooter GroupFooter2;
        private DataDynamics.ActiveReports.GroupHeader GroupHeader1;
        private DataDynamics.ActiveReports.TextBox TextBox1;
        private DataDynamics.ActiveReports.TextBox TextBox2;
        private DataDynamics.ActiveReports.TextBox TextBox3;
        private DataDynamics.ActiveReports.TextBox TextBox4;
        private DataDynamics.ActiveReports.TextBox TextBox5;
        private DataDynamics.ActiveReports.TextBox TextBox6;
        private DataDynamics.ActiveReports.TextBox TextBox7;
        private DataDynamics.ActiveReports.TextBox txt_sosID;
        private DataDynamics.ActiveReports.SubReport SubReport;
        private DataDynamics.ActiveReports.Line Line2;
        private DataDynamics.ActiveReports.GroupFooter GroupFooter1;
        private DataDynamics.ActiveReports.GroupHeader GroupHeader3;
        private DataDynamics.ActiveReports.Label lblSystem;
        private DataDynamics.ActiveReports.Label lblKurzbeschreibung;
        private DataDynamics.ActiveReports.Line Line1;
        private DataDynamics.ActiveReports.GroupFooter GroupFooter3;


        public Alpha.Page page; // to get the strings
        public string langId = "de";
        public string reportName = "bezugsquellenDetail";
        int sosID = Convert.ToInt32(HttpContext.Current.Session["SOS_ID"]);



		public rptBezugsquellenDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            DataBind();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			}
			base.Dispose( disposing );
		}

		#region Report Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBezugsquellenDetail));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.SubReport1 = new DataDynamics.ActiveReports.SubReport();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageBreak = new DataDynamics.ActiveReports.PageBreak();
            this.lblTitle = new DataDynamics.ActiveReports.TextBox();
            this.Picture = new DataDynamics.ActiveReports.Picture();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.Footer = new DataDynamics.ActiveReports.TextBox();
            this.TextBox = new DataDynamics.ActiveReports.TextBox();
            this.lblSeite = new DataDynamics.ActiveReports.Label();
            this.Seite = new DataDynamics.ActiveReports.TextBox();
            this.GroupHeader2 = new DataDynamics.ActiveReports.GroupHeader();
            this.lblFirma = new DataDynamics.ActiveReports.Label();
            this.lblStrasse = new DataDynamics.ActiveReports.Label();
            this.lblPLZ = new DataDynamics.ActiveReports.Label();
            this.lblCountry = new DataDynamics.ActiveReports.Label();
            this.lblPhone = new DataDynamics.ActiveReports.Label();
            this.lblFax = new DataDynamics.ActiveReports.Label();
            this.lblWebseite = new DataDynamics.ActiveReports.Label();
            this.Line = new DataDynamics.ActiveReports.Line();
            this.GroupFooter2 = new DataDynamics.ActiveReports.GroupFooter();
            this.GroupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.TextBox1 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox2 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox3 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox4 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox5 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox6 = new DataDynamics.ActiveReports.TextBox();
            this.TextBox7 = new DataDynamics.ActiveReports.TextBox();
            this.txt_sosID = new DataDynamics.ActiveReports.TextBox();
            this.SubReport = new DataDynamics.ActiveReports.SubReport();
            this.Line2 = new DataDynamics.ActiveReports.Line();
            this.GroupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            this.GroupHeader3 = new DataDynamics.ActiveReports.GroupHeader();
            this.lblSystem = new DataDynamics.ActiveReports.Label();
            this.lblKurzbeschreibung = new DataDynamics.ActiveReports.Label();
            this.Line1 = new DataDynamics.ActiveReports.Line();
            this.GroupFooter3 = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStrasse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWebseite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sosID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKurzbeschreibung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.SubReport1});
            this.Detail.Height = 0.2708333F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // SubReport1
            // 
            this.SubReport1.Border.BottomColor = System.Drawing.Color.Black;
            this.SubReport1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport1.Border.LeftColor = System.Drawing.Color.Black;
            this.SubReport1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport1.Border.RightColor = System.Drawing.Color.Black;
            this.SubReport1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport1.Border.TopColor = System.Drawing.Color.Black;
            this.SubReport1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1968504F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.04921259F;
            this.SubReport1.Width = 12.8937F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.PageBreak,
            this.lblTitle,
            this.Picture});
            this.PageHeader.Height = 1.3125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // PageBreak
            // 
            this.PageBreak.Border.BottomColor = System.Drawing.Color.Black;
            this.PageBreak.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.LeftColor = System.Drawing.Color.Black;
            this.PageBreak.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.RightColor = System.Drawing.Color.Black;
            this.PageBreak.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.TopColor = System.Drawing.Color.Black;
            this.PageBreak.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Height = 0.05555556F;
            this.PageBreak.Left = 0F;
            this.PageBreak.Name = "PageBreak";
            this.PageBreak.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
            this.PageBreak.Top = 1.119444F;
            this.PageBreak.Width = 6.5F;
            // 
            // lblTitle
            // 
            this.lblTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Height = 0.325F;
            this.lblTitle.Left = 0F;
            this.lblTitle.MultiLine = false;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "color: Black; ddo-char-set: 1; font-weight: bold; font-size: 18pt; font-family: A" +
                "rial; white-space: nowrap; ";
            this.lblTitle.Text = "Hawa-Bezugsquellen";
            this.lblTitle.Top = 0.4183071F;
            this.lblTitle.Width = 9.055119F;
            // 
            // Picture
            // 
            this.Picture.BackColor = System.Drawing.Color.White;
            this.Picture.Border.BottomColor = System.Drawing.Color.Black;
            this.Picture.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.LeftColor = System.Drawing.Color.Black;
            this.Picture.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.RightColor = System.Drawing.Color.Black;
            this.Picture.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.TopColor = System.Drawing.Color.Black;
            this.Picture.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Height = 0.6456693F;
            this.Picture.Image = ((System.Drawing.Image)(resources.GetObject("Picture.Image")));
            this.Picture.ImageData = ((System.IO.Stream)(resources.GetObject("Picture.ImageData")));
            this.Picture.Left = 11.375F;
            this.Picture.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture.LineStyle = DataDynamics.ActiveReports.LineStyle.Solid;
            this.Picture.LineWeight = 0F;
            this.Picture.Name = "Picture";
            this.Picture.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.Picture.Top = 0F;
            this.Picture.Width = 1.574803F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Footer,
            this.TextBox,
            this.lblSeite,
            this.Seite});
            this.PageFooter.Height = 0.6875F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // Footer
            // 
            this.Footer.Border.BottomColor = System.Drawing.Color.Black;
            this.Footer.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.LeftColor = System.Drawing.Color.Black;
            this.Footer.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.RightColor = System.Drawing.Color.Black;
            this.Footer.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.TopColor = System.Drawing.Color.Black;
            this.Footer.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Height = 0.1666667F;
            this.Footer.Left = 0F;
            this.Footer.Name = "Footer";
            this.Footer.Style = "ddo-char-set: 1; font-size: 8pt; font-family: Microsoft Sans Serif; ";
            this.Footer.Text = "Hawa AG Schiebebeschlagsysteme  Sliding Hardware Systems  Ferrures coulissantes";
            this.Footer.Top = 0.3333333F;
            this.Footer.Width = 7.874014F;
            // 
            // TextBox
            // 
            this.TextBox.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Height = 0.1875F;
            this.TextBox.Left = 0F;
            this.TextBox.Name = "TextBox";
            this.TextBox.Style = "ddo-char-set: 1; font-size: 8pt; font-family: Microsoft Sans Serif; ";
            this.TextBox.Text = "Untere Fischbachstrasse 4, Postfach, CH-8932 Mettmenstetten, Telefon +41 44 767 9" +
                "1 91, Telefax +41 44 767 91 78, www.hawa.ch";
            this.TextBox.Top = 0.5F;
            this.TextBox.Width = 7.874014F;
            // 
            // lblSeite
            // 
            this.lblSeite.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSeite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSeite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.RightColor = System.Drawing.Color.Black;
            this.lblSeite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.TopColor = System.Drawing.Color.Black;
            this.lblSeite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Height = 0.188F;
            this.lblSeite.HyperLink = null;
            this.lblSeite.Left = 12.375F;
            this.lblSeite.Name = "lblSeite";
            this.lblSeite.RightToLeft = true;
            this.lblSeite.Style = "ddo-char-set: 1; font-size: 8pt; ";
            this.lblSeite.Text = "Seite";
            this.lblSeite.Top = 0.5F;
            this.lblSeite.Width = 0.3125F;
            // 
            // Seite
            // 
            this.Seite.Border.BottomColor = System.Drawing.Color.Black;
            this.Seite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.LeftColor = System.Drawing.Color.Black;
            this.Seite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.RightColor = System.Drawing.Color.Black;
            this.Seite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.TopColor = System.Drawing.Color.Black;
            this.Seite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Height = 0.188F;
            this.Seite.Left = 12.6875F;
            this.Seite.Name = "Seite";
            this.Seite.RightToLeft = true;
            this.Seite.Style = "ddo-char-set: 1; font-size: 8pt; ";
            this.Seite.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All;
            this.Seite.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount;
            this.Seite.Text = "#";
            this.Seite.Top = 0.5F;
            this.Seite.Width = 0.25F;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblFirma,
            this.lblStrasse,
            this.lblPLZ,
            this.lblCountry,
            this.lblPhone,
            this.lblFax,
            this.lblWebseite,
            this.Line});
            this.GroupHeader2.Height = 0.2638889F;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
            // 
            // lblFirma
            // 
            this.lblFirma.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFirma.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFirma.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.RightColor = System.Drawing.Color.Black;
            this.lblFirma.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.TopColor = System.Drawing.Color.Black;
            this.lblFirma.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Height = 0.2F;
            this.lblFirma.HyperLink = null;
            this.lblFirma.Left = 0F;
            this.lblFirma.Name = "lblFirma";
            this.lblFirma.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblFirma.Text = "Firma";
            this.lblFirma.Top = 0F;
            this.lblFirma.Width = 1F;
            // 
            // lblStrasse
            // 
            this.lblStrasse.Border.BottomColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.LeftColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.RightColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.TopColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Height = 0.2F;
            this.lblStrasse.HyperLink = null;
            this.lblStrasse.Left = 2.75F;
            this.lblStrasse.Name = "lblStrasse";
            this.lblStrasse.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblStrasse.Text = "Strasse";
            this.lblStrasse.Top = 0F;
            this.lblStrasse.Width = 1F;
            // 
            // lblPLZ
            // 
            this.lblPLZ.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.RightColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.TopColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Height = 0.2F;
            this.lblPLZ.HyperLink = null;
            this.lblPLZ.Left = 4.822834F;
            this.lblPLZ.Name = "lblPLZ";
            this.lblPLZ.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblPLZ.Text = "PLZ/Ort";
            this.lblPLZ.Top = 0F;
            this.lblPLZ.Width = 2.214567F;
            // 
            // lblCountry
            // 
            this.lblCountry.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCountry.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCountry.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.RightColor = System.Drawing.Color.Black;
            this.lblCountry.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.TopColor = System.Drawing.Color.Black;
            this.lblCountry.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Height = 0.2F;
            this.lblCountry.HyperLink = null;
            this.lblCountry.Left = 7.1875F;
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblCountry.Text = "Land";
            this.lblCountry.Top = 0F;
            this.lblCountry.Width = 0.8125F;
            // 
            // lblPhone
            // 
            this.lblPhone.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPhone.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPhone.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.RightColor = System.Drawing.Color.Black;
            this.lblPhone.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.TopColor = System.Drawing.Color.Black;
            this.lblPhone.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Height = 0.2F;
            this.lblPhone.HyperLink = null;
            this.lblPhone.Left = 8.4375F;
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblPhone.Text = "Telefon";
            this.lblPhone.Top = 0F;
            this.lblPhone.Width = 1.060532F;
            // 
            // lblFax
            // 
            this.lblFax.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.RightColor = System.Drawing.Color.Black;
            this.lblFax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.TopColor = System.Drawing.Color.Black;
            this.lblFax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Height = 0.2F;
            this.lblFax.HyperLink = null;
            this.lblFax.Left = 9.625F;
            this.lblFax.Name = "lblFax";
            this.lblFax.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblFax.Text = "Fax";
            this.lblFax.Top = 0F;
            this.lblFax.Width = 0.625F;
            // 
            // lblWebseite
            // 
            this.lblWebseite.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.LeftColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.RightColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.TopColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Height = 0.2F;
            this.lblWebseite.HyperLink = null;
            this.lblWebseite.Left = 10.8125F;
            this.lblWebseite.Name = "lblWebseite";
            this.lblWebseite.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblWebseite.Text = "Webseite";
            this.lblWebseite.Top = 0F;
            this.lblWebseite.Width = 1.125F;
            // 
            // Line
            // 
            this.Line.Border.BottomColor = System.Drawing.Color.Black;
            this.Line.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.LeftColor = System.Drawing.Color.Black;
            this.Line.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.RightColor = System.Drawing.Color.Black;
            this.Line.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.TopColor = System.Drawing.Color.Black;
            this.Line.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Height = 0F;
            this.Line.Left = 0F;
            this.Line.LineWeight = 2F;
            this.Line.Name = "Line";
            this.Line.Top = 0.25F;
            this.Line.Width = 12.9375F;
            this.Line.X1 = 0F;
            this.Line.X2 = 12.9375F;
            this.Line.Y1 = 0.25F;
            this.Line.Y2 = 0.25F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Height = 0.25F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.TextBox1,
            this.TextBox2,
            this.TextBox3,
            this.TextBox4,
            this.TextBox5,
            this.TextBox6,
            this.TextBox7,
            this.txt_sosID,
            this.SubReport,
            this.Line2});
            this.GroupHeader1.Height = 0.4375F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // TextBox1
            // 
            this.TextBox1.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.DataField = "SOS_Name";
            this.TextBox1.Height = 0.1875F;
            this.TextBox1.Left = 0F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox1.Text = "TextBox1";
            this.TextBox1.Top = 0.2214567F;
            this.TextBox1.Width = 2.6875F;
            // 
            // TextBox2
            // 
            this.TextBox2.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox2.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox2.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox2.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox2.DataField = "SOS_Street";
            this.TextBox2.Height = 0.1875F;
            this.TextBox2.HyperLink = "HTTP://www.hawa.ch";
            this.TextBox2.Left = 2.75F;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox2.Text = "TextBox2";
            this.TextBox2.Top = 0.2214567F;
            this.TextBox2.Width = 2.0625F;
            // 
            // TextBox3
            // 
            this.TextBox3.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox3.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox3.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox3.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox3.DataField = "SOS_City";
            this.TextBox3.Height = 0.1875F;
            this.TextBox3.Left = 4.8125F;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox3.Text = "Ort";
            this.TextBox3.Top = 0.2214567F;
            this.TextBox3.Width = 2.3125F;
            // 
            // TextBox4
            // 
            this.TextBox4.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox4.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox4.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox4.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox4.DataField = "CNL_Name";
            this.TextBox4.Height = 0.1875F;
            this.TextBox4.Left = 7.1875F;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox4.Text = "TextBox4";
            this.TextBox4.Top = 0.2214567F;
            this.TextBox4.Width = 1.1875F;
            // 
            // TextBox5
            // 
            this.TextBox5.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox5.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox5.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox5.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox5.DataField = "SOS_Phone";
            this.TextBox5.Height = 0.1875F;
            this.TextBox5.Left = 8.4375F;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox5.Text = "TextBox5";
            this.TextBox5.Top = 0.2214567F;
            this.TextBox5.Width = 1.125F;
            // 
            // TextBox6
            // 
            this.TextBox6.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox6.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox6.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox6.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox6.DataField = "SOS_Fax";
            this.TextBox6.Height = 0.1875F;
            this.TextBox6.Left = 9.625F;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox6.Text = "TextBox6";
            this.TextBox6.Top = 0.2214567F;
            this.TextBox6.Width = 1.1875F;
            // 
            // TextBox7
            // 
            this.TextBox7.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox7.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox7.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox7.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox7.DataField = "SOS_web";
            this.TextBox7.Height = 0.1875F;
            this.TextBox7.Left = 10.8125F;
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.TextBox7.Text = "TextBox7";
            this.TextBox7.Top = 0.2214567F;
            this.TextBox7.Width = 2.125F;
            // 
            // txt_sosID
            // 
            this.txt_sosID.Border.BottomColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.LeftColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.RightColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.TopColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.DataField = "SOS_ID";
            this.txt_sosID.Height = 0.2F;
            this.txt_sosID.Left = 0F;
            this.txt_sosID.Name = "txt_sosID";
            this.txt_sosID.Style = "";
            this.txt_sosID.Text = "SOS_ID";
            this.txt_sosID.Top = 0.5964565F;
            this.txt_sosID.Visible = false;
            this.txt_sosID.Width = 1F;
            // 
            // SubReport
            // 
            this.SubReport.Border.BottomColor = System.Drawing.Color.Black;
            this.SubReport.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.LeftColor = System.Drawing.Color.Black;
            this.SubReport.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.RightColor = System.Drawing.Color.Black;
            this.SubReport.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.TopColor = System.Drawing.Color.Black;
            this.SubReport.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.CloseBorder = false;
            this.SubReport.Height = 0.15625F;
            this.SubReport.Left = 0F;
            this.SubReport.Name = "SubReport";
            this.SubReport.Report = null;
            this.SubReport.Top = 0.4402066F;
            this.SubReport.Width = 12.9375F;
            // 
            // Line2
            // 
            this.Line2.Border.BottomColor = System.Drawing.Color.Black;
            this.Line2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line2.Border.LeftColor = System.Drawing.Color.Black;
            this.Line2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line2.Border.RightColor = System.Drawing.Color.Black;
            this.Line2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line2.Border.TopColor = System.Drawing.Color.Black;
            this.Line2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.4089566F;
            this.Line2.Width = 12.9375F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 12.9375F;
            this.Line2.Y1 = 0.4089566F;
            this.Line2.Y2 = 0.4089566F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0.25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblSystem,
            this.lblKurzbeschreibung,
            this.Line1});
            this.GroupHeader3.Height = 0.4270833F;
            this.GroupHeader3.Name = "GroupHeader3";
            this.GroupHeader3.RepeatStyle = DataDynamics.ActiveReports.RepeatStyle.OnPage;
            this.GroupHeader3.Format += new System.EventHandler(this.GroupHeader3_Format);
            // 
            // lblSystem
            // 
            this.lblSystem.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSystem.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSystem.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSystem.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSystem.Border.RightColor = System.Drawing.Color.Black;
            this.lblSystem.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSystem.Border.TopColor = System.Drawing.Color.Black;
            this.lblSystem.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSystem.Height = 0.2F;
            this.lblSystem.HyperLink = null;
            this.lblSystem.Left = 0F;
            this.lblSystem.Name = "lblSystem";
            this.lblSystem.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblSystem.Text = "Systeme";
            this.lblSystem.Top = 0.1476378F;
            this.lblSystem.Width = 2.312992F;
            // 
            // lblKurzbeschreibung
            // 
            this.lblKurzbeschreibung.Border.BottomColor = System.Drawing.Color.Black;
            this.lblKurzbeschreibung.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblKurzbeschreibung.Border.LeftColor = System.Drawing.Color.Black;
            this.lblKurzbeschreibung.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblKurzbeschreibung.Border.RightColor = System.Drawing.Color.Black;
            this.lblKurzbeschreibung.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblKurzbeschreibung.Border.TopColor = System.Drawing.Color.Black;
            this.lblKurzbeschreibung.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblKurzbeschreibung.Height = 0.2F;
            this.lblKurzbeschreibung.HyperLink = null;
            this.lblKurzbeschreibung.Left = 2.780512F;
            this.lblKurzbeschreibung.Name = "lblKurzbeschreibung";
            this.lblKurzbeschreibung.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblKurzbeschreibung.Text = "Kurzbeschreibung";
            this.lblKurzbeschreibung.Top = 0.1476378F;
            this.lblKurzbeschreibung.Width = 1.968504F;
            // 
            // Line1
            // 
            this.Line1.Border.BottomColor = System.Drawing.Color.Black;
            this.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.LeftColor = System.Drawing.Color.Black;
            this.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.RightColor = System.Drawing.Color.Black;
            this.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.TopColor = System.Drawing.Color.Black;
            this.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 2F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.3937007F;
            this.Line1.Width = 12.9375F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 12.9375F;
            this.Line1.Y1 = 0.3937007F;
            this.Line1.Y2 = 0.3937007F;
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Height = 0.25F;
            this.GroupFooter3.Name = "GroupFooter3";
            // 
            // rptBezugsquellenDetail
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1819444F;
            this.PageSettings.Margins.Left = 0.5902778F;
            this.PageSettings.Margins.Right = 0.5784722F;
            this.PageSettings.Margins.Top = 0.1847222F;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperName = "Custom paper";
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 12.94792F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader3);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter3);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStrasse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWebseite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sosID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKurzbeschreibung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
		#endregion

        private void DataBind()
        {
            DataDynamics.ActiveReports.DataSources.SqlDBDataSource m_ds = new DataDynamics.ActiveReports.DataSources.SqlDBDataSource();
            m_ds.ConnectionString = ConfigurationSettings.AppSettings["DSN"];

            m_ds.SQL = "SELECT CSOS.SOS_ID, CSOS.SOS_Parent_ID, (CSOS.SOS_Name + ' ' + ISNULL(CSOS.SOS_AdditionalName1,'')+ ' ' + ISNULL(CSOSTL.STL_Name,'')) as SOS_Name,(ISNULL(CSOS.SOS_Street,'') +' '+ISNULL(CSOS.SOS_Street2,''))as SOS_Street, " +
                        "(ISNULL(CSOS.SOS_ZIP,'') +' '+ ISNULL(CSOS.SOS_City,''))as SOS_City, CSOS.SOS_Phone, CSOS.SOS_Fax, CSOS.SOS_Web, CCL.CNL_Name " +
                        "FROM   CLAPP_SourceOfSupplay CSOS " +
                        "LEFT JOIN CLAPP_SourceOfSupplay_Country CSOSCY ON CSOS.SOS_ID=CSOSCY.SOS_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayCategory CSOSSOSC ON CSOS.SOS_ID = CSOSSOSC.SOS_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayType CSOSSOST ON CSOS.SOS_ID = CSOSSOST.SOS_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplayCategory_Article CSOSCA " +
                        "INNER JOIN CLAPP_SourceOfSupplayCategory CSOSC ON CSOSCA.SCA_ID = CSOSC.SCA_ID ON CSOSSOSC.SCA_ID = CSOSC.SCA_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplayTypeLang CSOSTL ON CSOSSOST.SST_ID = CSOSTL.SST_ID AND CSOSTL.LNG_ID ='" + this.langId + "'" +
                        "INNER JOIN CLAPP_CountryLang CCL ON CSOS.CNT_ID = CCL.CNT_ID AND CCL.LNG_ID = '" + this.langId + "'" +
                        "WHERE  1=1 and CSOS.SOS_ID=" + this.sosID + " " +
                        "GROUP BY  CSOS.SOS_ID, CSOS.SOS_Parent_ID, CSOS.SOS_Name, CSOS.SOS_AdditionalName1, CSOS.SOS_Street, " +
                        "CSOS.SOS_Street2,CSOS.SOS_City, CSOS.SOS_ZIP,CSOSTL.STL_Name, CSOS.SOS_Phone, CSOS.SOS_Fax, CSOS.SOS_Web, CCL.CNL_Name " +
                        "ORDER BY  CSOS.SOS_Name, ISNULL (CSOS.SOS_Parent_ID,CSOS.SOS_ID)";


            this.DataSource = m_ds;


        }
        private void GroupHeader1_Format(object sender, System.EventArgs eArgs)
        {
            this.lblSystem.Text = this.page.getString("system", true);
            this.lblKurzbeschreibung.Text = this.page.getString("kurz", true);
            rptBZNiederlassungen rpt = new rptBZNiederlassungen();
            rpt.langId = this.langId;
            rpt.page = this.page;
            rpt.sosID = Convert.ToInt32(this.txt_sosID.Value.ToString());

            DataDynamics.ActiveReports.DataSources.SqlDBDataSource childDataSource = new DataDynamics.ActiveReports.DataSources.SqlDBDataSource();
            childDataSource.ConnectionString = ConfigurationSettings.AppSettings["DSN"];
            childDataSource.SQL = "SELECT  CSOS.SOS_ID, (CSOS.SOS_Name + ' ' + ISNULL(CSOS.SOS_AdditionalName1,'')) as SOS_Name,(ISNULL(CSOS.SOS_Street,'') +' '+ISNULL(CSOS.SOS_Street2,''))as SOS_Street, " +
                        "(ISNULL(CSOS.SOS_ZIP,'') +' '+ ISNULL(CSOS.SOS_City,''))as SOS_City, SOS_Phone, SOS_Fax, SOS_Web, CL.CNL_Name " +
                        "FROM   CLAPP_SourceOfSupplay CSOS left outer join CLAPP_CountryLang CL on CSOS.CNT_ID = CL.CNT_ID " +
                        "WHERE  SOS_Parent_ID = " + this.sosID +
                        "and CL.LNG_ID='" + this.langId + "'" +
                        "ORDER BY SOS_ZIP";

            rpt.DataSource = childDataSource;

            this.SubReport.Report = rpt;
       
        }
        private void GroupHeader2_Format(object sender, System.EventArgs eArgs)
        {
            this.lblFirma.Text = this.page.getString("firma", true);
            this.lblStrasse.Text = this.page.getString("strasse", true);
            this.lblPLZ.Text = this.page.getString("plz", true) + "/" + this.page.getString("ort", true);
            this.lblCountry.Text = this.page.getString("land", true);
            this.lblPhone.Text = this.page.getString("tel", true);
            this.lblFax.Text = this.page.getString("fax", true);
            this.lblWebseite.Text = this.page.getString("web", true);

        }
        private void GroupHeader3_Format(object sender, System.EventArgs eArgs)
        {
            this.lblSystem.Text = this.page.getString("system", true);
            this.lblKurzbeschreibung.Text = this.page.getString("kurz", true);

        }

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            //Systeme im Subreport
            
            rptBZSysteme rpt = new rptBZSysteme();
            rpt.langId = this.langId;
            rpt.page = this.page;
            rpt.sosID = this.sosID;
           

            DataDynamics.ActiveReports.DataSources.SqlDBDataSource childDataSource = new DataDynamics.ActiveReports.DataSources.SqlDBDataSource();
            childDataSource.ConnectionString = ConfigurationSettings.AppSettings["DSN"];
            childDataSource.SQL = "SELECT DISTINCT CAL.ARL_Name, CAL.ARL_Short, CAL.ART_ID " +
                        "FROM   CLAPP_SourceOfSupplay CSOS " +
                        "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayCategory CSOSSOSC ON CSOS.SOS_ID = CSOSSOSC.SOS_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayType CSOSSOST ON CSOS.SOS_ID = CSOSSOST.SOS_ID " +
                        "INNER JOIN CLAPP_ArticleLang CAL " +
                        "INNER JOIN CLAPP_Article CA ON CAL.ART_ID = CA.ART_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplayCategory_Article CSOSCA ON CAL.ART_ID = CSOSCA.ART_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplayCategory CSOSC ON CSOSCA.SCA_ID = CSOSC.SCA_ID ON CSOSSOSC.SCA_ID = CSOSC.SCA_ID " +
                        "INNER JOIN CLAPP_SourceOfSupplayTypeLang CSOSTL ON CSOSSOST.SST_ID = CSOSTL.SST_ID AND CSOSTL.LNG_ID = '" + this.langId + "' " +
                        "INNER JOIN CLAPP_CountryLang CCL ON CSOS.CNT_ID = CCL.CNT_ID AND CCL.LNG_ID = '" + this.langId + "' " +
                        "WHERE  CAL.LNG_ID='" + this.langId + "' " +
                        "AND (CSOS.SOS_ID=" + this.sosID +
                        " OR CSOS.SOS_Parent_ID=" + this.sosID + ") AND CA.ART_Active = 1 " +
                        "ORDER BY  CAL.ARL_Name";

            
            rpt.DataSource = childDataSource;
            this.SubReport1.Report = rpt;
          
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            this.lblTitle.Text = this.page.getString("DisplayTitle", true);
            this.lblFirma.Text = this.page.getString("firma", true);
            this.lblStrasse.Text = this.page.getString("strasse", true);
            this.lblPLZ.Text = this.page.getString("plz", true) + "/" + this.page.getString("ort", true);
            this.lblCountry.Text = this.page.getString("land", true);
            this.lblPhone.Text = this.page.getString("tel", true);
            this.lblFax.Text = this.page.getString("fax", true);
            this.lblWebseite.Text = this.page.getString("web", true);

        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.lblSeite.Text = this.page.getString("seite", true);
        }

	}
}
