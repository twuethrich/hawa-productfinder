using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export.Pdf;
using DataDynamics.ActiveReports.Export.Xls;

namespace Hawa.Src.Productfinder.documents {
	/// <summary>
	/// Summary description for materiallist1.
	/// </summary>
	public partial class reports : Alpha.Page {
		protected System.Web.UI.WebControls.Label Label1;
	
		private void Page_Load(object sender, System.EventArgs e) {
			ActiveReport3 rpt =  null;
			string reportName = "";
			 
			try{
				//Put user code to initialize the page here
				if (Session["languageId"] == null)
					Session["languageId"] = "de";
				
				/*try{
					orderId = Int32.Parse(Request["id"]);
				}
				catch(Exception){
					orderId = -1;
				}*/

				try{
					reportName = Request["report"].ToLower();
				}
				catch(Exception){
					reportName = "material";
				}


				switch(reportName){
					
					case "bezugsquellen":
						rpt = new rptBezugsquellen();
						((rptBezugsquellen)rpt).page = this;
						((rptBezugsquellen)rpt).langId = (string)Session["languageId"]; 
						break;

					case "bezugsquellendetail":
						rpt = new rptBezugsquellenDetail();
						((rptBezugsquellenDetail)rpt).page = this;
						((rptBezugsquellenDetail)rpt).langId = (string)Session["languageId"]; 
						break;

					case "systeme":
						rpt = new rptBezugsquellenDetail();
						((rptBezugsquellenDetail)rpt).page = this;
						((rptBezugsquellenDetail)rpt).langId = (string)Session["languageId"]; 
						break;

					default:
						rpt = new rptHeader();
						((rptHeader)rpt).page = this;
						((rptHeader)rpt).langId = (string)Session["languageId"]; 
						((rptHeader)rpt).reportName = reportName;						
						break;


				}
                rpt.SetLicense("Tobias Wuethrich,Hawa AG,DD-ARN-30-C000871,VOJ7M4JO8F9WOFHH884H");
                rpt.Document.Printer.PrinterName = "";

				rpt.Run(false);
			
			}
			catch (FormatException ex){
				// Failure running report, just report the error to the user:
				Response.Clear();
				Response.Write("<h1>Error running report:</h1>");
				Response.Write(ex.ToString());
				Response.End();
			}

			// Create a new memory stream that will hold the pdf output
			System.IO.MemoryStream memStream = new System.IO.MemoryStream();
			String strType = Request["type"];
			if(strType == null)
				strType="pdf";

			string fileName = "BezugsquellenListe";
			fileName = fileName.Replace(".", "_").Replace(":","_").Replace(" ", "_");
 
			switch (strType.ToLower()){
				case "xls":
					// Tell the browser this is a PDF document so it will use an appropriate viewer.
					Response.ContentType = "application/excel";

					// IE & Acrobat seam to require "content-disposition" header being in the response.  If you don't add it, the doc still works most of the time, but not always.
					// this makes a new window appear: 
					Response.AddHeader("content-disposition","attachment; filename=" + fileName + ".xls");

					// Create the PDF export object
					XlsExport xls = new XlsExport();

					
					//docexp.AutoRowHeight=true; 
					//docexp.DisplayGridLines=true; 
					//docexp.FileFormat=Xls95; //or Xls97Plus 
					//docexp.MinColumnWidth=360; 
					//docexp.MinRowHeight=250; 
					//docexp.MultiSheet=true; 
					xls.RemoveVerticalSpace=true; 
					xls.UseCellMerging=true; 
					
					

					
					// Export the report to PDF:
					xls.Export(rpt.Document, memStream);
					break;
				case "pdf":
				default: // and pdf
					// Tell the browser this is a PDF document so it will use an appropriate viewer.
					Response.ContentType = "application/pdf";

					// IE & Acrobat seam to require "content-disposition" header being in the response.  If you don't add it, the doc still works most of the time, but not always.
					Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".pdf");
					
					// Create the PDF export object
					PdfExport pdf = new PdfExport();
				
					// Export the report to PDF:
                    pdf.ConvertMetaToPng = true; 
					pdf.Export(rpt.Document, memStream);
					break;
			}

			// Write the PDF stream out
			Response.BinaryWrite(memStream.ToArray());
			// Send all buffered content to the client
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
