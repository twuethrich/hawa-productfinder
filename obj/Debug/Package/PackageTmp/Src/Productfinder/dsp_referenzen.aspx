<%@ Page language="c#" Codebehind="dsp_referenzen.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_referenzen" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
	<HEAD>
		<title>
		    <alpha:String ID="String9" RUNAT="server" KEY="PageTitle" /></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<SCRIPT language="JavaScript" src="jscript/general.js" type="text/javascript"></SCRIPT>
			<SCRIPT language="javascript">
				function onPageLoad()
					{
						<%= strScript %>
					}	
				 function keyCheck(e){        
    				 if ( (window.event && window.event.keyCode == 13) || (window.event.charCode == 13)){

                                        
                        document.all["btnSearch"].focus();
                    }
                 }
                 document.onkeydown = keyCheck;
			</SCRIPT>
			
		
			
			
			
			
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<form id="Form1" name="detail" runat="server">
			<table cellSpacing="0" cellPadding="0" width="900">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING id="String1" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>
                                    &nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>
                                    &nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnFr" RUNAT="server">Fran�ais</ASP:HYPERLINK>
                                    &nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnES" RUNAT="server">Espa�ol</ASP:HYPERLINK>
								<td></td>
							</tr>
						</table>
						<!-- Title & Language, start--></td>
				</tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30"><IMG height="10" src="images/ts.gif" width="12"><alpha:string id="iTipText" runat="server" Key="tipText"></alpha:string></td>
					</TD>
					<td align="right"><asp:hyperlink id="btn_close" runat="server" ImageUrl="images/btn_close_de.gif" NavigateUrl="javascript:window.close();">HyperLink</asp:hyperlink></td>
				</tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
			</table>
			<!--xxxxxxxxxxx -->
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<TBODY>
					<tr>
						<td valign="top" width="460">
							<table class="BoxGrayBright" cellSpacing="0" cellPadding="0" width="460" border="0">
								<tr>
									<td width="10" bgColor="white"><IMG src="images/ts.gif" width="10"></td>
									<td class="BoxTitle" colSpan="2"><ALPHA:STRING id="String2" RUNAT="server" KEY="Title"></ALPHA:STRING></td>
								</tr>
								<tr>
									<td width="10" bgColor="white" rowSpan="99"><IMG src="images/ts.gif" width="10"></td>
									<td colSpan="2" height="10"><IMG height="10" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td width="130" height="21"><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String3" RUNAT="server" KEY="Country"></ALPHA:STRING></td>
									<td height="21"><asp:dropdownlist class="inputField" id="cmbCountry" accessKey="X" 
                                            runat="server" Width="250px" AutoPostBack="True" 
                                            onselectedindexchanged="cmbCountry_SelectedIndexChanged"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"></td>
								</tr>
								<tr>
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String4" RUNAT="server" KEY="City"></ALPHA:STRING></td>
									<td><asp:textbox class="Inputfield1" id="txtOrt" runat="server" Width="250px"></asp:textbox></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"><IMG height="1" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String5" RUNAT="server" KEY="Object"></ALPHA:STRING></td>
									<td><asp:textbox class="Inputfield1" id="txtObject" runat="server" Width="250px"></asp:textbox></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"><IMG height="1" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String6" RUNAT="server" KEY="Building"></ALPHA:STRING></td>
									<td><asp:dropdownlist class="inputField" id="cmbBuildingType" runat="server" 
                                            Width="250px" AutoPostBack="True" 
                                            onselectedindexchanged="cmbBuildingType_SelectedIndexChanged"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td height="2"><IMG height="1" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String7" RUNAT="server" KEY="Beschlag"></ALPHA:STRING></td>
									<td><asp:dropdownlist class="inputField" id="cmbSystem" runat="server" 
                                            Width="250px" AutoPostBack="True" 
                                            onselectedindexchanged="cmbSystem_SelectedIndexChanged"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"><IMG height="10" src="images/ts.gif"></td>
								</tr>
								<tr vAlign="top">
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String8" RUNAT="server" KEY="Vorhaben"></ALPHA:STRING></td>
									<td><asp:panel id="plPlan" Runat="server"></asp:panel></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"><IMG height="10" src="images/ts.gif"></td>
								</tr>
								<tr vAlign="top">
									<td><IMG src="images/ts.gif" width="10"><ALPHA:STRING id="String10" RUNAT="server" KEY="Material"></ALPHA:STRING></td>
									<td><asp:panel id="plMaterial" Runat="server"></asp:panel></td>
								</tr>
								<tr>
									<td colSpan="2" height="2"><IMG height="20" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td></td>
									<td>
										<table cellSpacing="0" cellPadding="0" width="250" border="0">
											<tr>
												<!--<td><IMG onmouseover="style.cursor='pointer'" onclick="reset()" src="images/btn_reset.gif"></td>-->
												<td><asp:imagebutton id="btnReset" runat="server" ImageUrl="images/btn_reset.gif" 
                                                        onclick="btnReset_Click"></asp:imagebutton></td>
												<td align="right"><asp:imagebutton id="btnSearch" runat="server" 
                                                        ImageUrl="images/btn_search2_de.gif" onclick="btnSearch_Click"></asp:imagebutton>
													<!--<IMG id="btnSearch" src="images/btn_search2.gif" onClick="alert('dfdf');submit()" onMouseOver="javascript:style.cursor='pointer'"></td>--></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="20"><IMG height="10" src="images/ts.gif"></td>
								</tr>
								<tr>
									<td></td>
									<td width="250"><ALPHA:STRING id="String11" RUNAT="server" KEY="refFeedback"></ALPHA:STRING></td>
								</tr>
								<tr>
									<td></td>
									<td><ASP:HYPERLINK id="Feedback" RUNAT="server" NAVIGATEURL="">
											<ALPHA:STRING ID="String14" RUNAT="server" KEY="refFeedbackLink"></ALPHA:STRING>
										</ASP:HYPERLINK></td>
								</tr>
								<tr>
									<td height="50"><IMG height="50" src="images/ts.gif"></td>
								</tr>
							</table>
						</td>
						<td width="10"></td>
						<td valign="top" width="430">
							<TABLE cellSpacing="0" cellPadding="0" border="0" width="430">
								<TBODY>
									<tr>
										<td colspan="2"><IMG height="20" src="images/ts.gif"></td>
									</tr>
									<TR>
										<TD colspan="2" class="SearchTitle"><ALPHA:STRING id="String17" RUNAT="server" KEY="tiptitle"></ALPHA:STRING></TD>
									</TR>
									<TR>
										<TD colspan="2" class="SearchText"><ALPHA:STRING id="String13" RUNAT="server" KEY="tip1"></ALPHA:STRING></TD>
									</TR>
									<tr>
										<td colspan="2"><IMG height="10" src="images/ts.gif"></td>
									</tr>
									<TR>
										<TD colspan="2" class="SearchTitle"><ALPHA:STRING id="String15" RUNAT="server" KEY="tipexample"></ALPHA:STRING></TD>
									</TR>
									<tr>
										<td class="SearchText" width="140" nowrap><ALPHA:STRING id="String12" RUNAT="server" KEY="example_left"></ALPHA:STRING></td>
										<td class="SearchText" width="200" nowrap><ALPHA:STRING id="String18" RUNAT="server" KEY="example_right"></ALPHA:STRING></td>
									</tr>
									<tr>
										<td colspan="2"><IMG height="10" src="images/ts.gif"></td>
									</tr>
									<TR>
										<TD colspan="2" class="SearchText"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"><ALPHA:STRING id="String16" RUNAT="server" KEY="tip2"></ALPHA:STRING></TD>
									</TR>
								</TBODY>
							</TABLE>
						</td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
