using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for dsp_bezugsquellen_detail.
	/// </summary>
	public partial class dsp_bezugsquellen_detail : Alpha.Page
	{
		protected System.Web.UI.WebControls.Label lblShort;
		public string Kategory; 

		//fill all objects with search data
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				btnBack.ImageUrl ="images/btn_back_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				btnPrint.ImageUrl="images/btn_print_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";

				dgrOffice.Columns[0].HeaderText = getString("nl",true);
				dgrOffice.Columns[1].HeaderText = getString("nl2",true);
				dgrOffice.Columns[2].HeaderText = getString("strasse",true);
				dgrOffice.Columns[3].HeaderText = getString("plz",true);
				dgrOffice.Columns[4].HeaderText = getString("ort",true);
				dgrOffice.Columns[5].HeaderText = getString("land",true);
				dgrOffice.Columns[6].HeaderText = getString("tel",true);
				dgrOffice.Columns[7].HeaderText = getString("fax",true);
				dgrOffice.Columns[8].HeaderText = getString("web",true);

				dgrSystem.Columns[0].HeaderText = getString("system",true);
				dgrSystem.Columns[1].HeaderText = getString("kurz",true);

				DataSet tblOffice = Model.Bezugsquellen.getBezugsquellenSearchDet(Request.Params.Get("sosID").ToString());
				dgrOffice.DataSource = tblOffice ;
				if (tblOffice.Tables[0].Rows.Count == 0){
					txtNotFound1.Visible=true;
				}else{
					txtNotFound1.Visible=false;}
				dgrOffice.DataBind();

				DataSet tblKategory = Model.Bezugsquellen.getBezugsquellenKategorien(Request.Params.Get("sosID").ToString());
				dgrKategory.DataSource = tblKategory;
				if (tblKategory.Tables[0].Rows.Count == 0)
				{
					txtNotFound3.Visible=false;
				}
				else
				{
					txtNotFound3.Visible=false;}
				dgrKategory.DataBind();
			
				//DataTable dtDetail = Model.Bezugsquellen.getBezugsquellenSearchDetail(" AND CAL.ART_ID= " + Request.Params.Get("artID").ToString() + " AND CSOS.SOS_ID=" + Request.Params.Get("sosID").ToString()).Tables[0];
				DataTable dtDetail = Model.Bezugsquellen.getBezugsquellenSearchDetail(" AND CSOS.SOS_ID=" + Request.Params.Get("sosID").ToString()).Tables[0];
				//lblShort.Text = "Beustquellen: " + dtDetail.Rows[0].ItemArray[6].ToString();
				
                //Page Title
                Header.Title = "Hawa Bezugsquelle " + dtDetail.Rows[0].ItemArray[13].ToString() + ": " + dtDetail.Rows[0].ItemArray[2].ToString() + " (" + dtDetail.Rows[0].ItemArray[9].ToString() + " " + ")";
                
                lblArt.Text = dtDetail.Rows[0].ItemArray[9].ToString();
				lblCompany.Text = dtDetail.Rows[0].ItemArray[2].ToString();
				lblStreet.Text = dtDetail.Rows[0].ItemArray[3].ToString();
				lblZip.Text = dtDetail.Rows[0].ItemArray[5].ToString() + "&nbsp;" + dtDetail.Rows[0].ItemArray[4].ToString();
				lblCountry.Text = dtDetail.Rows[0].ItemArray[13].ToString();
				lblTel.Text = dtDetail.Rows[0].ItemArray[10].ToString();
				lblFax.Text = dtDetail.Rows[0].ItemArray[11].ToString();
				if(dtDetail.Rows[0].ItemArray[12].ToString() != "")
                    lblWebsite.Text = "<a href=\"http://" + dtDetail.Rows[0].ItemArray[12].ToString() + "\" target=\"_new\">"  + dtDetail.Rows[0].ItemArray[12].ToString() + "</a>";

									 //  "<a href=\"dsp_detail.aspx?artID=" + e.Item.Cells[2].Text + "\" class=\"detail\" style=\"font-size:11px\">" + sText + "</a>";


				if(dtDetail.Rows[0].ItemArray[15].ToString() != ""){
					lblCompany2.Text = dtDetail.Rows[0].ItemArray[15].ToString();
				}
				else{
					rCompany2.Visible = false;
				}
				if(dtDetail.Rows[0].ItemArray[16].ToString() != ""){
					lblCompany3.Text = dtDetail.Rows[0].ItemArray[16].ToString();
				}
				else{
					rCompany3.Visible = false;
				}
				if(dtDetail.Rows[0].ItemArray[17].ToString() != ""){
					lblStreet2.Text = dtDetail.Rows[0].ItemArray[17].ToString();
				}
				else{
					rStreet2.Visible = false;
				}

			
			
				DataSet tblCountry = Model.Bezugsquellen.getBezugsquellenCountry(Request.Params.Get("sosID").ToString());
				dgrCountry.DataSource = tblCountry;
				if (tblCountry.Tables[0].Rows.Count == 0)
				{
					txtNotFound2.Visible=false;
				}
				else
				{
					txtNotFound2.Visible=false;}
				dgrCountry.DataBind();

				//DataTable dtKategory = Model.Bezugsquellen.getBezugsquellenSearchSys(" AND (CSOS.SOS_ID=" + Request.Params.Get("sosID").ToString() + " OR CSOS.SOS_Parent_ID=" + Request.Params.Get("sosID").ToString() + ")").Tables[0];
				dgrSystem.DataSource = Model.Bezugsquellen.getBezugsquellenSearchSys(" AND (CSOS.SOS_ID=" + Request.Params.Get("sosID").ToString() + " OR CSOS.SOS_Parent_ID=" + Request.Params.Get("sosID").ToString() + ")");
				dgrSystem.ItemDataBound += new DataGridItemEventHandler(Grid_ItemDataBound);
				dgrSystem.DataBind();
				Session.Add("SOS_ID",Request.Params.Get("sosID"));
				//FillTablesKategories();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try 
				{
					E.sParam = "SOS_ID=" + Request.Params.Get("sosID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
			if (!Page.IsPostBack)
			{
				//txtNotFound1.Visible=false;
				//txtNotFound2.Visible=false;
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgrOffice.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrOffice_ItemDataBound);

		}
		#endregion
		
		public void FillTablesKategories()
		{
			DataTable dtKategory = Model.Bezugsquellen.getBezugsquellenKategorien(Request.Params.Get("sosID").ToString()).Tables[0];
			for (int i=0; i < dgrKategory.Items.Count; i++)
			{
				DataGrid dgrKat = new DataGrid();
				dgrKat.ID = "dgrKat_"+i;
				dgrKat.ShowHeader = false;
				dgrKat.BorderStyle = BorderStyle.NotSet;
				dgrKat.BorderWidth = Unit.Pixel(0);
				dgrKat.DataSource = Model.Bezugsquellen.getBezugsquellenSearchSys(" AND (CSOS.SOS_ID=" + Request.Params.Get("sosID").ToString() + " OR CSOS.SOS_Parent_ID=" + Request.Params.Get("sosID").ToString() + ") AND CSOSC.SCA_ID=" + dtKategory.Rows[i].ItemArray[1]);
				dgrKat.ItemDataBound += new DataGridItemEventHandler(Grid_ItemDataBound);
				dgrKat.DataBind();
				Label lblKategory = new Label(); 
				lblKategory.Text = "<a name=\"linkKat_" + i + "\"><b>" + dtKategory.Rows[i].ItemArray[0].ToString()+ "</b></a>";
				lblKategory.BackColor = Color.Gainsboro;
				lblKategory.Width = Unit.Pixel(700);
				lblKategory.Height = Unit.Pixel(16);

			}
		}


		//fill grid Kategory with data
		private void dgrKategory_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				ListItemType itemType = (ListItemType)e.Item.ItemType;	
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;
				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		private void dgrOffice_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{

		}

		//add links in table SYSTEM to open selected produkt
		private void Grid_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				ListItemType itemType = (ListItemType)e.Item.ItemType;		
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

				string sText = e.Item.Cells[0].Text;
				e.Item.Cells[0].Text = "<a href=\"dsp_detail.aspx?artID=" + e.Item.Cells[2].Text + "\" class=\"detail\" style=\"font-size:11px\">" + sText + "</a>";
				e.Item.Cells[2].Text = "";
				//e.Item.Cells[0].Text = "<a href=\"javascript:opener.location.href='dsp_detail.aspx?artID=" + e.Item.Cells[2].Text + "',window.close()\" class=\"detail\" style=\"font-size:11px\">" + sText + "</a>";
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}
		protected void btnPrint_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
			Response.Redirect("documents/reports.aspx?report=bezugsquellenDetail&type=pdf");
		}
		protected void dgrOffice_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dgrCountry_SelectedIndexChanged(object sender, System.EventArgs e) {
		
		}

		private void dgrKategory_SelectedIndexChanged(object sender, System.EventArgs e) {
		
		}

		protected void dgrSystem_SelectedIndexChanged(object sender, System.EventArgs e) {
		
		}
	}
}
