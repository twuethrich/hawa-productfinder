<%@ Page language="c#" Codebehind="dsp_referenzen_search.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_referenzen_search" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<Script language="JavaScript" src="jscript/general.js" type="text/javascript"></Script>
	</HEAD>
	</script>
	<body style="MARGIN: 0px" onload="window.focus()">
		<form name="detail" runat="server" ID="Form1">
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING ID="String1" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<!-- 
							<tr>
								<td align="right" colSpan="2"><a class="Language">Deutsch</a>&nbsp;|&nbsp;<a class="Language">English</a></td>
								<td></td>
							</tr>
							-->
						</table>
						<!-- Title & Language, start-->
					</td>
				</tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30"><IMG height="10" src="images/ts.gif" width="11"><alpha:string id="tipText" runat="server" Key="tipText"></alpha:string></td>
					</TD>
					<td align="right" style="padding-right:10px;">
						<ASP:IMAGE id="btnPrint" IMAGEURL="images/btn_print_DE.gif" Runat="server" border="0"></ASP:IMAGE>&nbsp;
						<asp:HyperLink id="btn_close" runat="server" ImageUrl="images/btn_back_DE.gif" NavigateUrl="javascript:alert();history.back()"></asp:HyperLink></td>
				</tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
			</table>
			<!-- header, end-->
			<table cellSpacing="0" cellPadding="0" width="880" border="0" align="center">
				<tr>
					<td>
						<DIV class="TA" id="tree" style="OVERFLOW: auto; WIDTH: 100%">
							<ASP:DATAGRID id="dgrReference" RUNAT="server" DATAKEYFIELD="ART_ID" ALLOWSORTING="True" WIDTH="100%"
								HORIZONTALALIGN="Center" PAGESIZE="18" BORDERCOLOR="White" BORDERSTYLE="Solid" BORDERWIDTH="1px"
								CELLPADDING="0" AUTOGENERATECOLUMNS="False" AllowPaging="True" onselectedindexchanged="dgrReference_SelectedIndexChanged">
								<ItemStyle CssClass="TableText"></ItemStyle>
								<Columns>
									<asp:BoundColumn DataField="CNL_Name" SortExpression="CNL_Name" HeaderText="Land">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="REL_Place" SortExpression="REL_Place" HeaderText="Ort">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="REL_Object" SortExpression="REL_Object" HeaderText="Objekt">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="RBL_Name" SortExpression="RBL_Name" HeaderText="Geb&#228;ude-Art">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="ARL_Name" SortExpression="ARL_Name" HeaderText="Systeme">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="MTL_Name" SortExpression="MTL_Name" HeaderText="Materialien">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="PLL_Name" SortExpression="PLL_Name" HeaderText="Vorhaben">
										<ItemStyle CssClass="TableText"></ItemStyle>
										<HeaderStyle ForeColor="Black" CssClass="GridHeaderText"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="REL_Architect" HeaderText="REL_Architect"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="REL_Realisator" HeaderText="REL_Realisator"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="REL_Description" HeaderText="REL_Description"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="REF_ID" HeaderText="REF_ID"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ART_ID" HeaderText="ART_ID"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="REF_FileName" HeaderText="REF_FileName"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ART_Active" HeaderText="ART_Active"></asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="right" Position="Top" CssClass="pager" Mode="NumericPages" >                         
                        </PagerStyle>
							</ASP:DATAGRID></DIV>
					</td>
				</tr>
				<!--
				<asp:Panel id="plFooter" runat="server" Height="8px">
					<TR>
						<TD align="center" colSpan="2">
							<asp:hyperlink id="lnkNewSearch" runat="server" NavigateUrl="dsp_referenzen.aspx?search=0">
								<Alpha:String key="newSearch" id="String5" runat="server"></Alpha:String>
							</asp:hyperlink>&nbsp; |
							<asp:hyperlink id="lnkOldSearch" runat="server" NavigateUrl="dsp_referenzen.aspx?search=1">
								<Alpha:String key="oldSearch" id="String6" runat="server"></Alpha:String>
							</asp:hyperlink></TD>
					</TR>
				</asp:Panel>-->
			</table>
		</form>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
