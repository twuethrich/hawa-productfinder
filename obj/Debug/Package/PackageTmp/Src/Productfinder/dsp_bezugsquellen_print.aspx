<%@ Page language="c#" Codebehind="dsp_bezugsquellen_print.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_bezugsquellen_print" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<Script language="JavaScript" src="jscript/general.js" type="text/javascript"></Script>
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus(); window.print();window.close();">
		<form name="Form1" id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="640" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING ID="String1" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2">
								<td></td>
							</tr>
						</table>
						<!-- Title & Language, start-->
					</td>
				</tr>
				<!-- header, start-->
				<!--
				<tr class="TopTitle">
					<td vAlign="middle" height="30">
						<IMG height="10" src="images/ts.gif" width="7">&nbsp;<b></b></td>
					<td vAlign="middle" align="right"><A href="javascript: window.close()">&nbsp;<ASP:IMAGE IMAGEURL="images/btn_print_de.gif" Runat="server" ID="btnPrint" border="0"></ASP:IMAGE></A>
						<ASP:IMAGE id="btnClose" border="0" Runat="server" IMAGEURL="images/btn_close_DE.gif"></ASP:IMAGE></td>
				</tr>
				-->
				<tr>
					<td colSpan="2" height="10"><IMG height="1" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
				<!-- header, end-->
			</table>
			<!-- liste, start-->
			<table cellSpacing="0" cellPadding="0" width="640" border="0" align="center">
				<tr>
					<td><img src="images/ts.gif" width="10"></td>
					<td align="left">
						<!--<DIV class="TA" id="tree" style="OVERFLOW: auto; WIDTH:640px; HEIGHT: 570px; scrollbar: ">-->
						<ASP:DATAGRID id="dgrBezugsquellen" RUNAT="server" ALLOWSORTING="True" WIDTH="100%" HORIZONTALALIGN="Center"
							PAGESIZE="20" BORDERCOLOR="White" BORDERSTYLE="Solid" BORDERWIDTH="1px" CELLPADDING="0" AUTOGENERATECOLUMNS="False"
							CssClass="BoxGrayAlternate">
							<AlternatingItemStyle CssClass="BoxGrayBright"></AlternatingItemStyle>
							<HeaderStyle ForeColor="Black" CssClass="BoxGrayBright"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="SOS_ID" HeaderText="ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_Name" SortExpression="SOS_Name" HeaderText="Firma">
									<HeaderStyle Wrap="false" Width="150px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle Wrap="true" CssClass="TableText" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STL_Name" SortExpression="STL_Name" HeaderText="Art" Visible="false">
									<HeaderStyle Wrap="False" Width="50px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle CssClass="TableText" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_Street" SortExpression="SOS_Street" HeaderText="Strasse">
									<HeaderStyle Wrap="False" Width="120px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle CssClass="TableText" Wrap="True" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_ZIP" SortExpression="SOS_ZIP" HeaderText="PLZ">
									<HeaderStyle Wrap="False" Width="40px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle Wrap="False" CssClass="TableText" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_City" SortExpression="SOS_City" HeaderText="Ort">
									<HeaderStyle Wrap="False" Width="100px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle CssClass="TableText" Wrap="True" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn DataField="CNL_Name" SortExpression="CNL_Name" HeaderText="Land">
									<HeaderStyle Wrap="False" Width="100px" CssClass="TableHeader" VerticalAlign="Top"></HeaderStyle>
									<ItemStyle CssClass="TableText" VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right" ForeColor="Black" Position="Top" CssClass="TableHeader"
								Mode="NumericPages"></PagerStyle>
						</ASP:DATAGRID><br>
						<!--</DIV>-->
					</td>
					<td><img src="images/ts.gif" width="10"></td>
				</tr>
			</table>
		</form>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
