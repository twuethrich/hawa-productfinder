using System;

namespace Hawa.Src.Productfinder.Model.Other
{
	/// <summary>
	/// Summary description for QPlanFuncMat.
	/// </summary>
	public class QPlanFuncMat
	{
		private int _plan_id;
		private int _fnc_id;
		private int _mtl_id;

		public  int iPLAN_ID 
		{
			get
			{
				return _plan_id;
			}

			set 
			{
				_plan_id = value;
			}
		}

		public  int iFNC_ID 
		{
			get
			{
				return _fnc_id;
			}

			set 
			{
				_fnc_id = value;
			}
		}

		public  int iMTL_ID 
		{
			get
			{
				return _mtl_id;
			}

			set 
			{
				_mtl_id = value;
			}
		}

		public bool isEQ(int pid, int fid,int  mid )
	{
		bool isB = true;

		if (pid>=0)
   		{
			if (pid != _plan_id)
			{
				isB = false;
			}
		}

			if (fid>=0)
			{
				if (fid != _fnc_id)
				{
					isB = false;
				}
			}

			if (mid>=0)
			{
				if (mid != _mtl_id)
				{
					isB = false;
				}
			}
      return isB;
	}
		public void New (int pid, int fid,int  mid )
	{
        _plan_id = pid;
		_fnc_id = fid;
		_mtl_id = mid;

	}
		public string getAsText()
		{
			return string.Format("{0},{1},{2}",_plan_id,_fnc_id,_mtl_id);
		}
		public QPlanFuncMat()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
