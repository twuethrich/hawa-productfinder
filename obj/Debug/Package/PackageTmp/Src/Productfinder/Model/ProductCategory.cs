﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Hawa.Src.Productfinder.Model
{
    public class ProductCategory
    {
        public ProductCategory()
        {
            //
            // TODO: Fügen Sie hier die Konstruktorlogik hinzu
            //
        }

        public static int getProductCategory(int productID)
        {
            int pCategory = 0;

            List<int> pList =  new List<int>();
            pList.Add(65);
            pList.Add(66);
            pList.Add(67);
            pList.Add(10407);
            pList.Add(10409);
            pList.Add(10410);
            pList.Add(11267);

            if(pList.Contains(productID)) {
                pCategory = 1;
            }
            return pCategory;

        }

    }
}