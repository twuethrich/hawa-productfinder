using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Plan.
	/// </summary>
	public class Plan
	{
		private    int  _plan_id  ;
		private string  _plan_name;
		private ArrayList _languages = new ArrayList();

		// Properties		
		public  int iPLAN_ID 
		{
			get
			{
				return _plan_id;
			}

			set 
			{
				_plan_id = value;
			}
		}

		public void Load(SqlConnection sqlConnection)
		{

            
             
			string mySelectQuery = 
				String.Format("SELECT LNG_ID, PLL_Name FROM CLAPP_PlanLang WHERE PLA_ID = {0}", _plan_id);

			SqlDataAdapter plLAD = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet plLDS = new DataSet();
			plLAD.Fill(plLDS,"CLAPP_PlanLang");

			setPlanesLangList(plLDS);


			}
		public void setPlanesLangList(DataSet dsPLL)
	    {
			foreach( DataRow row in dsPLL.Tables["CLAPP_PlanLang"].Rows)
			{
				PlanLang PL = new PlanLang();
				if (!row.IsNull(0) &
					!row.IsNull(1) 
				   )
				{
					PL.iLNG_ID = (string)row[0];
					PL.iPLL_NAME = (string)row[1];
                    _languages.Add(PL);
				}

			}
 		 
	    }
		public  string iPLAN_NAME 
		{
			get
			{
				return _plan_name;
			}

			set 
			{
				_plan_name = value;
			}
		}

		public string getPLL_Name(string LNGID)
		{
			for(int i=0;i<_languages.Count;i++)
			{
				if (((PlanLang)_languages[i]).iLNG_ID.ToUpper() == LNGID.ToUpper())
				{
                 return ((PlanLang)_languages[i]).iPLL_NAME; 
				}
			}
			return  "";
		}

		public Plan()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
