using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Functions.
	/// </summary>
	public class Functions
	{
		private ArrayList _functions = new ArrayList();

		public void Load (SqlConnection sqlConnection)
		{ 
			string mySelectQuery = "SELECT FNC_ID, FNC_Name FROM CLAPP_Function";

			SqlDataAdapter adFNC = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet dsFNC = new DataSet();
			adFNC.Fill(dsFNC,"CLAPP_Function");


			setFunctionsList(sqlConnection, dsFNC); 
		}
		public void Clear()
		{
			_functions.Clear();
		}

		public int Count()
		{
			return _functions.Count;
		}
		public Function getItem(int i)
		{
			if (i < _functions.Count & i >= 0)
			{
				return (Function)_functions[i];			            
			}
			else
			{
				return null;
			}
		}
		private void setFunctionsList( SqlConnection sqlConnection, DataSet dsFNC)
		{
			foreach( DataRow row in dsFNC.Tables["CLAPP_Function"].Rows)
			{
				Function f = new  Function();
				
				if (row.IsNull(0))
				{
					f.iFNC_ID = -1;
				}
				else
				{
					f.iFNC_ID = (int)row[0];
				}

				if (row.IsNull(1))
				{
					f.iFNC_NAME = "";
				}
				else
				{
					f.iFNC_NAME = (string)row[1];
				}
				f.Load(sqlConnection);
				_functions.Add(f);
			}
		}

		public Functions()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
