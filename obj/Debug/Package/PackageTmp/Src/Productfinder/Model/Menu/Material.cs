using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Material.
	/// </summary>
	public class Material
	{
		private    int  _material_id  ;
		private ArrayList _languages = new ArrayList();

		// Properties		
		public  int iMAT_ID 
		{
			get
			{
				return _material_id;
			}

			set 
			{
				_material_id = value;
			}
		}

		public void Load(SqlConnection sqlConnection)
		{

            
             
			string mySelectQuery = 
				String.Format("SELECT LNG_ID, MTL_Name FROM CLAPP_MaterialLang WHERE MAT_ID = {0}", _material_id);

			SqlDataAdapter adML = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet dsMATL = new DataSet();
			adML.Fill(dsMATL,"CLAPP_MaterialLang");

			setMaterialLangList(dsMATL);


		}
		public void setMaterialLangList(DataSet dsMATL)
		{
			foreach( DataRow row in dsMATL.Tables["CLAPP_MaterialLang"].Rows)
			{
				MatLang MT = new MatLang();					
				if (!row.IsNull(0) &
					!row.IsNull(1) 
					)
				{
					MT.iLNG_ID = (string)row[0];
					MT.iMTL_NAME = (string)row[1];
					_languages.Add(MT);
				}

			}
 		 
		}

		public string getMTL_Name(string LNGID)
		{
			for(int i=0;i<_languages.Count;i++)
			{
				if (((MatLang)_languages[i]).iLNG_ID.ToUpper() == LNGID.ToUpper())
				{
					return ((MatLang)_languages[i]).iMTL_NAME; 
				}
			}
			return  "";
		}

		public Material()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
