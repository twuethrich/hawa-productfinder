using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Menu
{

	/// <summary>
	/// Summary description for Menu.
	/// </summary>
	public class Menu
	{
		static string sConnStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
		Planes _P;
		Functions _F;
		Materials _M;

		public static Menu getSingelton()  
		{
			Menu M;
			M = new Menu();			
			M.Load();
			return M;
		}
		// Properties		
		public  Planes iPLANES 
		{
			get
			{
				return _P;
			}
		}

		public  Functions iFUNCTIONS 
		{
			get
			{
				return _F;
			}
		}

		public  Materials iMaterials 
		{
			get
			{
				return _M;
			}
		}


		public void Init()
		{
			 _P = new Planes();
			 _F = new Functions();
			 _M = new Materials();
		}
			public bool Load()
		{
			bool isOk = false;

			if (sConnStr.Length > 0) 
			{
				_P.Clear();
				_F.Clear();
				_M.Clear();
			}
			else
			{
				throw new Exception("SQL Connection string cannot be zero-length");
			}
			SqlConnection sqlConnection = new SqlConnection(sConnStr);
			sqlConnection.Open();

            // Plan,Function and Material read
			_P.Load(sqlConnection);        
    		_F.Load(sqlConnection);                         
			_M.Load(sqlConnection);

			sqlConnection.Close(); 
			return isOk;
			

		}
		public Menu()
		{
			//
			// TODO: Add constructor logic here
			//
			Init();
		}
	}
}
