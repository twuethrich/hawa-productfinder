using System;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for MatLang.
	/// </summary>
	public class MatLang
	{
		private string  _lng_id  ;
		private string  _mtl_name;
		
		// Properties		
		public  string iLNG_ID 
		{
			get
			{
				return _lng_id;
			}

			set 
			{
				_lng_id = value;
			}
		}
		// Properties		
		public  string iMTL_NAME 
		{
			get
			{
				return _mtl_name;
			}

			set 
			{
				_mtl_name = value;
			}
		}

		public MatLang()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
