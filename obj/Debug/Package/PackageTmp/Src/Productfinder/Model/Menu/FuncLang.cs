using System;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for FuncLang.
	/// </summary>
	public class FuncLang
	{
		private string  _lng_id  ;
		private string  _fnl_name;
		
		// Properties		
		public  string iLNG_ID 
		{
			get
			{
				return _lng_id;
			}

			set 
			{
				_lng_id = value;
			}
		}
		// Properties		
		public  string iFNL_NAME 
		{
			get
			{
				return _fnl_name;
			}

			set 
			{
				_fnl_name = value;
			}
		}

		public FuncLang()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
