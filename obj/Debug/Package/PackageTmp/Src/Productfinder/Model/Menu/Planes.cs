using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web;


namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Planes.
	/// </summary>
	public class Planes
	{
		private ArrayList _planes = new ArrayList();

		public void Load (SqlConnection sqlConnection)
		{ 
			string mySelectQuery = "SELECT PLA_ID, PLA_Name FROM CLAPP_Plan";
					

			SqlDataAdapter plAD = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet plDS = new DataSet();
			plAD.Fill(plDS,"CLAPP_Plan");

			setPlanesList(sqlConnection, plDS); 
		}
		public void Clear()
		{
            _planes.Clear();
		}

		public int Count()
		{
			return _planes.Count;
		}
		public Plan getItem(int i)
		{
			if (i < _planes.Count & i >= 0)
			{
				return (Plan)_planes[i];			            
			}
			else
			{
				return null;
			}
		}
		private void setPlanesList( SqlConnection sqlConnection, DataSet  plDS )
		{
			foreach( DataRow row in plDS.Tables["CLAPP_Plan"].Rows)
			{
				Plan p = new  Plan();
				if (row.IsNull(0))
				{
					p.iPLAN_ID = -1;
				}
				else
				{
					p.iPLAN_ID = (int)row[0];
				}

				if (row.IsNull(1))
				{
					p.iPLAN_NAME = "";
				}
				else
				{
					p.iPLAN_NAME = (string)row[1];
				}
			    p.Load(sqlConnection);
				_planes.Add(p);
			}
		}

		public Planes()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
