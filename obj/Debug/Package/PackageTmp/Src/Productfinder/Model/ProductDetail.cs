using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
//using Hawa.Classes;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Zusammenfassung f�r ProductDetail
	/// </summary>
	public class ProductDetail
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
		
		public static DataSet getProductDetail(int artID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getProductfinderDetail",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				if(artID > -1)
				{
					sqlParam = sqlCommand.Parameters.Add("@ProductID",SqlDbType.Int); 
					sqlParam.Value = artID;
				}
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
		

        public static DataSet getSystemplannerProducts(int productID){
            SqlConnection sqlConnection = new SqlConnection(sConnection);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("HWA_getProductList",sqlConnection);            
            sqlCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlParam;
            sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
            sqlParam.Direction = ParameterDirection.Input;
            sqlParam.Value = "DE";                                               
            sqlParam = sqlCommand.Parameters.Add("@ApplicationID",SqlDbType.Int);            
            sqlParam.Value = 2;                     
            sqlParam = sqlCommand.Parameters.Add("@Checkdoors",SqlDbType.Bit);            
            if(ProductCategory.getProductCategory(productID) == 1){
                sqlParam.Value = 0;
            }else{
                sqlParam.Value = 1;
            }

            SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
            sqlDAdapter.SelectCommand = sqlCommand;
            DataSet datSet = new DataSet();
            sqlDAdapter.Fill(datSet);
            sqlConnection.Close();
            return datSet;
        }


		public static DataSet getProductFinderObjects(int artID, int ObjectGroupID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getProductFinderObjects",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				if(artID > -1)
				{
					sqlParam = sqlCommand.Parameters.Add("@ProductID",SqlDbType.Int); 
					sqlParam.Value = artID;
				}
				sqlParam = sqlCommand.Parameters.Add("@ObjectGroupID",SqlDbType.Int); 
				sqlParam.Value = ObjectGroupID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		






	}
}
