using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for ArticleAndGroup.
	/// base class with return dataset for form detail
	/// </summary>
	public class ArticleAndGroup
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
	
		//return dataset with document for article in detail form
		//input param ArticleID => article id
		//input param GroupID => group of documents (38-DXF, 39-prospekte, ...)
		public static DataSet getArticleAndGroup(int ArticleID, int GroupID, int Accessoire)
		{	
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getArticleAndGroup",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArticleID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Value = GroupID;
				sqlParam = sqlCommand.Parameters.Add("@Accessoire",SqlDbType.Int); 
				sqlParam.Value = Accessoire;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
		public static DataSet getLanguageObjects(int ArticleID, int GroupID, int Accessoire) {	
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getLanguageObjects",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArticleID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Value = GroupID;
				sqlParam = sqlCommand.Parameters.Add("@Accessoire",SqlDbType.Int); 
				sqlParam.Value = Accessoire;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex) {
				throw (ex);
			}
		}
        public static DataSet getAusschreibungstext(int ArticleID, int GroupID, int GroupID2)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sConnection);
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("HWA_getAusschreibungstexte", sqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlParam;
                sqlParam = sqlCommand.Parameters.Add("@LanguageID", SqlDbType.Char);
                sqlParam.Direction = ParameterDirection.Input;
                sqlParam.Value = HttpContext.Current.Session["LanguageID"];
                sqlParam = sqlCommand.Parameters.Add("@ArticleID", SqlDbType.Int);
                sqlParam.Value = ArticleID;
                sqlParam = sqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                sqlParam.Value = GroupID;
                sqlParam = sqlCommand.Parameters.Add("@GroupID2", SqlDbType.Int);
                sqlParam.Value = GroupID2;
               
                SqlDataAdapter sqlDAdapter = new SqlDataAdapter();
                sqlDAdapter.SelectCommand = sqlCommand;
                DataSet datSet = new DataSet();
                sqlDAdapter.Fill(datSet);
                sqlConnection.Close();
                return datSet;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
		public static DataSet getPlanung(int ArticleID, int GroupID, int GroupID2,int Accessoire) {	
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getMontageObjects",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArticleID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Value = GroupID;
				sqlParam = sqlCommand.Parameters.Add("@GroupID2",SqlDbType.Int); 
				sqlParam.Value = GroupID2;
				sqlParam = sqlCommand.Parameters.Add("@Accessoire",SqlDbType.Int); 
				sqlParam.Value = Accessoire;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex) {
				throw (ex);
			}
		}

		//Query to get Subarticles Objects (Isometrische Darstellungen)
		public static DataSet getSubArticleObjects(int ArticleID, int GroupID) {	
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getSubArticleObjects",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArticleID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Value = GroupID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex) {
				throw (ex);
			}
		}

		
		//return dataset with document for article in detail form
		//was used for multiple group
		//input param ArticleID => article id
		//input param TypID => typ of documents (2 - jpg,gif, 4 - pdf,5 - dxf, 7 - eps, ...)
		public static DataSet getArticleAndTyp(int ArticleID, int TypID)
		{	
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getArticleAndTyp",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArticleID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				sqlParam = sqlCommand.Parameters.Add("@TypID",SqlDbType.Int); 
				sqlParam.Value = TypID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		public static DataSet getRelAccessoire(int ArticleID)
		{	
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getSystemAccessoires",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@artID",SqlDbType.Int); 
				sqlParam.Value = ArticleID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

	}
}
