using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model;
using System.IO;
using System.Xml;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r ObjectDownload.
	/// class return file to download
	/// </summary>
	public partial class ObjectDownload : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			string download_path;
			string name = "";
			string path = "";

			try
			{
				string id = Request["id"];
				long typ = Convert.ToInt32(Request["typ"]);
				int group = Convert.ToInt32(Request["group"]);
			
				if(typ == 0)
				{
					name = id+"_Blatt_1.gif";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemsymbol\thumbnails\" + name;
				}
			
				if(typ == 1)
				{
					name = id+".gif";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemgruppensymbol\thumbnails\" + name;
				}
				//Anzeige von flash-files
				if(typ == 10) {
					name = id+".swf";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemgruppensymbol\" + name;
				}
				//Anzeige der animierten Gif's - Systemgruppensymbole
				if(typ == 11) 
				{
					name = id+".gif";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemgruppensymbol\thumbnails\" + name;
				}


				//Generelle Objekte mit der Extension enthalten
				if(typ == 2)
				{
					download_path = Model.DownloadPath.getDownloadPathFolder(group);
					name = id+"";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" + name;
				}
				//Generelle jpg-Objekte
				if(typ == 3)
				{
					download_path = Model.DownloadPath.getDownloadPath(group);
					name = id+".jpg";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" + name;
				}
				//Anzeige thumbnails im Systembild-Downloadfenster
				if(typ == 4) {
					download_path = Model.DownloadPath.getDownloadPath(group);
					name = id+".jpg";
					if(download_path == "Systembild 1")
						path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systembild 1\thumbnails\" + name;
					if(download_path == "Systembild 2")
						path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systembild 2\thumbnails\" + name;
					if(download_path == "Systembild 3")
						path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systembild 3\thumbnails\" + name;
				}
				//Anzeige Systembild f�r die Produktdetail-Seite - Systembild 1/preview
				if(typ == 5) {
					name = id+".png";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systembild 1\preview\" + name;
				}
				//Accessoire-Gruppensymbole
				if(typ == 6) {
					name = id+".gif";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Accessoiregruppensymbole\thumbnails\" + name;
				}
				//Generelle Anzeige von gif-Objekten 
				if(typ == 7) {
					download_path = Model.DownloadPath.getDownloadPath(group);
					name = id+".gif";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" + name;
				}
				//Referenzbilder
				if(typ == 9) {
					name = id;
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"references\" + name;
				}
				//Referenzbilder
				if(typ == 15) {
					download_path = Model.DownloadPath.getDownloadPathFolder(group);
					name = id+".zip";
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" + name;
				}

               


                FileInfo file = new FileInfo(path);
               

                switch(file.Extension){
                    case ".pdf":
                        Response.ClearContent();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                        Response.AddHeader("Content-Length", file.Length.ToString());
                        Response.TransmitFile(file.FullName);
                        break;
                    case ".doc":
                        Response.ClearContent();
                        Response.ContentType = "application/msword";
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                        Response.AddHeader("Content-Length", file.Length.ToString());
                        Response.TransmitFile(file.FullName);
                        break;
                
                    default:
                        Response.AddHeader("content-disposition", "attachment; filename=" + name);
                        Response.WriteFile(path);
                        break;

                }
               
				//Response.End();
			}
			catch (Exception ex)
			{
				
//				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
//				E.sErrorMessage = ex.Message.ToString();
//				E.sErrorStackTrace = ex.StackTrace.ToString();
//				try
//				{
//					E.sParam = "ID=" + Request["id"] + " <br> ";
//					E.sParam += "GROUP=" + Request["group"];
//				}
//				catch
//				{
//					E.sParam = "";
//				}
//				Response.Redirect("dsp_error_page.aspx");
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = "File " + name + " was not found in " + path;
				Response.Redirect("dsp_error_download.aspx");
			}

		}

		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}

