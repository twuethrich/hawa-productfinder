<%@ Page language="c#" Codebehind="dsp_dxfinfo.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dxfinfo" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td><img src="images/ts.gif" width="10" height="10"></td>
				<td rowspan="3"><img src="images/hawa_logo_small.png"></td>
				<td></td>
				<td><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right" width="100%"><a href="javascript: window.close();"><asp:image imageUrl="images/btn_close_de.gif" border="0" Runat="server" ID="btnClose"></asp:image></a></td>
				<td><img src="images/ts.gif" width="10"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<TR>
				<TD colspan="4" class="BoxTitle"><asp:label id="lblShort" runat="server">
						<ALPHA:STRING ID="String1" RUNAT="server" KEY="Title"></ALPHA:STRING>
					</asp:label></TD>
			</TR>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
		</TABLE>
		<TABLE cellSpacing="0" cellPadding="0" width="400" border="0" >
			<form id="Form1" method="post" runat="server">
				<TBODY>
					<TR vAlign="top">
						<td><img src="images/ts.gif" width="10" height="10"></td>
						<TD colspan="2"><asp:label id="Label1" runat="server">
								<ALPHA:STRING ID="String2" RUNAT="server" KEY="infotext"></ALPHA:STRING>
					
							</asp:label></TD>
					</TR>
					<tr>
						<td><img src="images/ts.gif" width="10" height="10"></td>
						<td colspan="2"><img src="images/ts.gif" width="10" height="10"></td>
					</tr>
					<tr>
						<td><img src="images/ts.gif" width="10" height="10"></td>
						<td width="10"><asp:CheckBox id="CheckBox1" runat="server" oncheckedchanged="CheckBox1_CheckedChanged"></asp:CheckBox></td>
						<td width=390><ALPHA:STRING ID="String3" RUNAT="server" KEY="infotext3"></ALPHA:STRING></td>
					</tr>
				</TBODY>
			</form>
		</TABLE>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
