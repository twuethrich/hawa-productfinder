using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for dsp_bezugsquellen.
	/// </summary>
	public partial class dsp_bezugsquellen : Alpha.Page
	{
		protected Alpha.Controls.String Seite;
		public string alertText ="";
		public string sOrder = "";
		      
		int artID=0;
		
		int countryID=0;

		protected void Page_Load(object sender, System.EventArgs e)
		{   

			btnSearch.ImageUrl="images/btn_search2_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
			btnClose.ImageUrl="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
			btnPrint.ImageUrl="images/btn_print_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";

			alertText = getString("alertText",true);

			btnPrint.Attributes.Add("onClick", "javascript:confirmPDF();return true"); 
			

			
			if(Convert.ToInt32(Request.Params.Get("artID")) > 0){

				artID =Convert.ToInt32(Request.Params.Get("artID").ToString());
			}

			if(Convert.ToInt32(Request.Params.Get("countryID")) > 0){

				countryID =Convert.ToInt32(Request.Params.Get("countryID").ToString());
			}

			
		
			btnClose.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
			btnClose.Attributes.Add("onClick", "javascript:window.close()"); 
			
			dgrBezugsquellen.Columns[1].HeaderText = getString("firma",true);
			dgrBezugsquellen.Columns[2].HeaderText = getString("art",true);
			dgrBezugsquellen.Columns[3].HeaderText = getString("strasse",true);
			dgrBezugsquellen.Columns[4].HeaderText = getString("plz",true);
			dgrBezugsquellen.Columns[5].HeaderText = getString("ort",true);
			dgrBezugsquellen.Columns[17].HeaderText = getString("land",true);


			Reference R = (Reference)(HttpContext.Current.Session["reference"]);
            if (!Page.IsPostBack) {                                        
                R.sBezugsquellenSort = "SOS_Name";               
                strError.Visible=false;
            }
            if ((R.sBezugsquellenSort.Length>0) && (R.sBezugsquellenSort.Substring(R.sBezugsquellenSort.Length -4) == "DESC")){
                sOrder = "5";
            }
            else {
                sOrder = "6";
            }
			try	{
				//fill combo COUNTRY
				FillcmbCountry();
				
				countryID = Convert.ToInt32(cmbCountry.SelectedValue.ToString());

				btnEn.NavigateUrl = Request.Path + "?languageID=En&countryID="+countryID;
				//btnEn.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=En&countryID="+countryID;
				btnDe.NavigateUrl = Request.Path + "?languageID=De&countryID="+countryID;
				//btnDe.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=De&countryID="+countryID;
				btnFr.NavigateUrl = Request.Path + "?languageID=Fr&countryID="+countryID;
			    btnEs.NavigateUrl = Request.Path + "?languageID=Es&countryID="+countryID;

				
				if ((Request.Params.Get("artID")!= null & Convert.ToInt32(Request.Params.Get("artID"))>0) || (Request.Params.Get("countryID")!= null & Convert.ToInt32(Request.Params.Get("countryID"))>0)) 
				{ 
					dgrBezugsquellen.CurrentPageIndex = 0;
					FillTable();
					dgrBezugsquellen.DataBind();
					lblProductName.Text = Request.Params.Get("title");
					Session.Add("productName",Request.Params.Get("title"));
					lblTitle.Visible=false;
					lblProductName.Visible=true;
					String1.Visible=true;
				}
				else{
					lblTitle.Visible=true;
					lblProductName.Visible=false;
					Session.Add("productName","");
					String1.Visible=false;
				}
				
				

			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");
			}
			
		}

		//fill combo COUNTRY with data
		public void FillcmbCountry()
		{
			try
			{
				if (cmbCountry.Items.Count == 0)
				{
					DataSet dsCountry = new DataSet();
					if(Request.Params.Get("artID")!= null)
					{
						dsCountry = Model.Country.getCountryBezugsquelle(Convert.ToInt64(Request.Params.Get("artID").ToString()));
					
					}
					else
					{
						dsCountry = Model.Country.getCountryBezugsquelle(-1);
					}
					//cmbCountry.Items.Add("ALLE");
					//cmbCountry.Items[0].Value = "0";

					for(int i=0; i < dsCountry.Tables[0].Rows.Count; i++)
					{
						cmbCountry.Items.Add(dsCountry.Tables[0].Rows[i].ItemArray[1].ToString());	  
						cmbCountry.Items[i].Value = (dsCountry.Tables[0].Rows[i].ItemArray[0].ToString());	  

						if(cmbCountry.Items[i].Value.Equals(countryID.ToString()) && countryID > 0){
							cmbCountry.Items[i].Selected = true;
						}

					}
				}
			

			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgrBezugsquellen.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.OnItemCreated);
			this.dgrBezugsquellen.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgrBezugsquellen_PageIndexChanged);
			this.dgrBezugsquellen.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgrBezugsquellen_SortCommand);
			this.dgrBezugsquellen.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrBezugsquellen_ItemDataBound);

		}
		#endregion

		//add table paging
		private void dgrBezugsquellen_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{ 
			try
			{                
				string sWhere = "";
				//create "WHERE" string for select
				//add to "WHERE" string ARTIKLE_ID
				if (Request.Params.Get("artID") != null)
				{
					sWhere += " AND CSOSCA.ART_ID=" + Request.Params.Get("artID").ToString();
				}
				//add to "WHERE" string COUNTRY_ID
				if (cmbCountry.SelectedValue.ToString() != "0")
				{
					sWhere += " AND CSOS.CNT_ID=" + cmbCountry.SelectedValue;
				}
				//add to "WHERE" string (LIKE) NAME
				if (txtName.Text.ToString() != "")
				{
					sWhere += " AND CSOS.SOS_Name like '%" + txtName.Text.ToString() + "%' ";
				}
				//add to "WHERE" string DEALER or PROCESSOR
				if (chkDealer.Checked == true && chkProcessor.Checked == true)
				{
				}
				else
				{
					if (chkDealer.Checked == true)
					{
						sWhere += " AND CSOSTL.SST_ID = 1";
					}

					if (chkProcessor.Checked == true)
					{
						sWhere += " AND CSOSTL.SST_ID = 2";
					}
				}

				//sWhere += " AND CSOS.SOS_Parent_ID IS NULL";
				DataSet dsSort = new DataSet ();
				dsSort = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);
				DataView vSort = new DataView(dsSort.Tables[0]);
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				vSort.Sort = R.sBezugsquellenSort;
				if ((vSort.Sort.Length>0) &&
					(vSort.Sort.Substring(vSort.Sort.Length -4) == "DESC")
				   )
				{
					sOrder = "5";
				}
				else
				{
					sOrder = "6";
				}
				dgrBezugsquellen.DataSource = vSort;
				dgrBezugsquellen.CurrentPageIndex = e.NewPageIndex ;
				dgrBezugsquellen.DataBind();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		//set checkboxes to ON or OFF value
		private void dgrBezugsquellen_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{              
				ListItemType itemType = (ListItemType)e.Item.ItemType;                
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
				{
					Reference R = (Reference)(HttpContext.Current.Session["reference"]);
					/*
                    for (int i=0; i < dgrBezugsquellen.Columns.Count ; i++)
					{
						if (R.sBezugsquellenSort == dgrBezugsquellen.Columns[i].SortExpression || R.sBezugsquellenSort == dgrBezugsquellen.Columns[i].SortExpression + " DESC")
						{
							Label lblSorted = new Label();
							lblSorted.Font.Name = "webdings";
							lblSorted.Font.Size = FontUnit.XSmall;
							lblSorted.Text = sOrder;
							e.Item.Cells[i].Controls.Add(lblSorted);
							}
					}*/
					return;
				}
				//length
				string sText = e.Item.Cells[3].Text.ToString();
				if (sText.Length > 30)
				{
					e.Item.Cells[3].Text = sText.Substring(0,30)+ "...";
				}

				sText = e.Item.Cells[5].Text.ToString();
				if (sText.Length > 30)
				{
					e.Item.Cells[5].Text = sText.Substring(0,30)+ "...";
				}

				sText = e.Item.Cells[1].Text.ToString();
				if (sText.Length > 50){
					sText = sText.Substring(0,50) + "...";
				}
				if(Request.Params.Get("artID")!= null)
				{
					e.Item.Cells[1].Text = "<a href=\"dsp_bezugsquellen_detail.aspx?artID=" + Request.Params.Get("artID").ToString() + "&sosID=" + e.Item.Cells[0].Text.ToString() + "\" >" + sText + "</a>" ;
				}
				else{
					e.Item.Cells[1].Text = "<a href=\"dsp_bezugsquellen_detail.aspx?sosID=" + e.Item.Cells[0].Text.ToString() + "\" >" + sText + "</a>" ;				
				}

				DataSet dsCheckbox = Model.Bezugsquellen.getBezugsquellenKategorien(e.Item.Cells[0].Text.ToString());
				//set all checkboxes to OFF
				for (int i=6; i<17; i++)
				{
					e.Item.Cells[i].HorizontalAlign = HorizontalAlign.Center;
					e.Item.Cells[i].Text = "<IMG src=\"images/checkbox_off.gif\">";
				}
				//set checkboxes ON if find record in database
				for (int i=0; i<dsCheckbox.Tables[0].Rows.Count; i++)
				{
					switch(dsCheckbox.Tables[0].Rows[i].ItemArray[1].ToString())
					{
						case "1" : 
							e.Item.Cells[6].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "6" :
							e.Item.Cells[7].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "8" :
							e.Item.Cells[8].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "10" :
							e.Item.Cells[9].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "12" :
							e.Item.Cells[10].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "9" :
							e.Item.Cells[11].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "4" :
							e.Item.Cells[12].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "3" :
							e.Item.Cells[13].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "7" :
							e.Item.Cells[14].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "5" :
							e.Item.Cells[15].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
						case "11" :
							e.Item.Cells[16].Text = "<IMG src=\"images/checkbox_on.gif\">";
							break;
					}   
				}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}

		}

		protected void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				dgrBezugsquellen.CurrentPageIndex = 0;
				FillTable();
				dgrBezugsquellen.DataBind();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");
			}

		}

		public void FillTable()
		{
			try
			{
				string sWhere = "";
				//create "WHERE" string for select
				//add to "WHERE" string ARTIKLE_ID
				if (Request.Params.Get("artID") != null)
				{
					sWhere += " AND CSOSCA.ART_ID=" + Request.Params.Get("artID").ToString();
				}
				//add to "WHERE" string COUNTRY_ID
				if (cmbCountry.SelectedValue.ToString() != "0")
				{
					sWhere += " AND (CSOS.CNT_ID=" + cmbCountry.SelectedValue + " OR CSOSCY.CNT_ID="+ cmbCountry.SelectedValue+")";
				}
				//add to "WHERE" string (LIKE) NAME
				if (txtName.Text.ToString() != "")
				{
					sWhere += " AND CSOS.SOS_Name like '%" + txtName.Text.ToString() + "%' ";
				}
				//add to "WHERE" string DEALER or PROCESSOR
				
				if ((chkDealer.Checked == true && chkProcessor.Checked == true) || (chkDealer.Checked == true && chkAgent.Checked == true) || (chkProcessor.Checked == true && chkAgent.Checked == true) || (chkDealer.Checked == true && chkProcessor.Checked == true && chkAgent.Checked == true))														
				{
				}
				else
				{
					if (chkDealer.Checked == true)
					{
						sWhere += " AND CSOSTL.SST_ID = 1";
					}

					if (chkProcessor.Checked == true)
					{
						sWhere += " AND CSOSTL.SST_ID = 2";
					}
					if (chkAgent.Checked == true){
						sWhere += " AND CSOSTL.SST_ID = 3";
					}


				}

				sWhere += " AND CSOS.SOS_Parent_ID IS NULL";
				DataSet tblBezugsquellen = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);                
				if (tblBezugsquellen.Tables[0].Rows.Count == 0)
				{
					strError.Visible=true;
				}
				else{
					strError.Visible=false;
				}
                
				dgrBezugsquellen.DataSource = tblBezugsquellen;
				dgrBezugsquellen.DataBind();
                           
                DataSet dsSort = new DataSet ();
                dsSort = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);
                DataView vSort = new DataView(dsSort.Tables[0]);
                Reference R = (Reference)(HttpContext.Current.Session["reference"]);
                vSort.Sort = R.sBezugsquellenSort;    
                Session.Add("sortOrder",vSort.Sort.ToString());
                  
                dgrBezugsquellen.DataSource = vSort;
                dgrBezugsquellen.DataBind();                    
              
               // return sWhere;

					
				btnPrint.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
				//btnPrint.Attributes.Add("onClick", "javascript:openWindow('documents/reports.aspx?report=bezugsquellen&type=pdf&sWhere=" + sWhere + "', 'print', 650,675)"); 
				
				Session.Add("sWhere",sWhere);				

							
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");               
			}
            
		}

		private void dgrBezugsquellen_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			try
			{
				string sWhere = "";
				//create "WHERE" string for select
				//add to "WHERE" string ARTIKLE_ID
				if (Request.Params.Get("artID") != null)
				{
					sWhere += " AND CSOSCA.ART_ID=" + Request.Params.Get("artID").ToString();
				}
				//add to "WHERE" string COUNTRY_ID
				if (cmbCountry.SelectedValue.ToString() != "0")
				{
					sWhere += " AND CSOS.CNT_ID=" + cmbCountry.SelectedValue;
				}
				//add to "WHERE" string (LIKE) NAME
				if (txtName.Text.ToString() != "")
				{
					sWhere += " AND CSOS.SOS_Name like '%" + txtName.Text.ToString() + "%' ";
				}
				//add to "WHERE" string DEALER or PROCESSOR
				if ((chkDealer.Checked == true && chkProcessor.Checked == true) || (chkDealer.Checked == true && chkAgent.Checked == true) || (chkProcessor.Checked == true && chkAgent.Checked == true) || (chkDealer.Checked == true && chkProcessor.Checked == true && chkAgent.Checked == true)) {
				}
				else {
					if (chkDealer.Checked == true) {
						sWhere += " AND CSOSTL.SST_ID = 1";
					}

					if (chkProcessor.Checked == true) {
						sWhere += " AND CSOSTL.SST_ID = 2";
					}
					if (chkAgent.Checked == true){
						sWhere += " AND CSOSTL.SST_ID = 3";
					}


				}

				sWhere += " AND CSOS.SOS_Parent_ID IS NULL";
				
				DataSet dsSort = new DataSet ();
				dsSort = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);
				DataView vSort = new DataView(dsSort.Tables[0]);
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				if (R.sBezugsquellenSort == e.SortExpression.ToString())
				{
					vSort.Sort = e.SortExpression.ToString() + " DESC";
					sOrder = "5";
				}
				else
				{
					vSort.Sort = e.SortExpression.ToString();
					sOrder = "6";
				}
				R.sBezugsquellenSort = vSort.Sort;
				
				Session.Add("sortOrder",vSort.Sort.ToString());
				dgrBezugsquellen.DataSource = vSort;
				dgrBezugsquellen.DataBind();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		protected void txtName_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				dgrBezugsquellen.CurrentPageIndex = 0;
				FillTable();
				dgrBezugsquellen.DataBind();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		protected void dgrBezugsquellen_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		private void OnItemCreated(object sender, DataGridItemEventArgs e) {
			if(e.Item.ItemType==ListItemType.Pager) {
				Label lblPager = new Label();
				lblPager.Text = "<b>" + getString("page",false)+"</b> " + lblPager.Text;
				e.Item.Controls[0].Controls.AddAt(0,lblPager);
			}
		}
		protected void btnPrint_Click(object sender, System.Web.UI.ImageClickEventArgs e) {
			Response.Redirect("documents/reports.aspx?report=bezugsquellen&type=pdf");
		}

	}
}
