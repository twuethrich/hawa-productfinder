<%@ Page CodeBehind="acc_frm_main.aspx.cs" Language="c#" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.acc_frm_main" enableViewState="True" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING id="lblTitle" runat="server" Key="PageTitle"></ALPHA:STRING></title>
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<SCRIPT language="JavaScript" src="jscript/general.js" type="text/javascript"></SCRIPT>

			<script>
				function showChild(id){
					var child = id;
					var x = document.getElementsByTagName('tr');
						if(document.getElementById(child).style.display == "none")
						{
							for (var i=0;i<x.length;i++)
							{
								if(x[i].id == child){
									x[i].style.display="";
									
								}
								
							}
						}else{
							
							for (var i=0;i<x.length;i++)
							{
								if(x[i].id == child){
									x[i].style.display="none";
								}
								
							}
						
						}
					}
				
				
			</script>
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<form id="Form1" name="detail" runat="server">
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td></tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING id="String1" runat="server" Key="DisplayTitle"></ALPHA:STRING></td>
								<td></td></tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td></tr>
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnFr" RUNAT="server">Fran�ais</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEs" RUNAT="server">Espa�ol</ASP:HYPERLINK>
								<td></td></tr></table>
						<!-- Title & Language, start--></td></tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30" class="TopTitle"><ALPHA:STRING id="String2" runat="server" Key="TipText"></ALPHA:STRING></td>
					<td align="right" style="padding-right:10px"><asp:hyperlink id="btn_back" runat="server" ImageUrl="images/btn_back_de.gif">HyperLink</asp:hyperlink><A href="javascript:window.close();"></A></td></tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="1" border="0"></td></tr></table>
			<!-- header, end-->
			<table borderColor="red" cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td width="10"><IMG src="images/ts.gif" width="10"></td>
					<td width="435">
						<!-- Begin SearchList -->
						<table borderColor="blue" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="100%" colSpan="3">
									<DIV id="access" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 540px; scrollbar: ">
									<ASP:DATAGRID id="dgrAccessories" RUNAT="server" WIDTH="100%" BORDERCOLOR="White" BORDERSTYLE="Solid" BORDERWIDTH="1px" CELLPADDING="0" AUTOGENERATECOLUMNS="False" ONITEMDATABOUND="OnDataBound" CssClass="BoxGrayBright">
											<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999">
											</SelectedItemStyle>

											<AlternatingItemStyle Height="20px" CssClass="BoxGrayBright" VerticalAlign="Middle">
											</AlternatingItemStyle>

											<HeaderStyle Font-Size="20px" ForeColor="Black" Height="20">
											</HeaderStyle>

											<Columns>
												<asp:BoundColumn DataField="AGL_Name" HeaderText="AGL_Name">
													<ItemStyle Wrap="False" CssClass="TableText">
													</ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" HeaderText="OBJ_Filename">
													<ItemStyle CssClass="ImageBorder" BackColor="White">
													</ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="AGR_ID" HeaderText="AGR_ID">
													<ItemStyle HorizontalAlign="Right">
													</ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="AGR_ParentID" HeaderText="AGR_ParentID"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" HeaderText="OBJ_ID"></asp:BoundColumn>
												<asp:HyperLinkColumn Visible="False" DataTextField="AGL_Name" HeaderText="AGL_Name" NavigateUrl="acc_frm_main.aspx"></asp:HyperLinkColumn>
												<asp:BoundColumn Visible="False" HeaderText="OBJ_NAME"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Material">
													<HeaderStyle HorizontalAlign="Left" Width="50px">
													</HeaderStyle>

													<ItemStyle HorizontalAlign="Left" Width="50px" CssClass="TableText" VerticalAlign="Bottom">
													</ItemStyle>

													<ItemTemplate>
														<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">
															<TR>
																<TD NOWRAP>
																	<ASP:IMAGE ID="holz" IMAGEURL="images/pic_material1.gif" RUNAT="server" VISIBLE="false" WIDTH="15" height="15"></ASP:IMAGE>
																	<ASP:IMAGE ID="glas" IMAGEURL="images/pic_material2.gif" RUNAT="server" VISIBLE="false" width="15" HEIGHT="15"></ASP:IMAGE>
																	<ASP:IMAGE ID="metall" IMAGEURL="images/pic_material3.gif" RUNAT="server" VISIBLE="false" width="15" HEIGHT="15"></ASP:IMAGE>
																</TD>
															</TR>
														</TABLE>

													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="Holz"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Glas"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Metall"></asp:BoundColumn>
												<asp:TemplateColumn Visible="False" HeaderText="Merkmale">
													<HeaderStyle HorizontalAlign="Center">
													</HeaderStyle>

													<ItemStyle HorizontalAlign="Center" Width="15px" CssClass="TableText" VerticalAlign="Bottom">
													</ItemStyle>

													<ItemTemplate>
														<ASP:LABEL RUNAT="server" ID="info"></ASP:LABEL>

													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="hasChild"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="hasParent"></asp:BoundColumn>
											</Columns>
										</ASP:DATAGRID></DIV></TD></TR></table></td>
					<td width="10"><IMG src="images/ts.gif" width="10"></td>
					<td id="space" width="435" runat="server"><IMG src="images/ts.gif" width="10"></td>
					<td id="tabRight" vAlign="top" width="435" runat="server">
						<table borderColor="blue" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr vAlign="middle">
								<td class="BoxTitle" vAlign="middle" colSpan="2"><asp:label id="Label1" runat="server">Label</asp:label></td>
							</tr>
						</table>
						<table class="BoxGrayBright" borderColor="blue" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr vAlign="top">
								<td width="10" height="10"><IMG height="10" src="images/ts.gif" width="10"></td>
								<td width="179"></td>
								<td width="10" height="10"><IMG height="10" src="images/ts.gif" width="10"></td></tr>
							<tr vAlign="top">
								<td></td>
								<td width="180">
								    <asp:panel id="Panel1" runat="server" Width="180px" Height="180px" BackColor="#ffffff">
                                            <asp:Image id="imgSymbol" runat="server" Width="180" Height="180"></asp:Image>
									</asp:panel>
							    </td>
								<td width="100%" >
									<!-- start download area -->
									<TABLE class="BoxGrayBright" id="download" cellSpacing="0" cellPadding="0" border="0" runat="server">
										<TR >
											<TD vAlign="top" colSpan="3" class="BoxSubTitle"><B><ALPHA:STRING id="String8" RUNAT="server" KEY="tit_document"></ALPHA:STRING></B></TD></TR>
										<TR>
											<TD><IMG src="images/ts.gif" width="10"></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top" align="left" width="100%"><ASP:HYPERLINK id="Bestellangaben" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String5" RUNAT="server" KEY="Bestellangaben"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR>
											<TD></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="Planung" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String10" RUNAT="server" KEY="Planung_Montage"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR>
											<TD height="15"></TD>
											<TD height="15"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top" height="15"><ASP:HYPERLINK id="Prospekte" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String13" RUNAT="server" KEY="Prospekte"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR >
											<TD vAlign="top" colSpan="3" class="BoxSubTitle"><B><ALPHA:STRING id="String16" RUNAT="server" KEY="tit_download"></ALPHA:STRING></B></TD></TR>
										<TR>
											<TD></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="download_dfx" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String20" RUNAT="server" KEY="dxf"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR>
											<TD></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="download_eps" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String21" RUNAT="server" KEY="eps"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR>
											<TD vAlign="top" colSpan="3" class="BoxSubTitle"><B><ALPHA:STRING id="String23" RUNAT="server" KEY="tit_vip"></ALPHA:STRING></B></TD></TR>
										<TR>
											<TD></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="viproom" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String24" RUNAT="server" KEY="viproom"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR>
										<TR>
											<TD></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="Preise" RUNAT="server" NAVIGATEURL="">
													<ALPHA:STRING ID="String25" RUNAT="server" KEY="Preise"></ALPHA:STRING>
												</ASP:HYPERLINK></TD></TR></TABLE>
									<TABLE class="BoxGrayBright" id="systemDetail" cellSpacing="0" cellPadding="0" border="0" runat="server">
										<TR>
											<TD><IMG src="images/ts.gif" width="10"></TD>
											<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
											<TD vAlign="top"><ASP:HYPERLINK id="lnkDetail" RUNAT="server" NAVIGATEURL="">Name</ASP:HYPERLINK></TD></TR></TABLE></td></tr>
							<tr vAlign="top">
								<td colSpan="3" height="10"><IMG height="10" src="images/ts.gif" width="10"></td></tr></table>
						<table id="sysSpace" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
							<tr vAlign="top">
								<td colSpan="3" height="10"><IMG height="10" src="images/ts.gif" width="10"></td></tr>
						</table>



						<table class="BoxGrayBright" id="trRelSystem" borderColor="blue" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
							<tr vAlign="middle">
								<td class="BoxTitle" colSpan="3"><ALPHA:STRING id="String6" runat="server" Key="relSystems"></ALPHA:STRING></td></tr>
							<tr vAlign="top">
								<td rowspan="2"><IMG height="5" src="images/ts.gif" width="7"></td>
								<td colSpan="2"><IMG height="5" src="images/ts.gif" width="10"></td></tr>
							<tr vAlign="top">


								<td vAlign="top">
									<DIV id="relSys" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 270px; scrollbar: ">
										<asp:datagrid id="dgrRelevantSystem" runat="server" ONITEMDATABOUND="OnDataBoundRelevantSystem" Width="100%" PageSize="99" AutoGenerateColumns="False" CellPadding="2" BorderColor="#DEDEDE" BORDERWIDTH="0px"  ShowHeader="False">
											<Columns>
												<asp:TemplateColumn>
													<ItemStyle Width="30px"></ItemStyle>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="ART_ID" SortExpression="ART_ID" HeaderText="ART_ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="ARL_Name" SortExpression="ARL_Name" HeaderText="ARL_Name"></asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</DIV>
								</td>


								<td></td>
							</tr>
							<tr vAlign="top">
								<td colSpan="3" height="10"><IMG height="10" src="images/ts.gif" width="10"></td></tr></table></td></tr></table></form>
								 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
