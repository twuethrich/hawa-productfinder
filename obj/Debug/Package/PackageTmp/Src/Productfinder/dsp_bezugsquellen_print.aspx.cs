using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r dsp_bezugsquellen_print.
	/// </summary>
	/// 


	public partial class dsp_bezugsquellen_print : Alpha.Page
	{
		public static DataTable tabList;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			//btnPrint.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
			//btnPrint.Attributes.Add("onClick", "javascript:window.print()"); 

			//btnPrint.ImageUrl="images/btn_print_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
			//btnClose.ImageUrl="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";


			//btnClose.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
			//btnClose.Attributes.Add("onClick", "javascript:window.close()"); 

			dgrBezugsquellen.Columns[1].HeaderText = getString("firma",true) + " / "+getString("art",true);
			
			dgrBezugsquellen.Columns[3].HeaderText = getString("strasse",true);
			dgrBezugsquellen.Columns[4].HeaderText = getString("plz",true);
			dgrBezugsquellen.Columns[5].HeaderText = getString("ort",true);
			dgrBezugsquellen.Columns[17].HeaderText = getString("land",true);

			FillTable();
			
			// Hier Benutzercode zur Seiteninitialisierung einf�gen
		}

		public void FillTable() {
			try {
				string sWhere = "";
			
				if (Request.Params.Get("sWhere") != null) {
					sWhere = Request.Params.Get("sWhere").ToString();
				}
			
				DataSet tblBezugsquellen = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);                
				dgrBezugsquellen.DataSource = tblBezugsquellen;
				dgrBezugsquellen.DataBind();
                
			
				
			    
			}
			catch (Exception ex) {
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				Response.Redirect("dsp_error_page.aspx");               
			}
            
		}

			#region Vom Web Form-Designer generierter Code
			override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgrBezugsquellen.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrBezugsquellen_ItemDataBound);

		}
		#endregion

		private void dgrBezugsquellen_ItemDataBound(object sender, DataGridItemEventArgs e) {
			
			ListItemType itemType = (ListItemType)e.Item.ItemType;

			if(itemType != ListItemType.Header)
			{
				e.Item.Cells[1].Text = e.Item.Cells[1].Text.ToString() + " <b>(" + e.Item.Cells[2].Text.ToString() +")</b>";
			}
			
			
		}
	}
}
