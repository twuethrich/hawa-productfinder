using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for dsp_referenzen.
	/// </summary>
	public partial class dsp_referenzen : System.Web.UI.Page
	{
		protected System.Web.UI.HtmlControls.HtmlForm frmReference;
		protected System.Web.UI.WebControls.Label lblShort;
		protected System.Web.UI.WebControls.ImageButton imgReset;
		protected Alpha.Controls.String tipText;
		public string strScript = "";


		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				btnEn.NavigateUrl = Request.Path + "?languageID=En";
				btnDe.NavigateUrl = Request.Path + "?languageID=De";
				btnFr.NavigateUrl = Request.Path + "?languageID=Fr";
                this.btnES.NavigateUrl = Request.Path + "?languageID=Es";
				
				btn_close.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				btnSearch.ImageUrl ="images/btn_search2_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				
				int langID = 0;
				if(HttpContext.Current.Session["languageID"].ToString() == "EN"){
					langID=1;
				}
				if(HttpContext.Current.Session["languageID"].ToString() == "FR"){
					langID=2;
				}
                if (HttpContext.Current.Session["languageID"].ToString() == "ES")
                {
                    langID = 3;
                    this.btnReset.ImageUrl = "images/btn_reset_es.gif";
                }

				//Feedback Referenzen
				string refURL = System.Configuration.ConfigurationSettings.AppSettings["RefFeedbackURL"]+"&L="+langID;
				Feedback.Attributes.Add("onClick", "javascript:opener.location=('" + refURL + "'); opener.focus()"); 
				Feedback.NavigateUrl = "#";
				
							
				if (!Page.IsPostBack) { 
					Session.Add("selBuildingType", 0);
					Session.Add("selCountry", 0);
					Session.Add("selSystem", 0);
					FillcmbCountry();
					FillcmbBuildingType();
					FillcmbSystem();
					FillplPlan();
					FillplMaterial();


					
				}

              
				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}


		

		//fill combo country
		public void FillcmbCountry()
		{
			try
			{
				//if (cmbCountry.Items.Count == 0) {

					int artID = Convert.ToInt32(HttpContext.Current.Session["selSystem"]);
					int buildTypeID = Convert.ToInt32(HttpContext.Current.Session["selBuildingType"]);
					cmbCountry.Items.Clear();
				
					DataSet dsCountry = new DataSet();
					dsCountry = Model.Country.getCountryReference(buildTypeID,artID);
					cmbCountry.Items.Add(Alpha.Global.getString("cmbAll1").ToString());
					
					cmbCountry.Items[0].Value = "0";

					for(int i=0; i < dsCountry.Tables[0].Rows.Count; i++)
					{
						cmbCountry.Items.Add(dsCountry.Tables[0].Rows[i].ItemArray[1].ToString());	  
						cmbCountry.Items[i+1].Value = (dsCountry.Tables[0].Rows[i].ItemArray[0].ToString());	  
					}
					
				//}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//fill combo type of building
		public void FillcmbBuildingType()
		{
			try {
				//if (cmbBuildingType.Items.Count == 0)
				//{
					
		
                    int country = Convert.ToInt32(HttpContext.Current.Session["selCountry"]);
					int artID = Convert.ToInt32(HttpContext.Current.Session["selSystem"]);
					cmbBuildingType.Items.Clear();
					
					DataSet dsBuildingType = new DataSet();
					dsBuildingType = Model.BuildingType.getBuildingType(country,artID);
					
					cmbBuildingType.Items.Add(Alpha.Global.getString("cmbAll1").ToString());
										
					cmbBuildingType.Items[0].Value = "0";
					for(int i=0; i < dsBuildingType.Tables[0].Rows.Count; i++)
					{
						cmbBuildingType.Items.Add(dsBuildingType.Tables[0].Rows[i].ItemArray[3].ToString());	  
						cmbBuildingType.Items[i+1].Value = (dsBuildingType.Tables[0].Rows[i].ItemArray[1].ToString());	  
					}
				//}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString(); 
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//fill combo system
		public void FillcmbSystem()
		{
			try
			{
				//if (cmbSystem.Items.Count == 0)
				//{

					int country = Convert.ToInt32(HttpContext.Current.Session["selCountry"]);
					int buildTypeID = Convert.ToInt32(HttpContext.Current.Session["selBuildingType"]);
					cmbSystem.Items.Clear();


					DataSet dsReferenceSystem = new DataSet();
					dsReferenceSystem = Model.ReferenceSystem.getReferenceSystem(country,buildTypeID);
					cmbSystem.Items.Add(Alpha.Global.getString("cmbAll2").ToString());
								
					cmbSystem.Items[0].Value = "0";

					for(int i=0; i < dsReferenceSystem.Tables[0].Rows.Count; i++)
					{
						cmbSystem.Items.Add(dsReferenceSystem.Tables[0].Rows[i].ItemArray[1].ToString());	  
						cmbSystem.Items[i+1].Value = (dsReferenceSystem.Tables[0].Rows[i].ItemArray[0].ToString());	  
					}
				//}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}
		
		//create checkbox for plan
		public void FillplPlan()
		{
			try
			{
				int country = Convert.ToInt32(HttpContext.Current.Session["selCountry"]);
				int buildTypeID = Convert.ToInt32(HttpContext.Current.Session["selBuildingType"]);
				int artID = Convert.ToInt32(HttpContext.Current.Session["selSystem"]);

				DataSet dsReferencePlan = new DataSet();
				dsReferencePlan = Model.Reference.getReferencePlan(country,buildTypeID,artID);
				
				

				plPlan.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 width=300>"));
				for(int i=0; i < Global.M.iPLANES.Count();i++)
				{
					System.Web.UI.WebControls.CheckBox chkPlan = new System.Web.UI.WebControls.CheckBox();
					System.Web.UI.WebControls.CheckBox chkPlan1 = new System.Web.UI.WebControls.CheckBox();
				
					Plan p = (Plan)Global.M.iPLANES.getItem(i);
					chkPlan.ID = "chkPla_"+ p.iPLAN_ID;
					chkPlan.Text = p.getPLL_Name((String)HttpContext.Current.Session["LanguageID"]);

                    plPlan.Controls.Add(new LiteralControl("<tr><td style=\"width:160px;vertical-align:top;\" nowrap>"));
					plPlan.Controls.Add(chkPlan);
					

					i++;
					chkPlan.Enabled = false;
		
					if (i < Global.M.iPLANES.Count())
					{
						Plan p1 = (Plan)Global.M.iPLANES.getItem(i);
						chkPlan1.ID = "chkPla_"+ p1.iPLAN_ID;
						chkPlan1.Text = p1.getPLL_Name((String)HttpContext.Current.Session["LanguageID"]);
						plPlan.Controls.Add(new LiteralControl("</td><td nowrap>"));
						plPlan.Controls.Add(chkPlan1);
						chkPlan1.Enabled = false;
						
						
					
						for(int k=0; k < dsReferencePlan.Tables[0].Rows.Count; k++) {
							if(dsReferencePlan.Tables[0].Rows[k].ItemArray[0].ToString().Equals(p1.iPLAN_ID.ToString())){
								chkPlan1.Enabled = true;
								
							}
						}

					}
					else 
					{
						plPlan.Controls.Add(new LiteralControl("</td><td>"));
					}
					plPlan.Controls.Add(new LiteralControl("</td></tr>"));

					for(int k=0; k < dsReferencePlan.Tables[0].Rows.Count; k++) {
						if(dsReferencePlan.Tables[0].Rows[k].ItemArray[0].ToString().Equals(p.iPLAN_ID.ToString())){
							chkPlan.Enabled = true;
							
						}
									
					}

				}
				plPlan.Controls.Add(new LiteralControl("</table>"));
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//fill combo for material
		public void FillplMaterial()
		{
			//try
			//{
				//get available material
				int country = Convert.ToInt32(HttpContext.Current.Session["selCountry"]);
				int buildTypeID = Convert.ToInt32(HttpContext.Current.Session["selBuildingType"]);
				int artID = Convert.ToInt32(HttpContext.Current.Session["selSystem"]);
				int planID = 0;


				DataSet dsReferenceMaterial = new DataSet();
				dsReferenceMaterial = Model.Reference.getReferenceMaterial(country,buildTypeID,artID,planID);
				

				plMaterial.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 width=300>"));
				for(int i=0; i < Global.M.iMaterials.Count();i++)
				{
					string strLng = HttpContext.Current.Session["LanguageID"].ToString();
					System.Web.UI.WebControls.CheckBox chkMaterial = new System.Web.UI.WebControls.CheckBox();
					System.Web.UI.WebControls.CheckBox chkMaterial1 = new System.Web.UI.WebControls.CheckBox();
				
					Material m = (Material)Global.M.iMaterials.getItem(i);
					chkMaterial.ID = "chkMaterial_"+ m.iMAT_ID ;
					chkMaterial.Text = m.getMTL_Name(strLng); 
					plMaterial.Controls.Add(new LiteralControl("<tr><td width=\"160px\">"));
					plMaterial.Controls.Add(chkMaterial);

					i++;
					
					chkMaterial.Enabled = false;
					
					
					if (i < Global.M.iMaterials.Count())
					{
						Material m1 = (Material)Global.M.iMaterials.getItem(i);
						chkMaterial1.ID = "chkMaterial_"+ m1.iMAT_ID;
						chkMaterial1.Text = m1.getMTL_Name(strLng);
						plMaterial.Controls.Add(new LiteralControl("</td><td>"));
						plMaterial.Controls.Add(chkMaterial1);
						chkMaterial1.Enabled = false;

						for(int k=0; k < dsReferenceMaterial.Tables[0].Rows.Count; k++) {
								if(dsReferenceMaterial.Tables[0].Rows[k].ItemArray[0].ToString().Equals(m1.iMAT_ID.ToString())){
								chkMaterial1.Enabled = true;
							}
						}
					}
					else 
					{
						plMaterial.Controls.Add(new LiteralControl("</td><td>"));
					}
					plMaterial.Controls.Add(new LiteralControl("</td></tr>"));
					
					

					for(int k=0; k < dsReferenceMaterial.Tables[0].Rows.Count; k++) {
						if(dsReferenceMaterial.Tables[0].Rows[k].ItemArray[0].ToString().Equals(m.iMAT_ID.ToString())){
							chkMaterial.Enabled = true;
						}
							
					
					}

				}
				plMaterial.Controls.Add(new LiteralControl("</table>"));
			/*}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}*/
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		//search button
		//create "WHERE" string for search
		protected void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			string strWhere = "";
			string strPlan = "";
			string strMaterial = "";

			try
			{
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				if(R == null)
				{
					R = new Reference();
					Session.Add("reference", R);
				}
				//check which PLAN checkbox are checked
				for(int i=0; i < Global.M.iPLANES.Count(); i++)
				{				
					Plan p = (Plan)Global.M.iPLANES.getItem(i);
					//strWhere = ((CheckBox)(Controls[i])).Checked.ToString();
					string strRequest = Request.Params.Get("chkPla_"+p.iPLAN_ID.ToString());
					if (strRequest != null && strRequest.ToString() == "on")
					{
						if (strPlan != "") strPlan += ",";
						strPlan += p.iPLAN_ID.ToString();
					}
				}
				//check which MATERIAL checkbox are checked
				for(int i=0; i < Global.M.iMaterials.Count(); i++)
				{				
					Material m = (Material)Global.M.iMaterials.getItem(i);
					//strWhere = ((CheckBox)(Controls[i])).Checked.ToString();
					string strRequest = Request.Params.Get("chkMaterial_"+m.iMAT_ID.ToString());
					if (strRequest != null && strRequest.ToString() == "on")
					{
						if (strMaterial != "") strMaterial += ",";
						strMaterial += m.iMAT_ID.ToString();
					}
				}
			
				R.iCountry = 0;
				R.iBuild = 0;
				R.iSystem = 0;
				R.sMaterial = "";
				R.sPlan = "";
				R.sOrt = "";
				R.sObject = "";
				//create "WHERE" string for COUNTRY
				if (Convert.ToInt32(Request.Params.Get("cmbCountry")) != 0)
				{
					strWhere += " AND CR.CNT_ID=" + Request.Params.Get("cmbCountry").ToString();
					R.iCountry = Convert.ToInt32(Request.Params.Get("cmbCountry"));
				}
				//create "WHERE" string for type of BUILDING
				if (Convert.ToInt32(Request.Params.Get("cmbBuildingType")) != 0)
				{
					strWhere += " AND CR.RBT_ID=" + Request.Params.Get("cmbBuildingType").ToString();
					R.iBuild = Convert.ToInt32(Request.Params.Get("cmbBuildingType"));
				}
				//create "WHERE" string for SYSTEM
				if (Convert.ToInt32(Request.Params.Get("cmbSystem")) != 0)
				{
					strWhere += " AND CAA.ART_ID=" + Request.Params.Get("cmbSystem").ToString();
					R.iSystem = Convert.ToInt32(Request.Params.Get("cmbSystem"));
				}
				//create "WHERE" string for ORT
				if (Request.Params.Get("txtOrt").ToString() != "")
				{
					strWhere += " AND CRL.REL_Place LIKE '%" + Request.Params.Get("txtOrt").ToString() + "%'" ;
					R.sOrt = Request.Params.Get("txtOrt").ToString();
				}
				//create "WHERE" string for OBJECT
				if (Request.Params.Get("txtObject").ToString() != "")
				{
					strWhere += " AND CRL.REL_Object LIKE '%" + Request.Params.Get("txtObject").ToString() + "%'" ;
					R.sObject = Request.Params.Get("txtObject").ToString();
				}
				//create "WHERE" string for MATERIAL
				if (strMaterial != "")
				{
					strWhere += " AND CAR.MAT_ID in (" + strMaterial + ")";
					R.sMaterial = strMaterial;
				}
				//create "WHERE" string for PLAN
				if (strPlan != "")
				{
					strWhere += " AND CAR.PLA_ID in (" + strPlan + ")";
					R.sPlan = strPlan;
				}
				//save strWhere
				R.strWhere = strWhere;	
				
				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
				//open page dsp_referenzen_search.aspx	
				
				Response.Redirect("dsp_referenzen_search.aspx");
		}

		protected void cmbBuildingType_SelectedIndexChanged(object sender, EventArgs e) {
			Session.Add("selBuildingType", cmbBuildingType.SelectedValue);
			//FillcmbCountry();
			//FillcmbBuildingType();
			FillcmbSystem();
			FillplPlan();
			FillplMaterial();


		}

		protected void cmbCountry_SelectedIndexChanged(object sender, EventArgs e) {
			Session.Add("selCountry", cmbCountry.SelectedValue);
			//FillcmbCountry();
			FillcmbBuildingType();
			FillcmbSystem();
			FillplPlan();
			FillplMaterial();
		}

		protected void cmbSystem_SelectedIndexChanged(object sender, EventArgs e) {
			Session.Add("selSystem", cmbSystem.SelectedValue);
			//FillcmbCountry();
			//FillcmbBuildingType();
			//FillcmbSystem();
			FillplPlan();
			FillplMaterial();

		}

		protected void btnReset_Click(object sender, ImageClickEventArgs e) {
			Session.Add("selBuildingType", 0);
			Session.Add("selCountry", 0);
			Session.Add("selSystem", 0);
			txtOrt.Text = "";
			txtObject.Text ="";
			FillcmbCountry();
			FillcmbBuildingType();
			FillcmbSystem();
			FillplPlan();
			FillplMaterial();

		}

		private void Plan_Clicked(object sender, EventArgs e) {
			FillplPlan();
			FillplMaterial();
		}
	}
}
