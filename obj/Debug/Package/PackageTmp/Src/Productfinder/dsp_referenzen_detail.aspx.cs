using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for dsp_referenzen_detail.
	/// </summary>
	public partial class dsp_referenzen_detail : System.Web.UI.Page
	{
		protected Alpha.Controls.String tipText;
		protected System.Web.UI.WebControls.Label lblDescription1;
		public static DataTable tabRefDetail;

		//fill all object on formular with data
		protected void Page_Load(object sender, System.EventArgs e)
		{			
			try
			{
				btnBack.ImageUrl ="images/btn_back_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				R.strWhereDetail = " AND CR.REF_ID = " + Request.Params.Get("refID").ToString();
				DataTable dtReferenceDetail = Model.Reference.getReferenceSearch(R.strWhereDetail).Tables[0];
				lblShort.Text = "<b>" + dtReferenceDetail.Rows[0].ItemArray[6].ToString() +  " - " + dtReferenceDetail.Rows[0].ItemArray[7].ToString()+ " - " + dtReferenceDetail.Rows[0].ItemArray[8].ToString()+ "</b>";
                Header.Title = "Hawa Referenzen: " +dtReferenceDetail.Rows[0].ItemArray[6].ToString() + ", " + dtReferenceDetail.Rows[0].ItemArray[7].ToString() + " (" + dtReferenceDetail.Rows[0].ItemArray[8].ToString() + ")";
                lblProduct.Text = "";  	
				//einzelne Produkte einf�gen
				foreach( DataRow row in dtReferenceDetail.Rows)
				{
                     lblProduct.Text = lblProduct.Text + (string)row[5] + "<br>";  	
				}
					
				//lblProduct.Text = "<a href=\"javascript:opener.location.href='dsp_detail.aspx?artID=" + Request.Params.Get("artID").ToString() + "',window.close()\" class=\"detail\" style=\"font-size:14px\">" + dtReferenceDetail.Rows[0].ItemArray[5].ToString() + "</a>";  	
				lblDescription.Text = dtReferenceDetail.Rows[0].ItemArray[11].ToString();  	
				lblArchitect.Text = dtReferenceDetail.Rows[0].ItemArray[9].ToString();  	
				lblRealisator.Text = dtReferenceDetail.Rows[0].ItemArray[10].ToString();
				imgRef.ImageUrl = "dsp_objectdownload.aspx?typ=9&id="+ dtReferenceDetail.Rows[0]["REF_FileGUID"].ToString();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					Reference R = (Reference)(HttpContext.Current.Session["reference"]);
					E.sParam = "sWhere=" +  R.strWhere;
				}
				catch
				{
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
