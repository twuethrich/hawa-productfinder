<%@ Page language="c#" Codebehind="dsp_error_page.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_error_page" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>�Hawa� - Error page</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN: 0px">
		<form id="Form1" method="post" runat="server">
			<table height="34" cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<!-- 
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>
								<td></td>
							</tr>--></table>
						<!-- Title & Language, start--></td>
				</tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30"><asp:label id="lblError" runat="server" CssClass="Short">Error message</asp:label><IMG height="10" src="images/ts.gif" width="10"></td>
					<td vAlign="middle" align="right">
						<IMG height="1" alt="" src="images/ts.gif" width="2" border="0"><A href="javascript: history.back(-1)"><asp:image id="btnBack" IMAGEURL="images/btn_back_de.gif" Runat="server"></asp:image></A></td>
				</tr>
				<tr>
					<td colSpan="3" height="10"><IMG height="1" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td width="10">&nbsp;</td>
					<td>
						<table cellSpacing="0" cellPadding="0" width="880" border="0">
							<tr>
								<td class="FileNotFound" colSpan="2" height="15"></td>
							</tr>
							<tr>
								<td class="FileNotFound" height="15"><asp:label id="lblErrorText" runat="server" CssClass="FileNotFound">Error message</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2" height="5"></td>
							</tr>
							<!--
							<tr>
								<td class="PRODUCTTEXT" colSpan="2" height="15"><asp:label id="lblStack" runat="server">Error message</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2" height="5"></td>
							</tr>
							<tr>
								<td class="PRODUCTTEXT" colSpan="2" height="15"><asp:label id="lblParam" runat="server" DESIGNTIMEDRAGDROP="488">Parameters:</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2" height="30"></td>
							</tr>
							<tr>
								<td class="PRODUCTTEXT" colSpan="2" height="15"><b>Send this error message to 
										administrator</b></td>
							</tr>
							-->
							<tr>
								<td colSpan="2" height="30"></td>
							</tr>
							<tr>
								<td colSpan="2" height="15" >Hawa AG</td>
							</tr>
							<tr>
								<td colSpan="2" height="15"><asp:label id="lblPhone" runat="server" >Phone: +41 44 767 91 91</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2" height="15"><asp:label id="lblFax" runat="server">Fax: +41 44 767 91 78</asp:label></td>
							</tr>
							<tr vAlign="top">
								<td height="15"><asp:label id="lblEmail" runat="server" ></asp:label> <A href="mailto:info@hawa.ch">info@hawa.ch</A></td>
								<td vAlign="middle" align="right" rowsapn="2"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
