﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace Hawa.Src.Productfinder {
    
    
    public partial class dsp_detail {
        
        /// <summary>
        /// Header-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Header;
        
        /// <summary>
        /// String2-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String2;
        
        /// <summary>
        /// btnDe-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnDe;
        
        /// <summary>
        /// btnEn-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnEn;
        
        /// <summary>
        /// btnFr-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnFr;
        
        /// <summary>
        /// btnEs-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnEs;
        
        /// <summary>
        /// btnBack-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnBack;
        
        /// <summary>
        /// lblProductName-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblProductName;
        
        /// <summary>
        /// imgProductPicture-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgProductPicture;
        
        /// <summary>
        /// lblShort-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblShort;
        
        /// <summary>
        /// ZumProdukt-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String ZumProdukt;
        
        /// <summary>
        /// lblProduct-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblProduct;
        
        /// <summary>
        /// plTypisch-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel plTypisch;
        
        /// <summary>
        /// String1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String1;
        
        /// <summary>
        /// lblProductN-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblProductN;
        
        /// <summary>
        /// typisch-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell typisch;
        
        /// <summary>
        /// lblTypisch-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTypisch;
        
        /// <summary>
        /// spacer-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow spacer;
        
        /// <summary>
        /// plAnwendung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel plAnwendung;
        
        /// <summary>
        /// anwendung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell anwendung;
        
        /// <summary>
        /// Anwendungsbereich-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String Anwendungsbereich;
        
        /// <summary>
        /// lblAnwendung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAnwendung;
        
        /// <summary>
        /// String6-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String6;
        
        /// <summary>
        /// String8-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String8;
        
        /// <summary>
        /// Bestellangaben-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Bestellangaben;
        
        /// <summary>
        /// String5-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String5;
        
        /// <summary>
        /// Planung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Planung;
        
        /// <summary>
        /// String10-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String10;
        
        /// <summary>
        /// Berechnungshilfe-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Berechnungshilfe;
        
        /// <summary>
        /// String11-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String11;
        
        /// <summary>
        /// Zertifikate-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Zertifikate;
        
        /// <summary>
        /// String12-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String12;
        
        /// <summary>
        /// Prospekte-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Prospekte;
        
        /// <summary>
        /// String13-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String13;
        
        /// <summary>
        /// Ausschreibungstexte-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Ausschreibungstexte;
        
        /// <summary>
        /// String9-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String9;
        
        /// <summary>
        /// String3-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String3;
        
        /// <summary>
        /// Bezugsquellen-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Bezugsquellen;
        
        /// <summary>
        /// String14-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String14;
        
        /// <summary>
        /// Referenzen-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Referenzen;
        
        /// <summary>
        /// String15-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String15;
        
        /// <summary>
        /// Presse-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Presse;
        
        /// <summary>
        /// String17-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String17;
        
        /// <summary>
        /// FAQ-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink FAQ;
        
        /// <summary>
        /// String18-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String18;
        
        /// <summary>
        /// String4-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String4;
        
        /// <summary>
        /// download_dfx-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink download_dfx;
        
        /// <summary>
        /// String20-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String20;
        
        /// <summary>
        /// download_eps-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink download_eps;
        
        /// <summary>
        /// String21-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String21;
        
        /// <summary>
        /// accessories-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink accessories;
        
        /// <summary>
        /// String19-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String19;
        
        /// <summary>
        /// download_fotos-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink download_fotos;
        
        /// <summary>
        /// String22-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String22;
        
        /// <summary>
        /// String23-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String23;
        
        /// <summary>
        /// viproom-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink viproom;
        
        /// <summary>
        /// String16-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String16;
        
        /// <summary>
        /// Preise-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Preise;
        
        /// <summary>
        /// String24-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String24;
        
        /// <summary>
        /// String7-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String7;
        
        /// <summary>
        /// imgProductSymbol-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgProductSymbol;
        
        /// <summary>
        /// juniorPreview-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl juniorPreview;
    }
}
