using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r dsp_referenz_print.
	/// </summary>
	public partial class dsp_referenz_print :  Alpha.Page
	{
		public int iObj;
		public DataTable dtReference = new DataTable();
		string strNextColor = "WhiteSmoke";
		string sOrder = "";

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try {
				
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				if (!Page.IsPostBack) {                                        
					R.sReferenceSort = "REL_Place";                                  
				}
				if ((R.sReferenceSort.Length>0) && (R.sReferenceSort.Substring(R.sReferenceSort.Length -4) == "DESC"))
					sOrder = "5";                
				else
					sOrder = "6";
                
				dgrReference.Columns[0].HeaderText = getString("ort",true) + " / "+getString("land",true);
				dgrReference.Columns[2].HeaderText = getString("objekt",true)+" / "+getString("art",true);
				dgrReference.Columns[4].HeaderText = getString("system",true);
				dgrReference.Columns[5].HeaderText = getString("material",true);
				dgrReference.Columns[6].HeaderText = getString("vorhaben",true);


				//LBU  Wenn artID vorhandeln dann nur die Referenzen, die artID haben anzeigen
				if(Request.Params.Get("artID")!= null)
					R.strWhere = "AND ((SELECT COUNT(*) from CLAPP_Article_Reference CAR2  where  CAR2.REF_ID = CAR.REF_ID and CAR2.ART_ID = " + Request.Params.Get("artID").ToString() + ") > 0) " ; 
				dtReference = Model.Reference.getReferenceSearch(R.strWhere).Tables[0];
				dgrReference.DataSource = dtReference;
				dgrReference.DataBind();
				//DataSet dsSort = new DataSet ();
				//dsSort = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);
				DataView vSort = new DataView(dtReference);                
				vSort.Sort = R.sReferenceSort;                                      
				dgrReference.DataSource = vSort;
				dgrReference.DataBind();      
				
			}
			catch (Exception ex) {
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
			
		}

		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgrReference.ItemDataBound +=new DataGridItemEventHandler(dgrReference_ItemDataBound);

		}
		#endregion

		private void dgrReference_ItemDataBound(object sender, DataGridItemEventArgs e) {
			ListItemType itemType = (ListItemType)e.Item.ItemType;

		
			if(itemType != ListItemType.Header) {
				e.Item.Cells[0].Text = "<b>"+e.Item.Cells[1].Text.ToString() + "</b><br>" + e.Item.Cells[0].Text.ToString() ;
				e.Item.Cells[2].Text = "<b>"+e.Item.Cells[2].Text.ToString() + "</b> - " + e.Item.Cells[3].Text.ToString() ;

			}
			
			
		}
	}
}
