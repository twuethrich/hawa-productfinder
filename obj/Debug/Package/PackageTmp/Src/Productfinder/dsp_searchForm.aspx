<%@ Page CodeBehind="dsp_searchForm.aspx.cs" Language="c#" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_searchForm" enableViewState="True" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Styles/colorbox.css" media="screen" />
		<SCRIPT language="JavaScript" src="jscript/general.js" type="text/javascript"></SCRIPT>
       


			<SCRIPT language="javascript">
i0 = new Image;
i0.src='';
kurz0 ='';
flash0='';

row1 = new Image;
row1.src='images/checkbox_off.gif';
row2 = new Image;
row2.src='images/checkbox_on.gif';

var strQuery = new Array(<%= query %>);
var ColorGrey = "#909090";
var ColorBlack = "#000000";
sPlan = <%= sPlan %>;
sFunction = <%= sFunction %>;
sMaterial = <%= sMaterial %>;
intPlan = <%= iPlan %>;
intFunc = <%= iFunc %>;
intMat = <%= iMat %>;

function onLoadPage(){
	document.getElementById("plan").value = sPlan;
	document.getElementById("function").value = sFunction;
	document.getElementById("material").value = sMaterial;
	
	if (sPlan != -1){
		document.getElementById("imgPla_" + sPlan).src = row2.src;
		document.getElementById("plan").value = sPlan;
		onPlan(sPlan);
	}
	if (sFunction != -1){
		document.getElementById("imgFun_" + sFunction).src = row2.src;
		document.getElementById("function").value = sFunction;
		onFunction(sFunction);
	}
	if (sMaterial != -1){
		document.getElementById("imgMat_" + sMaterial).src = row2.src;
		document.getElementById("material").value = sMaterial;
		//if (sFunction == -1 && sPlan == -1)
		onMaterial(sMaterial);
	}
	<%= sSetFocus %>
}

function onPlan(row_ID){
	
	if (document.getElementById("lblPla_" + row_ID).style.color != ColorGrey){
		disablePlan();
		disableFunctionColor();
		disableMaterialColor();
		disableFunction();
		disableMaterial();
		document.getElementById("imgPla_" + row_ID).src = row2.src;
		document.getElementById("plan").value = row_ID;
		
		selPlan = row_ID;
		
        for (i=0; i<strQuery.length; i++){
            if (strQuery[i][0] == row_ID){
            
                //document.getElementById("imgFun_" + strQuery[i][1]).src = row1.src;                    
                //document.getElementById("imgMat_" + strQuery[i][2]).src = row1.src;
              
                //Kein Funktion und kein Material ausgew�hlt
                if (document.getElementById("function").value == -1 && document.getElementById("material").value == -1){
                    document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                }
                
                //Funktion ausgew�hlt und kein Material
                else if (document.getElementById("function").value != -1 && document.getElementById("material").value == -1){
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    }
                    if (strQuery[i][1] == document.getElementById("function").value){
                       document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                       document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src; 
                    }
                }
                
                // Keine Funktion aber Material ausgew�hlt 
                else if (document.getElementById("function").value == -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                    }
                     if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
                    }
                }
                
                // Funktion und Material ausw�hlt                                                         
                else if (document.getElementById("function").value != -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][0] == document.getElementById("plan").value && strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        if (strQuery[i][1] == document.getElementById("function").value){
							document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
						}
                        
                    }
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                        if (strQuery[i][2] == document.getElementById("material").value){
							document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
                        }
                       
                    }
                }
			}
		}
	}
}

function onFunction(row_ID){
	
    if (document.getElementById("lblFun_" + row_ID).style.color != ColorGrey){
        disableFunction();
        
        disablePlanColor();
        disableMaterialColor();
		disablePlan();
		disableMaterial();
        document.getElementById("imgFun_" + row_ID).src = row2.src;
        selFunction = row_ID;
        document.getElementById("function").value = row_ID;
        for (i=0; i<strQuery.length; i++){
            //alert("Array:" + strQuery[i] + "\n row_ID:" + row_ID + " strQuery[i][1]:" + strQuery[i][1]);
            if (strQuery[i][1] == row_ID){ 
                
             
                if (document.getElementById("plan").value == -1 && document.getElementById("material").value == -1){
                //alert("kein vorhaben und kein material");
                    document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                   
                }
                else if (document.getElementById("plan").value != -1 && document.getElementById("material").value == -1){
                    if (strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    }
                    if (strQuery[i][0] == document.getElementById("plan").value){
						//alert("nur Vorhaben " + document.getElementById("plan").value);
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                        document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                    }
                   
                }
                else if (document.getElementById("plan").value == -1 && document.getElementById("material").value != -1){
					if (strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
					}
					if (strQuery[i][2] == document.getElementById("material").value){
						//alert("nur Material " + document.getElementById("plan").value);
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
						document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
					}
					
                }                                                         
                else if (document.getElementById("plan").value != -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                        if(strQuery[i][0] == document.getElementById("plan").value){
						
							document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                        }
                        
                    }
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][0] == document.getElementById("plan").value){
						document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
						if (strQuery[i][2] == document.getElementById("material").value){
							document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
						}
                    }
                }
            }
        }
    }
}
 
function onMaterial(row_ID){
		
	   if (document.getElementById("lblMat_" + row_ID).style.color != ColorGrey){
        /*if (document.getElementById("plan").value != -1 && document.getElementById("function").value != -1){
        }
        else
        {
			//alert('disableMat');
			disablePlanColor();
			disableFunctionColor();
        }*/
		disableMaterial();
		disablePlanColor();
		disableFunctionColor();
		disablePlan();
		disableFunction();
		document.getElementById("imgMat_" + row_ID).src = row2.src;
        selMaterial = row_ID;
        document.getElementById("material").value = row_ID;
        
     
        
        
        for (i=0; i<strQuery.length; i++){
            if (strQuery[i][2] == row_ID){ 
                
                
                
                if (document.getElementById("plan").value == -1 && document.getElementById("function").value == -1){
                    document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                }
                else if (document.getElementById("plan").value != -1 && document.getElementById("function").value == -1){
                    if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    }
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                    }
                }
                else if (document.getElementById("plan").value == -1 && document.getElementById("function").value != -1){
                    if (strQuery[i][1] == document.getElementById("function").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                        document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
                        
                    }
                    if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    }
                }                                                         
                
               
                else if (document.getElementById("plan").value != -1 && document.getElementById("function").value != -1){
                 
                    if (strQuery[i][2] == document.getElementById("material").value && strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        if(strQuery[i][0] == document.getElementById("plan").value){
							document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                        }
                        
                    }
                    if (strQuery[i][2] == document.getElementById("material").value && strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
						if (strQuery[i][1] == document.getElementById("function").value){
							document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
						}
                    }
                }
                
                
                
                
                
                
                
                
             }
        }  
        
       
        
    }
}

function enablePlan(){
	for (i=1; i<intPlan+1; i++){
		//document.getElementById("imgPla_" + i).src = row1.src;
		document.getElementById("lblPla_" + i).style.color = ColorBlack;
	}
}

function enableFunction(){
	for (i=1; i<intFunc+1; i++){
		//document.getElementById("imgFun_" + i).src = row1.src;
		document.getElementById("lblFun_" + i).style.color = ColorBlack;
	}
}

function enableMaterial(){
	for (i=1; i<intMat+1; i++){
		//document.getElementById("imgMat_" + i).src = row1.src;
		document.getElementById("lblMat_" + i).style.color = ColorBlack;
	}
}

function disablePlanColor(){
	for (i=1; i<intPlan; i++){
		document.getElementById("lblPla_" + i).style.color = ColorGrey;
	}
}

function disableFunctionColor(){
	for (i=1; i<intFunc+1; i++){
		document.getElementById("lblFun_" + i).style.color = ColorGrey;
	}
}

function disableMaterialColor(){
	for (i=1; i<intMat+1; i++){
		document.getElementById("lblMat_" + i).style.color = ColorGrey;
		
	}
}

function disablePlan(){
	for (i=1; i<intPlan; i++){
		document.getElementById("imgPla_" + i).src = row1.src;
	}
}

function disableFunction(){
	for (i=1; i<intFunc+1; i++){
		document.getElementById("imgFun_" + i).src = row1.src;
	}
}

function disableMaterial(){
	for (i=1; i<intMat+1; i++){
		document.getElementById("imgMat_" + i).src = row1.src;
	}
}
<% 
int tabCount = 0;
if (tabList != null) tabCount = tabList.Rows.Count;
for(int i=0;i<tabCount;i++) { 
%>
 //i<%= tabList.Rows[i]["Art_ID"] %> = new Image;
 i<%= tabList.Rows[i]["Art_ID"] %>='dsp_objectdownload.aspx?id=<%= tabList.Rows[i]["Systemsymbol"]%>&typ=0';
 rr<%= tabList.Rows[i]["Art_ID"] %> ='<%= tabList.Rows[i]["Kurz"]%>';
 flash<%= tabList.Rows[i]["Art_ID"] %> = 'dsp_objectdownload.aspx?id=<%= tabList.Rows[i]["Groupsymbol"]%>&typ=11';
<% } %>

function changeGif(show,file){
	str = '<img src="'+ eval("i" + file)+'" >';
    document.getElementById("bild").innerHTML = str;
	//document.all["bild"].innerHTML = str;
	if (file>0){
		str = eval("rr" + file);
		//document.all["merkmal"].firstChild.nodeValue  = str;
		//document.all["merkmal"].innerHTML  = str;
        document.getElementById("merkmal").innerHTML = str;
	}else{
		//document.all["merkmal"].firstChild.nodeValue  = "";
        document.getElementById("merkmal").firstChild.nodeValue = "";
	}
	if (file>0){
	    str = '<img src="'+ eval("flash" + file)+'">';
	 	//document.all["flash"].innerHTML = str;		
        document.getElementById("flash").innerHTML = str;		
	}
    if(document.getElementById("juniorPreview")){
        if(show == "hidden")
            document.getElementById("juniorPreview").style.visibility = "visible";
        else
            document.getElementById("juniorPreview").style.visibility = "hidden";
    }
     document.getElementById("merkmal").style.visibility = show;
     document.getElementById("detail").style.visibility = show;
     document.getElementById("flash").style.visibility = show;
     document.getElementById("bild").style.visibility = show;
    //merkmal.style.visibility = show;
    //detail.style.visibility = show;
    //flash.style.visibility = show;
	//bild.style.visibility = show;
}

function changeFlash(show,file){
	document.getElementById("symbol").src= eval("i" + file + ".src");
    document.getElementById("merkmal").firstChild.nodeValue = eval("kurz" + file);
    str = '<OBJECT id="obj" codeBase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0"';
    str += 'height="145" width="150" border="1" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" VIEWASTEXT>';
    str += '<PARAM NAME="_cx" VALUE="3969"> <PARAM NAME="_cy" VALUE="3836"> <PARAM NAME="FlashVars" VALUE="">';
	str += '<PARAM NAME="Movie" VALUE="'+eval("flash" + file)+'">';
	str += '<PARAM NAME="Src" VALUE="'+eval("flash" + file)+'">';
	//str += '<PARAM NAME="WMode" VALUE="Window">';
	//str += '<PARAM NAME="Play" VALUE="0">';
	//str += '<PARAM NAME="Loop" VALUE="-1">';
	str += '<PARAM NAME="Quality" VALUE="High">';
	//str += '<PARAM NAME="SAlign" VALUE="">';
	//str += '<PARAM NAME="Menu" VALUE="-1">';
	//str += '<PARAM NAME="Base" VALUE="">';
	//str += '<PARAM NAME="AllowScriptAccess" VALUE="always">';
	//str += '<PARAM NAME="Scale" VALUE="ShowAll">';
	//str += '<PARAM NAME="DeviceFont" VALUE="0">';
	//str += '<PARAM NAME="EmbedMovie" VALUE="0">';
	//str += '<PARAM NAME="BGColor" VALUE="FFFFFF">';
	//str += '<PARAM NAME="SWRemote" VALUE="">';
	//str += '<PARAM NAME="MovieData" VALUE="">';
	//str += '<PARAM NAME="SeamlessTabbing" VALUE="1">';
	str += '<EMBED SRC="'+eval("flash" + file)+'" QUALITY="high" BGCOLOR="#FFFFFF" WIDTH="150" HEIGHT="145" NAME="81382"';
	str += 'ALIGN="" BORDER="0" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer">';
	str += '</EMBED>';
	str += '</OBJECT>';
    document.all["flash"].innerHTML = str;
    merkmal.style.visibility = show;
    detail.style.visibility = show;
    flash.style.visibility = show;
	bild.style.visibility = show;
}

</SCRIPT>
 <script type="text/javascript" src="jscript/jquery-1.6.2.min.js"></script>
 <script type="text/javascript" src="jscript/jquery.colorbox.js"></script>
 <script type="text/javascript">
     jQuery.noConflict();
     jQuery(document).ready(function () {
         jQuery(".junior").colorbox({ opacity: "0.75" });
         jQuery(".junior").colorbox({ iframe: true, width: "900px", height: "680px" });

     });
 </script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="onLoadPage(); setFocus('txtSearch');" rightMargin="0" marginwidth="0" marginheight="0">
	<div id="containert">	
        <table cellSpacing="0" cellPadding="0" width="900" border="0">
			<form id="productForm" action="dsp_searchForm.aspx" runat="server">
				<INPUT id=plan type=hidden value="<%= sPlan %>" name=plan 
  > <INPUT id=function type=hidden value="<%= sFunction %>" 
  name=function> <INPUT id=material type=hidden 
  value="<%= sMaterial %>" name=material>
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2">
									<div class="AppTitle"><ALPHA:STRING id="String4" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></div>
								</td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnFr" RUNAT="server">Fran�ais</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEs" RUNAT="server">Espa�ol</ASP:HYPERLINK>
								<td></td>
							</tr>
						</table>
						<!-- Title & Language, end--></td>
				<!-- TopTitle, start-->
				<tr class="TopTitle">
					<td vAlign="middle" width="450" height="30" style="padding-left:12px"><ALPHA:STRING id="Tipp" RUNAT="server" KEY="Tipp"></ALPHA:STRING></td>
					<td vAlign="middle" align="right">
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td><ALPHA:STRING id="Schnellsuche" RUNAT="server" KEY="Schnellsuche"></ALPHA:STRING>&nbsp;</td>
								<td><asp:textbox id="txtSearch" runat="server" ontextchanged="txtSearch_TextChanged" ></asp:textbox>&nbsp;</td>
								<td align="left"><A href="javascript:document.forms[0].submit();"></A><asp:imagebutton id="btnSearch" runat="server" ImageUrl="images/btn_search.gif" onclick="btnSearch_Click"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="1" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
				<!-- TopTitle, end-->
				<!-- Search, start-->
				<tr>
					<td vAlign="top" width="900" colSpan="3">
						<table borderColor="red" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- Title, start-->
							<tr>
								<td width="10"><IMG height="1" src="images/ts.gif" width="10" border="0"></td>
								<td class="BoxTitle" vAlign="middle" width="190"><ALPHA:STRING id="Vorhaben" RUNAT="server" KEY="Vorhaben"></ALPHA:STRING></td>
								<td width="10"><IMG height="1" src="images/ts.gif" width="10" border="0"></td>
								<TD class="BoxTitle" vAlign="middle" width="480"><ALPHA:STRING id="Funktion" RUNAT="server" KEY="Funktion"></ALPHA:STRING></TD>
								<td width="10"><IMG height="1" src="images/ts.gif" width="10" border="0"></td>
								<TD class="BoxTitle" vAlign="middle" width="190"><ALPHA:STRING id="String2" RUNAT="server" KEY="Material"></ALPHA:STRING></TD>
								<td width="10"><IMG height="1" src="images/ts.gif" width="10" border="0"></td>
							</tr>
							<!-- Title, end-->
							<tr>
								<td><IMG src="images/ts.gif" border="0"></td>
								<td class="BoxGrayBright" vAlign="top"><asp:panel id="plPlan" Runat="server"></asp:panel></td>
								<td></td>
								<td class="BoxGrayBright" vAlign="top"><asp:panel id="plFunction" Runat="server"></asp:panel></td>
								<td></td>
								<td class="BoxGrayBright" vAlign="top" height="100%">
									<!-- panel material, start-->
									<table borderColor="blue" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td><IMG height="1" src="images/ts.gif"></td>
										</tr>
										<tr>
											<td class="BoxGrayBright" vAlign="top"><asp:panel id="plMaterial" Runat="server"></asp:panel></td>
										</tr>
										<!-- berechnungshilfe, end -->
										<!-- buttons, start -->
										<tr>
											<td class="BoxGrayBright" vAlign="middle" height="52">
												<table cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td><IMG height="5" src="images/ts.gif"></td>
													</tr>
													<tr>
														<td><IMG src="images/ts.gif" width="10"></td>
														<td colSpan="2"><ALPHA:STRING id="Start" RUNAT="server" KEY="Start"></ALPHA:STRING></td>
														<td><IMG src="images/ts.gif" width="10"></td>
													</tr>
													<tr>
														<td><IMG height="4" src="images/ts.gif"></td>
													</tr>
													<tr>
														<td></td>
														<td><asp:imagebutton id="btnReset" runat="server" ImageUrl="images/btn_reset.gif" onclick="btnReset_Click"></asp:imagebutton></td>
														<td align="right"><asp:imagebutton id="btnSearchProdukt" runat="server" ImageUrl="images/btn_search2_de.gif" onclick="btnSearchProdukt_Click"></asp:imagebutton></td>
														<td></td>
													</tr>
													<tr>
														<td><IMG height="12" src="images/ts.gif"></td>
													</tr>
												</table>
											</td>
										</tr>
										<!-- buttons, end -->
										<tr>
											<td bgColor="white"><IMG height="10" src="images/ts.gif"></td>
										</tr>
										<!-- accessoires, start -->
										<TR class="BoxGrayBright" height="31">
											<td vAlign="middle"><IMG src="images/ts.gif" width="10"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0">
												<A href="acc_frm_main.aspx?back=true">
													<ALPHA:STRING id="String3" RUNAT="server" KEY="Accessoires"></ALPHA:STRING></A></td>
										</TR>
										<!-- accessoires, end -->
										<tr>
											<td bgColor="white"><IMG height="10" src="images/ts.gif"></td>
										</tr>
										<!-- berechnungshilfe, start -->
										<TR class="BoxGrayBright" height="31">
											<td vAlign="middle"><IMG src="images/ts.gif" width="10"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0">
												<A href="javascript:openWindow('dsp_berechnungshilfe.aspx','new_window', 400,330);">
													<ALPHA:STRING id="String1" RUNAT="server" KEY="berechnungshilfe"></ALPHA:STRING></A></td>
										</TR>
										<!-- berechnungshilfe, end -->
									</table>
									<!-- panel material, end--></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<!-- Search, end-->
				<tr>
					<td><IMG height="10" alt="" src="images/ts.gif" border="0"></td>
				</tr>
		</table>
		<!-- Begin SearchList -->
		<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="900" border="0">
			<TR>
				<TD width="10"></TD>
				<TD vAlign="top" width="450">
                    <asp:GridView ID="GridView1" runat="server">
                    </asp:GridView>
					<!--<DIV class="TA" id="tree" style="OVERFLOW: auto; WIDTH: 450px; HEIGHT: 300px; scrollbar:">-->
                   <ASP:DATAGRID id="dgrProductFinder" RUNAT="server" DATAKEYFIELD="ART_ID" ONPAGEINDEXCHANGED="pager" ONSORTCOMMAND="onProductFinderSort" ONITEMDATABOUND="OnDataBound" ALLOWSORTING="True" WIDTH="450px" HORIZONTALALIGN="Center" BORDERCOLOR="White" BORDERSTYLE="Solid" BORDERWIDTH="1px" CELLPADDING="0" AUTOGENERATECOLUMNS="False" AllowPaging="True" CssClass="BoxGrayAlternate">
						<AlternatingItemStyle CssClass="BoxGrayBright"></AlternatingItemStyle>
						<HeaderStyle Wrap="False" ForeColor="Black" CssClass="TableHeader"></HeaderStyle>
						<Columns>
							<asp:BoundColumn Visible="False" DataField="ART_ID" SortExpression="ART_ID" ReadOnly="True" HeaderText="ID">
								<ItemStyle HorizontalAlign="Right"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn SortExpression="Product" HeaderText="Produkt">
							   <HeaderStyle CssClass="GridHeaderText"></HeaderStyle>
								<ItemStyle Width="200px" CssClass="TableText"></ItemStyle>
								<ItemTemplate>
									<ASP:HYPERLINK ID="product" VISIBLE="true" RUNAT="server" NAVIGATEURL="dsp_detail.aspx" TARGET="_self">Hello</ASP:HYPERLINK>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="Weight" SortExpression="SortWeight" ReadOnly="True" HeaderText="max. T&#252;rgewicht">
								<HeaderStyle Wrap="False" HorizontalAlign="Right" CssClass="GridHeaderText"></HeaderStyle>
								<ItemStyle HorizontalAlign="Right" Width="100px" CssClass="TableText"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Material">
								<HeaderStyle HorizontalAlign="Left" CssClass="GridHeaderText"></HeaderStyle>
								<ItemStyle HorizontalAlign="Left" Width="80px" CssClass="TableText" VerticalAlign="Bottom"></ItemStyle>
								<ItemTemplate>
									<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">
										<TR>
											<TD NOWRAP>
												<ASP:IMAGE ID="holz" IMAGEURL="images/pic_material1.gif" RUNAT="server" VISIBLE="false" WIDTH="15" height="15"></ASP:IMAGE>
												<ASP:IMAGE ID="glas" IMAGEURL="images/pic_material2.gif" RUNAT="server" VISIBLE="false" width="15" HEIGHT="15"></ASP:IMAGE>
												<ASP:IMAGE ID="metall" IMAGEURL="images/pic_material3.gif" RUNAT="server" VISIBLE="false" width="15" HEIGHT="15"></ASP:IMAGE>
											</TD>
										</TR>
									</TABLE>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Merkmale">
								<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
								<ItemStyle HorizontalAlign="Center" Width="60px" CssClass="TableText" VerticalAlign="Bottom"></ItemStyle>
								<ItemTemplate>
									<ASP:LABEL RUNAT="server" ID="info"></ASP:LABEL>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn Visible="False" DataField="Product"></asp:BoundColumn>
							<asp:BoundColumn Visible="False" DataField="Holz"></asp:BoundColumn>
							<asp:BoundColumn Visible="False" DataField="Glas"></asp:BoundColumn>
							<asp:BoundColumn Visible="False" DataField="Metall"></asp:BoundColumn>
						</Columns>
                        
						<PagerStyle HorizontalAlign="right" Position="Top" CssClass="pager" Mode="NumericPages" >                         
                        </PagerStyle>
					</ASP:DATAGRID><br>
					<div class="TableNotFound"><alpha:string id="notFound" runat="server" Key="notFound"></alpha:string></div>
					<DIV></DIV>
				</TD>
				<TD width="10"><IMG height="1" alt="" src="images/ts.gif" width="10" border="0"></TD>
				<TD vAlign="top" width="420">
					<DIV id="detail" style="VISIBILITY: hidden" runat="server">
						<TABLE id="Table2" height="280" cellSpacing="0" cellPadding="10" width="100%" border="0">
							<TR>
								<TD class="BoxGrayBright">
									<TABLE id="Table3" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD align="center" width="210" bgColor="white">
												<DIV id="flash" style="VISIBILITY: visible"></DIV>
											</TD>
											<TD width="10"><IMG height="160" src="images/ts.gif" width="10"></TD>
											<TD vAlign="middle" align="center" width="180" bgColor="white">
												<DIV id="bild" style="VISIBILITY: visible"></DIV>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="BoxGray" vAlign="top" height="100">
									<DIV id="merkmal" NAME="merkmal">Beschlag mit Kurzbeschreibungchreibung</DIV>
								</TD>
							</TR>
						</TABLE>
					</DIV>
                    <div id="juniorPreview" runat="server" style="margin-top:-280px; * margin-top:-300px; visibility:hidden;">xxx</div>
                    
				</TD>
				<TD width="10"><IMG height="1" alt="" src="images/ts.gif" width="10" border="0"></TD>
			</TR>
		</TABLE></FORM>
	
    </div>
    	 <!--Google Analytics relevant -->		
   
    <script type="text/javascript">
    
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-3201274-1");
    pageTracker._trackPageview();
    pageTracker._setDomainName("none");
    pageTracker._setAllowLinker(true);
    pageTracker._setAllowHash(false);
    } catch(err) {}</script>
	</body>
</HTML>