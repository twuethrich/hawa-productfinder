using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;


namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class download_eps : System.Web.UI.Page
	{
		public static DataTable tabDownload;
		int agrID;
		int artID;
		//fill table with data for download ESP files
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try {
				
				int accessoire = -1;
				int artID = -1;
				if(Request.Params.Get("artID") != null)
				{
					artID = Convert.ToInt32(Request.Params.Get("artID"));
				}
				if(Request.Params.Get("groupID") != null)
				{
					artID = Convert.ToInt32(Request.Params.Get("groupID"));
					accessoire = 1;
				}
                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }
				
				btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				if(Convert.ToInt32(Request.Params.Get("agrID")) == 0){
					agrID=37;
				}else{
					agrID=Convert.ToInt32(Request.Params.Get("agrID"));
				}
				tabDownload = Model.ArticleAndGroup.getSubArticleObjects(artID, 43).Tables[0];
				
				if(agrID == 37){
					dgrEps.DataSource = Model.ArticleAndGroup.getArticleAndGroup(artID, agrID,accessoire);
				}else{
					dgrEps.DataSource = Model.ArticleAndGroup.getSubArticleObjects(artID, agrID);
				}
				dgrEps.DataBind();
				
				if(agrID == 37)
				{
					if(tabDownload.Rows.Count > 0)
					{
						lnkIso.NavigateUrl = "dsp_download_eps.aspx?artID=" + artID + "&agrID=43";
						lnkIso.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
						lnkEPS.Attributes.Add("Style", "color:#909090;");
					}
					else
					{
						lnkIso.Attributes.Add("Style", "color:#909090;");
					}
				}
				else
				{
					lnkEPS.NavigateUrl = "dsp_download_eps.aspx?artID=" + artID + "&agrID=37";
					lnkEPS.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					lnkIso.Attributes.Add("Style", "color:#909090;");
                    
				}
			
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
                //iResponse.Redirect("dsp_error_page.aspx");
			}

			
			//if (tabDownload.Rows.Count > 0) {
			
			//}
			
		}

		//add links for every rows to download EPS files
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				string showname;
				string name;
				string code;
                string productname;
                string filename;
				string id;
                string category;

				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;
				if(agrID == 37){
					name = ((TableCell)(e.Item.Controls[2])).Text.ToString();
                    category = "Katalogzeichnungen";
				}else{
					name = ((TableCell)(e.Item.Controls[6])).Text.ToString();
                    category = "Artikelperspektiven";
                }


                filename = ((TableCell)(e.Item.Controls[0])).Text.ToString();
                productname = ((TableCell)(e.Item.Controls[5])).Text.ToString(); 

            	id = ((TableCell)(e.Item.Controls[0])).Text.ToString();
				code=((TableCell)(e.Item.Controls[2])).Text.ToString();
				showname = name;
				
				if(agrID == 43 & code.Length > 0 & code !=""){
					showname= code + " - " +((TableCell)(e.Item.Controls[5])).Text.ToString();
                   
				}
				
				
				
				double size;
				size = Convert.ToInt32(((TableCell)(e.Item.Controls[4])).Text) /1024;
				
                String pfad = TypPath.getPath(2,agrID)+id;
                
               
				if(agrID==43){
					try{
						FileInfo FI = new FileInfo(pfad);
						size = FI.Length / 1024;
                      
                    }catch{
						size = 0;
					}
				}
				
				if(size == 0){
					e.Item.Visible = false;
				}

                String pfad2 = TypPath.getPath(7, agrID) + name + ".jpg";
                FileInfo tFI = new FileInfo(pfad2);

                if (File.Exists(pfad2))
                {
                    e.Item.Cells[0].Text = "<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('" + category +"','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + id + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\"><img src=\"dsp_objectdownload.aspx?typ=3&group=" + agrID + "&id=" + name + "\" border=0></a>";
                }
                else
                {
                    e.Item.Cells[0].Text = "<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('" + category + "','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + id + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\"><img src=\"images/ts.gif\" border=0></a>";
                }
                e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('" + category + "','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + id + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + showname + "</a>";
				e.Item.Cells[4].Text =  size + "&nbsp;KB &nbsp;";
				if(size == 0){
					e.Item.Visible = false;
				}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
