using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r dsp_dxfinfo.
	/// </summary>
	public partial class dxfinfo : System.Web.UI.Page
	{
		public static DataTable tabDownload_dxf;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			int accessoire = -1;
			int artID = -1;
			if(Request.Params.Get("artID") != null)
			{
				artID = Convert.ToInt32(Request.Params.Get("artID"));
			}
			if(Request.Params.Get("agrID") != null)
			{
				artID = Convert.ToInt32(Request.Params.Get("agrID"));
				accessoire = 1;
			}
            if (Request.Params.Get("languageID") != null)
            {
                Session.Add("languageID", Request.Params.Get("languageID").ToString());
            }
			CheckBox1.Attributes.Add("onClick", "javascript:document.location.href='dsp_download_dxf.aspx?artID=" + artID +"&accessoire="+accessoire+"'"); 
			btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
			
		}

		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void CheckBox1_CheckedChanged(object sender, System.EventArgs e) {
			
			
			
		}

	}
}
