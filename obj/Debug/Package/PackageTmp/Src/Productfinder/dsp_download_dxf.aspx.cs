using System;
using  System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class download_dxf : System.Web.UI.Page
	{
		int agrID = 38;
        public static DataTable tabDetail;
    	//fill table with data for download DXF files
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				int accessoire = Convert.ToInt32(Request.Params.Get("accessoire"));
				int artID = Convert.ToInt32(Request.Params.Get("artID"));

                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }

                /*tabDetail = Model.ProductDetail.getProductDetail(artID).Tables[0];
                if(tabDetail.Rows.Count>0)
                    Session.Add("productName", tabDetail.Rows[0]["Name"].ToString());
                */
                btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				dgrDxf.DataSource = Model.ArticleAndGroup.getArticleAndGroup(artID, agrID,accessoire);
				dgrDxf.DataBind(); 
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//add links for every rows to download DXF files
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				string showname;
				string name;
				string id;
                string filename;
                string productname;

				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;


                productname = ((TableCell)(e.Item.Controls[5])).Text.ToString();
                
                showname = ((TableCell)(e.Item.Controls[2])).Text.ToString();
				name = ((TableCell)(e.Item.Controls[1])).Text.ToString();
				id = ((TableCell)(e.Item.Controls[0])).Text.ToString();


                filename = id;
              
           
              	double size;
				size = Convert.ToInt32(((TableCell)(e.Item.Controls[4])).Text) /1024;
                
				e.Item.Cells[4].Text =  size + "&nbsp;KB &nbsp;";


                e.Item.Cells[0].Text = "<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('DXF','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + id + "','new_window', 'height=250px, width=350px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\"><img src=\"dsp_objectdownload.aspx?typ=3&group=" + agrID + "&id=" + showname + "\" border=0 alt=></a>";
                e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('DXF','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + id + "','new_window', 'height=250px, width=350px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + showname + "</a>";
				
				String pfad = TypPath.getPath(2,agrID)+showname+".zip";
				if(size > 2000){
					try{
						FileInfo FI = new FileInfo(pfad);
						size = FI.Length / 1024;
					}catch{
						size = 0;
					}
                    e.Item.Cells[0].Text = "<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('DXF','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=15&group=" + agrID + "&id=" + showname + "','new_window', 'height=250px, width=350px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\"><img src=\"dsp_objectdownload.aspx?typ=3&group=" + agrID + "&id=" + showname + "\" border=0 alt=></a>";
                    e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('DXF','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=15&group=" + agrID + "&id=" + showname + "','new_window', 'height=250px, width=350px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + showname + "</a>";
				
				}

			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
