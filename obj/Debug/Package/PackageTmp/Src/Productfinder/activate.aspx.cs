using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Hawa.Src.Security {
	/// <summary>
	/// Zusammenfassung f�r Activate.
	/// </summary>
	public partial class Activate : Alpha.Page {
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
	
	
		protected void Page_Load(object sender, System.EventArgs e) {
			ActivateAccount();
		
		}

		private void ActivateAccount() {
			
			string connectionStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];
			SqlConnection connection = new SqlConnection(connectionStr);          


			int user = Convert.ToInt32(Request.Params.Get("userID"));
			string code = Request.Params.Get("code").ToString();
			string lang = Request.Params.Get("LNG_ID").ToString();
			int iLang = 0;
			if(lang == "EN"){
				iLang=1;
			}

			int iUser = Convert.ToInt32(getUserID(user));
			string iCode = getPassword(iUser).ToString();
			iCode = FormsAuthentication.HashPasswordForStoringInConfigFile(iCode, "md5").ToString();
						
			if((iUser > 0) && (code == iCode)){
				
				string queryString = "UPDATE WORK_Mandant_User SET MDT_USR_Active = 1 WHERE USR_ID = " + iUser ;                                 

				connection.Open();
				SqlCommand command = new SqlCommand(queryString,connection);
				command.ExecuteNonQuery();                          
				connection.Close();
				Response.Redirect("http://www.hawa.ch/hawa/index.php?id=306&L="+iLang);
								
			}else{
				Response.Redirect("http://www.hawa.ch/hawa/index.php?id=307&L="+iLang);
				
			}



				
			        
		}

		public static int getUserID(int user){
			string connectionStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];
			object res;
			SqlConnection sqlConnection = new SqlConnection(connectionStr);
			sqlConnection.Open();
			string sql = "SELECT USR_ID FROM WORK_Mandant_User WHERE USR_ID="+user;						
			
			SqlCommand cmd = new SqlCommand(sql, sqlConnection);
			res = cmd.ExecuteScalar();
			if(res == null)
				res = 0;
			sqlConnection.Close();
			return Convert.ToInt32(res);
		}
		public static string getPassword(int user){
			string connectionStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];
			string res;
			SqlConnection sqlConnection = new SqlConnection(connectionStr);
			sqlConnection.Open();
			string sql = "SELECT MDT_USR_Password FROM WORK_Mandant_User WHERE USR_ID="+user;						
			SqlCommand cmd = new SqlCommand(sql, sqlConnection);
			res = cmd.ExecuteScalar().ToString();
			if(res == null)
				res = "";
			sqlConnection.Close();
			return res;
		}
		


		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent() {    

		}
		#endregion
	}
}
