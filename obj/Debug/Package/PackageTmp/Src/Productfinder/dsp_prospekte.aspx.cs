using System;
using  System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;


namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class Prospekte : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblShort;
		int agrID = 39; 

		//fill table with data for download prospekte
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				int accessoire = -1;
				int artID = -1;
				if(Request.Params.Get("artID") != null)
				{
					artID = Convert.ToInt32(Request.Params.Get("artID"));
				}
				if(Request.Params.Get("agrID") != null)
				{
					artID = Convert.ToInt32(Request.Params.Get("agrID"));
					accessoire = 1;
					agrID = 45;
				}
                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }

				btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				dgrProspekte.DataSource = Model.ArticleAndGroup.getLanguageObjects(artID, agrID, accessoire);
				dgrProspekte.DataBind(); 
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//add links for every rows to download prospekte files
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				
				string name;
                string filename;
				double size;
                string productname;
			
				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

			
				name = ((TableCell)(e.Item.Controls[1])).Text.ToString();
                filename = ((TableCell)(e.Item.Controls[2])).Text.ToString();
				size = Convert.ToInt32(((TableCell)(e.Item.Controls[5])).Text) /1024;
                productname = ((TableCell)(e.Item.Controls[6])).Text.ToString(); 

            

				//String pfad  = TypPath.getPath(2,agrID) + showname + TypPath.getEXT(2,agrID);

				name = ((TableCell)(e.Item.Controls[3])).Text.ToString();
				e.Item.Cells[0].Text = "<img src=\"images/pdf_file.gif\">";
                if (size > 2000)
                    e.Item.Cells[0].Text = "<img src=\"images/zip_file.gif\">";


                e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('Prospekte','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + ((TableCell)(e.Item.Controls[2])).Text.ToString() + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name + "</a>";
				e.Item.Cells[5].Text =  size + "kB &nbsp;";

			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
