using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model.Other;
using Hawa.Src.Productfinder.Model;
//using Hawa.Classes;

namespace Hawa 
{
	/// <summary>
	/// Zusammenfassung f�r Global.
	/// </summary>
	public class Global : Alpha.Global
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		public static Menu M; 
		public static Articles A; 
		public static PlanFuncMat PFM; 
		public Error E; 

		public Global()
		{
			InitializeComponent();
			try	 
			{
				E = new Error();
				M = Menu.getSingelton(); 
				A = Articles.getSingelton(); 
				PFM = PlanFuncMat.getSingelton(Global.M); 
			}
			catch (Exception e)
			{
				E.iError = e.ToString();
			}

		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{           
			Reference r = new Reference();
			Session.Add("reference", r);
			ErrorMessage errMes = new ErrorMessage();
			Session.Add("error", errMes);
			
		
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Vom Web Form-Designer generierter Code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
			

		}
	
		#endregion
	}
}

