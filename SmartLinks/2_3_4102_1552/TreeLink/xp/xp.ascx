<%@ Register TagPrefix="sl" Namespace="DevMansion.Web.UI.SmartLinks" Assembly="devmansion.web.ui.smartlinks" %>
<%@ Control Language="C#" Inherits="DevMansion.Web.UI.SmartLinks.UserTreeLink" %>

<script runat="server" language=C#>

	private new string ResolveUrl(string url) {
		return DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(base.ResolveUrl(url));
	}

	protected override void OnInit(EventArgs e) {
	
		base.OnInit(e);
		
		if(CSSFile == null || CSSFile == string.Empty)
			CSSFile = DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(ResolveUrl("xp.css"));
			
		string path = DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(ResolveUrl("xp.js"));
		string script = "<script language='javascript' src='" + path + "'><" + "/script>";
		DevMansion.Web.UI.SmartLinks.RootContext.GetContext(Context, Page).RegisterScriptBlock(path, script);


		tree.DefaultValues["RootNodeImage"] = ResolveUrl("root.gif");
		tree.ClientSideDataItems.Add("@RootNodeImage");
		tree.SerializableDataItems.Add("RootNodeImage");
		
		tree.DefaultValues["DummyImage"] = ResolveUrl("p.gif");
		tree.ClientSideDataItems.Add("@DummyImage");
		tree.SerializableDataItems.Add("DummyImage");
		
		tree.DefaultValues["RowCSS"] = "sltXpRow";
		tree.ClientSideDataItems.Add("@RowCSS");
		tree.SerializableDataItems.Add("RowCSS");

		tree.DefaultValues["HolderCSS"] = "sltXpHolder";
		tree.ClientSideDataItems.Add("@HolderCSS");
		tree.SerializableDataItems.Add("HolderCSS");
		
		////////////////////////////////////////////
		// ITEM
		////////////////////////////////////////////
		
		tree.DefaultValues["Text"] = "Caption";
		tree.ClientSideDataItems.Add("@Text");
		tree.SerializableDataItems.Add("Text");
		
		tree.DefaultValues["BranchImage1"] = ResolveUrl("folder1.gif");
		tree.ClientSideDataItems.Add("@BranchImage1");
		tree.SerializableDataItems.Add("BranchImage1");
		
		tree.DefaultValues["BranchImage2"] = ResolveUrl("folder2.gif");
		tree.ClientSideDataItems.Add("@BranchImage2");
		tree.SerializableDataItems.Add("BranchImage2");
		
		tree.DefaultValues["ItemImage"] = ResolveUrl("item.gif");
		tree.ClientSideDataItems.Add("@ItemImage");
		tree.SerializableDataItems.Add("ItemImage");
		
		tree.DefaultValues["ItemImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@ItemImageWidth");
		tree.SerializableDataItems.Add("ItemImageWidth");
		
		tree.DefaultValues["ItemImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@ItemImageHeight");
		tree.SerializableDataItems.Add("ItemImageHeight");
		
		tree.DefaultValues["ItemDCSS"] = "sltXpItemd";
		tree.ClientSideDataItems.Add("@ItemDCSS");
		tree.SerializableDataItems.Add("ItemDCSS");

		tree.DefaultValues["ItemNCSS"] = "sltXpItemn";
		tree.ClientSideDataItems.Add("@ItemNCSS");
		tree.SerializableDataItems.Add("ItemNCSS");
		
		tree.DefaultValues["ItemCSS"] = "sltXpItem";
		tree.ClientSideDataItems.Add("@ItemCSS");
		tree.SerializableDataItems.Add("ItemCSS");

		tree.DefaultValues["ItemCSS"] = "sltXpItem";
		tree.ClientSideDataItems.Add("@ItemCSS");
		tree.SerializableDataItems.Add("ItemCSS");
		
		tree.DefaultValues["ItemACSS"] = "sltXpItema";
		tree.ClientSideDataItems.Add("@ItemACSS");
		tree.SerializableDataItems.Add("ItemACSS");
		
		tree.DefaultValues["ItemSCSS"] = "sltXpItems";
		tree.ClientSideDataItems.Add("@ItemSCSS");
		tree.SerializableDataItems.Add("ItemSCSS");
		
		tree.DefaultValues["ItemASCSS"] = "sltXpItemas";
		tree.ClientSideDataItems.Add("@ItemASCSS");
		tree.SerializableDataItems.Add("ItemASCSS");
		
		////////////////////////////////////////////
		// KNOT
		////////////////////////////////////////////
		
		tree.DefaultValues["KnotImage1"] = ResolveUrl("knot1.gif");
		tree.ClientSideDataItems.Add("@KnotImage1");
		tree.SerializableDataItems.Add("KnotImage1");
		
		tree.DefaultValues["KnotImage2"] = ResolveUrl("knot2.gif");
		tree.ClientSideDataItems.Add("@KnotImage2");
		tree.SerializableDataItems.Add("KnotImage2");
		
		tree.DefaultValues["KnotImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@KnotImageWidth");
		tree.SerializableDataItems.Add("KnotImageWidth");
		
		tree.DefaultValues["KnotImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@KnotImageHeight");
		tree.SerializableDataItems.Add("KnotImageHeight");
		
		tree.DefaultValues["KnotCSS"] = "sltXpKnot";
		tree.ClientSideDataItems.Add("@KnotCSS");
		tree.SerializableDataItems.Add("KnotCSS");

		
		////////////////////////////////////////////
		// SPACER
		////////////////////////////////////////////
		
		tree.DefaultValues["SpacerImage"] = ResolveUrl("p.gif");
		tree.ClientSideDataItems.Add("@SpacerImage");
		tree.SerializableDataItems.Add("SpacerImage");
		
		tree.DefaultValues["SpacerImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@SpacerImageWidth");
		tree.SerializableDataItems.Add("SpacerImageWidth");
		
		tree.DefaultValues["SpacerImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@SpacerImageHeight");
		tree.SerializableDataItems.Add("SpacerImageHeight");
		
		tree.DefaultValues["SpacerCSS"] = "sltXpSpacer";
		tree.ClientSideDataItems.Add("@SpacerCSS");
		tree.SerializableDataItems.Add("SpacerCSS");
		
	}

</script>

<script language="javascript">

	TreeLinkAPI.setUpdateView(<%= tree.InstanceExpression%>, sltxp_updateView);
	TreeLinkAPI.setResolveValue(<%= tree.InstanceExpression%>, sltxp_resolveValue);
	TreeLinkAPI.setResolveCssClass(<%= tree.InstanceExpression%>, sltxp_resolveCssClass);
	
</script>

<sl:TreeLink id="tree" runat="server"
	PreloadImageFields="KnotImage1,KnotImage2,DummyImage,ItemImage,BranchImage1,BranchImage2"
	RelativeFields="RootNodeImage,KnotImage1,KnotImage2,DummyImage,ItemImage,BranchImage1,BranchImage2" >

	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Item">

		<Template><table border=0 cellpadding=1 cellspacing=0><tr>
			<td><img 
					<%# Container.DefineElement("ItemImage")%>
					src='<%# Container.Eval("ItemImage")%>'
					width='<%# Container.Eval("ItemImageWidth")%>'
					height='<%# Container.Eval("ItemImageHeight")%>'
					border=0></td>
			<td><img src='<%# Container.Eval("DummyImage")%>'
					width=2
					height=16
					border=0></td>
			<td id=sltl nowrap><%# Container.Eval("Text")%></td>
			</tr></table></Template>
		
	</sl:TreeLinkTemplate>

	<sl:TreeLinkTemplate runat="server"
		CustomType="Disabled"
		Type="Item">

		<Template><table border=0 cellpadding=1 cellspacing=0><tr>
			<td><img 
					<%# Container.DefineElement("ItemImage")%>
					src='<%# Container.Eval("ItemImage")%>'
					width='<%# Container.Eval("ItemImageWidth")%>'
					height='<%# Container.Eval("ItemImageHeight")%>'
					border=0></td>
			<td><img src='<%# Container.Eval("DummyImage")%>'
					width=2
					height=16
					border=0></td>
			<td id=sltl nowrap><%# Container.Eval("Text")%></td>
			</tr></table></Template>
		
	</sl:TreeLinkTemplate>

	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Knot">

		<Template><img
				<%# Container.DefineElement("KnotImage")%>
				src='<%# Container.Eval("KnotImage")%>'
				width='<%# Container.Eval("KnotImageWidth")%>'
				height='<%# Container.Eval("KnotImageHeight")%>'
				border=0></Template>
		
	</sl:TreeLinkTemplate>
	
	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Spacer">

		<Template><img src='<%# Container.Eval("SpacerImage")%>'
				width='<%# Container.Eval("SpacerImageWidth")%>'
				height='<%# Container.Eval("SpacerImageHeight")%>'
				border=0></Template>
		
	</sl:TreeLinkTemplate>
	
</sl:TreeLink>