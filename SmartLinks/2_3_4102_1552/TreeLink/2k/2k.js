function slt2k_updateView(F){if(slMode=="W3C")TreeLinkAPI.updateCssClass(F);else if(slMode=="NN"){F.updateSelection();return}
if(TreeLinkAPI.getChildNodes(F).length > 0&&TreeLinkAPI.getParent(F)){var Av=TreeLinkAPI.getElement(F, "#KnotImage");var Ax=TreeLinkAPI.getElement(F, "#ItemImage");if(TreeLinkAPI.getExpanded(F)){if(Av)Av.src=TreeLinkAPI.getValue(F, "@KnotImage1");if(Ax)Ax.src=TreeLinkAPI.getValue(F, "@BranchImage1")}
else{if(Av)Av.src=TreeLinkAPI.getValue(F, "@KnotImage2");if(Ax)Ax.src=TreeLinkAPI.getValue(F, "@BranchImage2")}}}
function slt2k_resolveValue(F, AX, Y, D){if(Y==1&&AX=="@KnotImage")return TreeLinkAPI.getValue(F, TreeLinkAPI.getExpanded(F)?"@KnotImage1":"@KnotImage2");else if(Y==0&&AX=="@ItemImage"&&TreeLinkAPI.getChildNodes(F).length > 0){if(TreeLinkAPI.getParent(F))return TreeLinkAPI.getValue(F, TreeLinkAPI.getExpanded(F)?"@BranchImage1":"@BranchImage2");return TreeLinkAPI.getValue(F, "@RootNodeImage")}
return TreeLinkAPI.getValue(F, AX)}
function slt2k_resolveCssClass(F, Y, EF){var W=TreeLinkAPI;if(Y !=0){var value;var Ec=W.getDepth(F);D=Ec-EF-1;var AU;if(slMode !="W3C")value=W.getValue(F, "@Spacer0CSS");else if(D==0){AU=W.getNodeKind(F);if(AU==0 || AU==3)value=W.getValue(F, Y==1?"@Knot3CSS":"@Spacer3CSS");else value=W.getValue(F, Y==1?"@Knot2CSS":"@Spacer2CSS")}
else{var Ap=F;while(D > 0){Ap=W.getParent(Ap);D--}
AU=W.getNodeKind(Ap);if(AU==1 || AU==2)value=W.getValue(F, "@Spacer1CSS");else value=W.getValue(F, "@Spacer0CSS")}
return value}
if(F.Disabled)return W.getValue(F, "@ItemDCSS");if(F.NonSelectable)return W.getValue(F, "@ItemNCSS");var Bk=W.getSelected(F);if(W.getActive(F))return W.getValue(F, Bk?"@ItemASCSS":"@ItemACSS");return W.getValue(F, Bk?"@ItemSCSS":"@ItemCSS")}