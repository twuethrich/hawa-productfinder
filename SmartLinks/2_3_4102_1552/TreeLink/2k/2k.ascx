<%@ Register TagPrefix="sl" Namespace="DevMansion.Web.UI.SmartLinks" Assembly="devmansion.web.ui.smartlinks" %>
<%@ Control Language="C#" Inherits="DevMansion.Web.UI.SmartLinks.UserTreeLink" %>

<script runat="server" language=C#>

	private new string ResolveUrl(string url) {
		return DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(base.ResolveUrl(url));
	}

	protected override void OnInit(EventArgs e) {
	
		base.OnInit(e);
		
		if(CSSFile == null || CSSFile == string.Empty)
			CSSFile = DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(ResolveUrl("2k.css"));
			
		tree.DefaultValues["Spacer1"] = ResolveUrl("spacer1.gif");
		tree.DefaultValues["Spacer2"] = ResolveUrl("spacer2.gif");
		tree.DefaultValues["Spacer3"] = ResolveUrl("spacer3.gif");
		
		tree.ClientSideDataItems.Add("@Spacer1");
		tree.ClientSideDataItems.Add("@Spacer2");
		tree.ClientSideDataItems.Add("@Spacer3");
		
		string path = DevMansion.Web.UI.SmartLinks.SmartLinkUtils.CleanUrlFromSessionID(ResolveUrl("2k.js"));
		string script = "<script language='javascript' src='" + path + "'><" + "/script>";
		DevMansion.Web.UI.SmartLinks.RootContext.GetContext(Context, Page).RegisterScriptBlock(path, script);
		
		tree.DefaultValues["RootNodeImage"] = ResolveUrl("root.gif");
		tree.ClientSideDataItems.Add("@RootNodeImage");
		tree.SerializableDataItems.Add("RootNodeImage");
		
		tree.DefaultValues["DummyImage"] = ResolveUrl("p.gif");
		tree.ClientSideDataItems.Add("@DummyImage");
		tree.SerializableDataItems.Add("DummyImage");
		
		tree.DefaultValues["RowCSS"] = "slt2kRow";
		tree.ClientSideDataItems.Add("@RowCSS");
		tree.SerializableDataItems.Add("RowCSS");

		tree.DefaultValues["HolderCSS"] = "slt2kHolder";
		tree.ClientSideDataItems.Add("@HolderCSS");
		tree.SerializableDataItems.Add("HolderCSS");
		
		////////////////////////////////////////////
		// ITEM
		////////////////////////////////////////////
		
		tree.DefaultValues["Text"] = "Caption";
		tree.ClientSideDataItems.Add("@Text");
		tree.SerializableDataItems.Add("Text");
		
		tree.DefaultValues["BranchImage1"] = ResolveUrl("folder1.gif");
		tree.ClientSideDataItems.Add("@BranchImage1");
		tree.SerializableDataItems.Add("BranchImage1");
		
		tree.DefaultValues["BranchImage2"] = ResolveUrl("folder2.gif");
		tree.ClientSideDataItems.Add("@BranchImage2");
		tree.SerializableDataItems.Add("BranchImage2");
		
		tree.DefaultValues["ItemImage"] = ResolveUrl("item.gif");
		tree.ClientSideDataItems.Add("@ItemImage");
		tree.SerializableDataItems.Add("ItemImage");
		
		tree.DefaultValues["ItemImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@ItemImageWidth");
		tree.SerializableDataItems.Add("ItemImageWidth");
		
		tree.DefaultValues["ItemImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@ItemImageHeight");
		tree.SerializableDataItems.Add("ItemImageHeight");
		
		tree.DefaultValues["ItemDCSS"] = "slt2kItemd";
		tree.ClientSideDataItems.Add("@ItemDCSS");
		tree.SerializableDataItems.Add("ItemDCSS");

		tree.DefaultValues["ItemNCSS"] = "slt2kItemn";
		tree.ClientSideDataItems.Add("@ItemNCSS");
		tree.SerializableDataItems.Add("ItemNCSS");
		
		tree.DefaultValues["ItemCSS"] = "slt2kItem";
		tree.ClientSideDataItems.Add("@ItemCSS");
		tree.SerializableDataItems.Add("ItemCSS");

		tree.DefaultValues["ItemCSS"] = "slt2kItem";
		tree.ClientSideDataItems.Add("@ItemCSS");
		tree.SerializableDataItems.Add("ItemCSS");
		
		tree.DefaultValues["ItemACSS"] = "slt2kItema";
		tree.ClientSideDataItems.Add("@ItemACSS");
		tree.SerializableDataItems.Add("ItemACSS");
		
		tree.DefaultValues["ItemSCSS"] = "slt2kItems";
		tree.ClientSideDataItems.Add("@ItemSCSS");
		tree.SerializableDataItems.Add("ItemSCSS");
		
		tree.DefaultValues["ItemASCSS"] = "slt2kItemas";
		tree.ClientSideDataItems.Add("@ItemASCSS");
		tree.SerializableDataItems.Add("ItemASCSS");
		
		////////////////////////////////////////////
		// KNOT
		////////////////////////////////////////////
		
		tree.DefaultValues["KnotImage1"] = ResolveUrl("knot1.gif");
		tree.ClientSideDataItems.Add("@KnotImage1");
		tree.SerializableDataItems.Add("KnotImage1");
		
		tree.DefaultValues["KnotImage2"] = ResolveUrl("knot2.gif");
		tree.ClientSideDataItems.Add("@KnotImage2");
		tree.SerializableDataItems.Add("KnotImage2");
		
		tree.DefaultValues["KnotImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@KnotImageWidth");
		tree.SerializableDataItems.Add("KnotImageWidth");
		
		tree.DefaultValues["KnotImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@KnotImageHeight");
		tree.SerializableDataItems.Add("KnotImageHeight");
		
		tree.DefaultValues["Knot2CSS"] = "slt2kKnot2";
		tree.ClientSideDataItems.Add("@Knot2CSS");
		tree.SerializableDataItems.Add("Knot2CSS");

		tree.DefaultValues["Knot3CSS"] = "slt2kKnot3";
		tree.ClientSideDataItems.Add("@Knot3CSS");
		tree.SerializableDataItems.Add("Knot3CSS");
		
		////////////////////////////////////////////
		// SPACER
		////////////////////////////////////////////
		
		tree.DefaultValues["SpacerImage"] = ResolveUrl("p.gif");
		tree.ClientSideDataItems.Add("@SpacerImage");
		tree.SerializableDataItems.Add("SpacerImage");
		
		tree.DefaultValues["SpacerImageWidth"] = "16";
		tree.ClientSideDataItems.Add("@SpacerImageWidth");
		tree.SerializableDataItems.Add("SpacerImageWidth");
		
		tree.DefaultValues["SpacerImageHeight"] = "16";
		tree.ClientSideDataItems.Add("@SpacerImageHeight");
		tree.SerializableDataItems.Add("SpacerImageHeight");
		
		tree.DefaultValues["Spacer0CSS"] = "slt2kSpacer0";
		tree.ClientSideDataItems.Add("@Spacer0CSS");
		tree.SerializableDataItems.Add("Spacer0CSS");
		
		tree.DefaultValues["Spacer1CSS"] = "slt2kSpacer1";
		tree.ClientSideDataItems.Add("@Spacer1CSS");
		tree.SerializableDataItems.Add("Spacer1CSS");
		
		tree.DefaultValues["Spacer2CSS"] = "slt2kSpacer2";
		tree.ClientSideDataItems.Add("@Spacer2CSS");
		tree.SerializableDataItems.Add("Spacer2CSS");

		tree.DefaultValues["Spacer3CSS"] = "slt2kSpacer3";
		tree.ClientSideDataItems.Add("@Spacer3CSS");
		tree.SerializableDataItems.Add("Spacer3CSS");
		
	}

</script>

<script language="javascript">

	TreeLinkAPI.setUpdateView(<%= tree.InstanceExpression%>, slt2k_updateView);
	TreeLinkAPI.setResolveValue(<%= tree.InstanceExpression%>, slt2k_resolveValue);
	TreeLinkAPI.setResolveCssClass(<%= tree.InstanceExpression%>, slt2k_resolveCssClass);
	
</script>

<sl:TreeLink id="tree" runat="server"
	PreloadImageFields="KnotImage1,KnotImage2,DummyImage,ItemImage,BranchImage1,BranchImage2,Spacer1,Spacer2,Spacer3"
	RelativeFields="RootNodeImage,KnotImage1,KnotImage2,DummyImage,ItemImage,BranchImage1,BranchImage2" >

	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Item">

		<Template><table border=0 cellpadding=1 cellspacing=0><tr>
			<td><img 
					<%# Container.DefineElement("ItemImage")%>
					src='<%# Container.Eval("ItemImage")%>'
					width='<%# Container.Eval("ItemImageWidth")%>'
					height='<%# Container.Eval("ItemImageHeight")%>'
					border=0></td>
			<td><img src='<%# Container.Eval("DummyImage")%>'
					width=2
					height=16
					border=0></td>
			<td id=sltl nowrap><%# Container.Eval("Text")%></td>
			</tr></table></Template>
		
	</sl:TreeLinkTemplate>

	<sl:TreeLinkTemplate runat="server"
		CustomType="Disabled"
		Type="Item">

		<Template><table border=0 cellpadding=1 cellspacing=0><tr>
			<td><img 
					<%# Container.DefineElement("ItemImage")%>
					src='<%# Container.Eval("ItemImage")%>'
					width='<%# Container.Eval("ItemImageWidth")%>'
					height='<%# Container.Eval("ItemImageHeight")%>'
					border=0></td>
			<td><img src='<%# Container.Eval("DummyImage")%>'
					width=2
					height=16
					border=0></td>
			<td id=sltl nowrap><%# Container.Eval("Text")%></td>
			</tr></table></Template>
		
	</sl:TreeLinkTemplate>

	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Knot">

		<Template><img
				<%# Container.DefineElement("KnotImage")%>
				src='<%# Container.Eval("KnotImage")%>'
				width='<%# Container.Eval("KnotImageWidth")%>'
				height='<%# Container.Eval("KnotImageHeight")%>'
				border=0></Template>
		
	</sl:TreeLinkTemplate>
	
	<sl:TreeLinkTemplate runat="server"
		CustomType="Default"
		Type="Spacer">

		<Template><img src='<%# Container.Eval("SpacerImage")%>'
				width='<%# Container.Eval("SpacerImageWidth")%>'
				height='<%# Container.Eval("SpacerImageHeight")%>'
				border=0></Template>
		
	</sl:TreeLinkTemplate>
	
</sl:TreeLink>