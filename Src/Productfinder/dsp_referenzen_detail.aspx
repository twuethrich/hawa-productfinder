<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<%@ Page language="c#" Codebehind="dsp_referenzen_detail.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_referenzen_detail" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
	<HEAD id="Head" runat="server">
		<title>
			</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<Script language="JavaScript" src="jscript/general.js" type="text/javascript"></Script>
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<form name="detail" runat="server" ID="Form1">
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING ID="String1" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<!--
							<tr>
								<td align="right" colSpan="2"><a class="Language">Deutsch</a>&nbsp;|&nbsp;<a class="Language">English</a></td>
								<td></td>
							</tr>-->
						</table>
						<!-- Title & Language, start-->
					</td>
				</tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30"><IMG height="10" src="images/ts.gif" width="10"><alpha:string id="string" runat="server" Key="TipText"></alpha:string></td>
					</TD>
					<td align="right" style="padding-right:10px"><A href="javascript:history.go(-1);"><ASP:IMAGE IMAGEURL="images/btn_back_de.gif" border="0" Runat="server" ID="btnBack"></ASP:IMAGE></A></td>
				</tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
			</table>
			<!-- header, end-->
			<table cellSpacing="0" cellPadding="0" width="880" border="0" align="center">
				<tr>
					<td colspan="2" class="BoxTitle"><asp:label id="lblShort" runat="server">label</asp:label></td>
				</tr>
				<tr class="BoxText">
					<td width="200" valign="top" class="BoxText"><asp:image id="imgRef" runat="server" Width="200px" Height="200px"></asp:image></td>
					<td width="100%" valign="top" class="BoxText">
						<b>
							<ALPHA:STRING ID="String2" RUNAT="server" KEY="Description"></ALPHA:STRING></b>
						<br>
						<asp:label id="lblDescription" runat="server">Beschreibung</asp:label>
						<br>
						<br>
						<b>
							<ALPHA:STRING ID="String3" RUNAT="server" KEY="Architect"></ALPHA:STRING></b>
						<br>
						<asp:label id="lblArchitect" runat="server">label</asp:label>
						<br>
						<br>
						<b>
							<ALPHA:STRING ID="String4" RUNAT="server" KEY="Realisator"></ALPHA:STRING></b>
						<br>
						<asp:label id="lblRealisator" runat="server">label</asp:label></B>
						<br>
						<br>
						<b>
							<ALPHA:STRING ID="String5" RUNAT="server" KEY="Products"></ALPHA:STRING></b>
						<br>
						<asp:label id="lblProduct" runat="server" Width="262px">label</asp:label>
					</td>
				</tr>
			</table>
			<!-- old -->
			<!--<td align="center" colSpan="2"><asp:hyperlink id="lnkNewSearch" runat="server" NavigateUrl="dsp_referenzen.aspx?search=0">Neue Suche</asp:hyperlink>&nbsp;|&nbsp;
						<asp:hyperlink id="lnkOldSearch" runat="server" NavigateUrl="javascript:history.go(-1)">Zurick</asp:hyperlink></td>-->
		</form>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-3201274-1");
            pageTracker._trackPageview();
            pageTracker._setDomainName("none");
            pageTracker._setAllowLinker(true);
            pageTracker._setAllowHash(false);
        } catch (err) { }</script>
	</body>
</HTML>
