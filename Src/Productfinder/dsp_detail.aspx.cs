using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Hawa.Classes;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r dsp_detail.
	/// </summary>
	public partial class dsp_detail : System.Web.UI.Page
	{
        protected Alpha.Controls.String DownloadZone;
        public static DataTable tabDetail;
        public static DataTable tabBestellangaben;
        public static DataTable tabPlanung;
		public static DataTable tabBerechnungshilfe;
        public static DataTable tabZertifikat;
        public static DataTable tabProspekte;
        public static DataTable tabAusschreibungstexte;
		public static DataTable tabDownload_dxf;
		public static DataTable tabDownload_eps;
		public static DataTable tabDownload_foto1;
		public static DataTable tabDownload_foto2;
		public static DataTable tabDownload_foto3;
		public static DataTable tabBezugsquellen;
		public static DataTable tabReferenzen;
		public static DataTable tabAccessories;
        public static DataTable tabProductVideo;
        public static DataTable tabMontageVideo;
        public static string pName;
		
		//protected System.Web.UI.WebControls.Imge objGroupSymbol;
	
		public static DataTable tabDownload;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlImage GroupSymbol;

		public static string flashID;
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try { 
				// Read productdetail informations
				int artID = Convert.ToInt32(Request.Params.Get("artID"));
				tabDetail = Model.ProductDetail.getProductDetail(artID).Tables[0];
				// fill the information to the page
               
                int callOverview = 0;
                if (Request.Params.Get("overview") != null)
                    callOverview = Convert.ToInt32(Request.Params.Get("overview"));

                Session.Add("productName", tabDetail.Rows[0]["Name"].ToString());

               
                btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
                btnBack.ImageUrl = "images/btn_back_" + HttpContext.Current.Session["languageID"].ToString() + ".gif";
                

				btnEn.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=En";
				btnDe.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=De";
				btnFr.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=Fr";
				btnEs.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=Es";
				
				
				int langID = 0;
				if(HttpContext.Current.Session["languageID"].ToString() == "EN"){
					langID=1;
				}
				if(HttpContext.Current.Session["languageID"].ToString() == "FR"){
					langID=2;
				}
                if (HttpContext.Current.Session["languageID"].ToString() == "ES")
                {
                    langID = 3;
                }
	
	
				
				
			    if(tabDetail.Rows.Count>0)
			    {


                    if(Convert.ToInt32(tabDetail.Rows[0]["Junior"].ToString()) == 1){
                        //string overviewURL = System.Configuration.ConfigurationSettings.AppSettings["overviewURL"] + HttpContext.Current.Session["languageID"].ToString() + "/OverviewApplication.html";
                        juniorPreview.Visible = true;
                        juniorPreview.Style.Add("Visibility", "visible");

                        juniorPreview.InnerHtml = "<a href='http://www.hawa.ch/index.php?id=471' title='HAWA-Junior Sortiment'><img src='images/overview_tool.jpg' border='0'  alt='HAWA-Junior Sortiment' ></a>";
                        if (callOverview == 1) { 
                            juniorPreview.InnerHtml = "<a href='#' onClick='javascript:window.close();' title='HAWA-Junior Sortiment'><img src='images/overview_tool.jpg' border='0'  alt='HAWA-Junior Sortiment' ></a>";
                        }
                    }
                
                
                
                    lblProductName.Text = tabDetail.Rows[0]["Name"].ToString();
				    lblShort.Text = tabDetail.Rows[0]["Kurztext"].ToString();
				    lblProduct.Text = tabDetail.Rows[0]["Produkt"].ToString();
				    lblAnwendung.Text = tabDetail.Rows[0]["Anwendung"].ToString();
				    lblTypisch.Text = tabDetail.Rows[0]["Typisch"].ToString();
				    lblProductN.Text = tabDetail.Rows[0]["Name"].ToString();

                    pName = tabDetail.Rows[0]["Name"].ToString();

                    Header.Title = "HAWA-Productfinder/" + tabDetail.Rows[0]["Name"].ToString();

				
				    if(lblAnwendung.Text == ""){
					    plAnwendung.Visible = false;
					    typisch.Height="330";
					    anwendung.Height="0";
					    spacer.Visible=false;
				    }
				    if(lblTypisch.Text == ""){
					    plTypisch.Visible=false;
					    anwendung.Height="330";
					    typisch.Height="0";
				    }
				
				
				
				    int symbolID = Convert.ToInt32(tabDetail.Rows[0]["Systemsymbol"]);
				    imgProductSymbol.ImageUrl = "dsp_objectdownload.aspx?typ=0&id=" + symbolID;
					
				    string bildID = tabDetail.Rows[0]["Systembild"].ToString();
				    if(bildID == null){
					    imgProductPicture.ImageUrl = "images\ts.gif";
				    }else{
				
					    imgProductPicture.ImageUrl ="dsp_objectdownload.aspx?typ=5&id=" + bildID;
				    }
					
				    string symbolGroupID = "";
				    if(tabDetail.Rows[0]["Groupsymbol"] != System.DBNull.Value)
				    {
					    symbolGroupID = tabDetail.Rows[0]["Groupsymbol"].ToString();
				    }
				    flashID = symbolGroupID;
				
				
			
			    }

				tabBestellangaben = Model.ArticleAndGroup.getLanguageObjects(artID, 35,-1).Tables[0];
				tabPlanung = Model.ArticleAndGroup.getPlanung(artID, 40,44,-1).Tables[0];
				tabBerechnungshilfe = Model.ArticleAndGroup.getLanguageObjects(artID, 41,-1).Tables[0];
				tabZertifikat = Model.ArticleAndGroup.getLanguageObjects(artID, 42,-1).Tables[0];
				tabProspekte = Model.ArticleAndGroup.getLanguageObjects(artID, 39,-1).Tables[0];

                tabAusschreibungstexte = Model.ArticleAndGroup.getAusschreibungstext(artID, 50, 51).Tables[0];
				
                tabDownload_dxf = Model.ArticleAndGroup.getArticleAndGroup(artID, 38,-1).Tables[0];
				tabDownload_eps = Model.ArticleAndGroup.getArticleAndGroup(artID, 37,-1).Tables[0];
				tabDownload_foto1 = Model.ArticleAndGroup.getArticleAndGroup(artID, 29, -1).Tables[0];
				tabDownload_foto2 = Model.ArticleAndGroup.getArticleAndGroup(artID, 30, -1).Tables[0];
				tabDownload_foto3 = Model.ArticleAndGroup.getArticleAndGroup(artID, 31, -1).Tables[0];
				tabBezugsquellen = Model.Bezugsquellen.getBezugsquellenSearch("AND CSOSCA.ART_ID=" + artID.ToString() + " AND CSOS.SOS_Parent_ID IS NULL" , 0).Tables[0];
				tabReferenzen = Model.Reference.getReferenceSearch("AND ((SELECT COUNT(*) from CLAPP_Article_Reference CAR2  where  CAR2.REF_ID = CAR.REF_ID and CAR2.ART_ID = "+ artID.ToString() +") > 0)").Tables[0];
				tabAccessories = Model.ArticleAndGroup.getRelAccessoire(artID).Tables[0];
                //tabProductVideo = Model.ArticleAndGroup.getVimeoVideos(artID, 54).Tables[0];
                //tabMontageVideo = Model.ArticleAndGroup.getVimeoVideos(artID, 55).Tables[0];

			
//				}


                if (tabAusschreibungstexte.Rows.Count > 0)
                {
                    string file = tabAusschreibungstexte.Rows[0]["OBJ_FileName"].ToString();
                    Ausschreibungstexte.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
                    Ausschreibungstexte.Attributes.Add("onClick", "javascript:openWindow('dsp_ausschreibung.aspx?artID=" + artID + "', 'new_window', 450,450)"); 
                }
                else
                {
                    Ausschreibungstexte.Attributes.Add("Style", "color:#909090;");
                }


				if (tabBestellangaben.Rows.Count > 0)
				{
					string file = tabBestellangaben.Rows[0]["OBJ_FileName"].ToString();
					Bestellangaben.NavigateUrl = "dsp_objectdownload.aspx?typ=2&group=35&id=" + file;
                    Bestellangaben.Attributes.Add("onClick", "javascript:pageTracker._trackEvent('Bestellangaben','" + pName + "','" + file +"')"); 
				}
				else
				{
					Bestellangaben.Attributes.Add("Style", "color:#909090;");
				}
				//Bestellangaben.NavigateUrl = "javascript:window.showModalDialog('dsp_bestellangaben.aspx?artID=" + artID + "', 'new_window', 'dialogHeight:350px;dialogWidth:350px;help:no;resizable:no;status:no;scroll:no;')";
				if (tabPlanung.Rows.Count > 0)
				{
					Planung.NavigateUrl = "#";
					Planung.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					Planung.Attributes.Add("onClick", "javascript:openWindow('dsp_planung_montage.aspx?artID=" + artID + "', 'new_window', 450,450)"); 
				}
				else{
					Planung.Attributes.Add("Style", "color:#909090;");
				}
				if (tabBerechnungshilfe.Rows.Count > 0)
				{
					Berechnungshilfe.NavigateUrl = "#";
					Berechnungshilfe.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					Berechnungshilfe.Attributes.Add("onClick", "javascript:openWindow('dsp_download_berechnungshilfe.aspx?artID=" + artID + "', 'new_window', 450,450)"); 
				}
				else{
					Berechnungshilfe.Attributes.Add("Style", "color:#909090;");
				}
				//Planung.NavigateUrl = "javascript:window.showModalDialog('dsp_planung_montage.aspx?artID=" + artID + "', 'new_window', 'dialogHeight:350px;dialogWidth:450px;help:no;resizable:no;status:no;scroll:no;')";
				if (tabZertifikat.Rows.Count > 0)
				{
					Zertifikate.NavigateUrl = "#";
					Zertifikate.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					Zertifikate.Attributes.Add("onClick", "javascript:openWindow('dsp_zertifikate.aspx?artID=" + artID + "', 'new_window', 450,450)"); 
				}
				else{
					Zertifikate.Attributes.Add("Style", "color:#909090;");
				}
				//Zertifikate.NavigateUrl = "javascript:window.showModalDialog('dsp_zertifikate.aspx?artID=" + artID + "', 'new_window', 'dialogHeight:350px;dialogWidth:350px;help:no;resizable:no;status:no;scroll:no;')";
				FAQ.NavigateUrl = "#";
				//FAQ.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
				//FAQ.Attributes.Add("onClick", "javascript:window.open('dsp_prospekte.aspx?artID=" + artID + "', 'new_window', 'height=350px, width=350px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')"); 
				//FAQ.NavigateUrl = "";
                int directMail = Convert.ToInt32(tabDetail.Rows[0]["ContactSales"]);
                string SourceOfSupplayContact = System.Configuration.ConfigurationSettings.AppSettings["SourceOfSupplayContact"].ToString();
             
                if (tabBezugsquellen.Rows.Count > 0 || directMail == 1)
				{
                    Bezugsquellen.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
                    if (directMail == 1)
                        Bezugsquellen.NavigateUrl = "mailto:" + SourceOfSupplayContact.ToString();
                    else
                    {
                        Bezugsquellen.NavigateUrl = "#";
                        Bezugsquellen.Attributes.Add("onClick", "javascript:openWindow('dsp_bezugsquellen.aspx?artID=" + artID + "&title=" + tabDetail.Rows[0]["Name"].ToString() + "', 'new_window', 900,675)");
                    }
                }
				else{
					Bezugsquellen.Attributes.Add("Style", "color:#909090;");
				}
                  
               	if (tabProspekte.Rows.Count > 0)
				{
					Prospekte.NavigateUrl = "#";
					Prospekte.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					Prospekte.Attributes.Add("onClick", "javascript:openWindow('dsp_prospekte.aspx?artID=" + artID + "', 'new_window', 450,450)"); 
				}
				else{
					Prospekte.Attributes.Add("Style", "color:#909090;");
				}
				//Prospekte.NavigateUrl = "javascript:window.showModalDialog('dsp_prospekte.aspx?artID=" + artID + "', window, 'dialogHeight:350px;dialogWidth:350px;help:no;resizable:no;status:no;scroll:no;')";
				if (tabReferenzen.Rows.Count > 0)
				{				
					Referenzen.NavigateUrl = "#";
					Referenzen.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					Referenzen.Attributes.Add("onClick", "javascript:openWindow('dsp_referenzen_search.aspx?artID=" + artID + "','new_window', 900,675)");
				}
				else{
					Referenzen.Attributes.Add("Style", "color:#909090;");
				}

              


				//Referenzen.Attributes.Add("onClick", "javascript:window.open('dsp_referenzen.aspx?artID=" + artID + "', 'new_window', 'height=850px, width=650px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')");
				//Referenzen.NavigateUrl = "javascript:window.showModalDialog('dsp_referenzen.aspx?artID=" + artID + "', window, 'dialogHeight:450px;dialogWidth:450px;help:no;resizable:no;status:no;scroll:no;')";
				Preise.NavigateUrl = "";
				//download_dfx.NavigateUrl = "javascript:window.open('dsp_download_dxf.aspx?artID=" + artID + "', 'window', 'height=550px, width=400px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')";
				if (tabDownload_dxf.Rows.Count > 0)
				{
					download_dfx.NavigateUrl = "#";
					download_dfx.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					//download_dfx.Attributes.Add("onClick", "javascript:openWindow('dsp_download_dxf.aspx?artID=" + artID + "', 'new_window', 600,500)"); 
					download_dfx.Attributes.Add("onClick", "javascript:openWindow('dsp_dxfinfo.aspx?artID=" + artID + "', 'new_window', 600,500)"); 
				}
				else{
					download_dfx.Attributes.Add("Style", "color:#909090;");
				}
				//download_dfx.NavigateUrl = "javascript:window.showModalDialog('dsp_download_dxf.aspx?artID=" + artID + "', window, 'dialogHeight:550px;dialogWidth:450px;help:no;resizable:no;status:no;scroll:no;')";
				if (tabDownload_eps.Rows.Count > 0)
				{
					download_eps.NavigateUrl = "#";
					download_eps.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					download_eps.Attributes.Add("onClick", "javascript:openWindow('dsp_download_eps.aspx?artID=" + artID + "', 'new_window', 600,500)"); 
				}
				else{
					download_eps.Attributes.Add("Style", "color:#909090;");
				}
				//download_eps.NavigateUrl = "javascript:window.showModalDialog('dsp_download_eps.aspx?artID=" + artID + "', window, 'dialogHeight:350px;dialogWidth:350px;help:no;resizable:no;status:no;scroll:no;')";
				if (tabDownload_foto1.Rows.Count > 0 ||tabDownload_foto2.Rows.Count >0 || tabDownload_foto3.Rows.Count >0)
				{
					download_fotos.NavigateUrl = "#";
					download_fotos.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					download_fotos.Attributes.Add("onClick", "javascript:openWindow('dsp_download_fotos.aspx?artID=" + artID + "', 'new_window', 600,720)"); 
				}
				else{
					download_fotos.Attributes.Add("Style", "color:#909090;");
				}
			
				if (tabAccessories.Rows.Count > 0 ) {
					accessories.NavigateUrl = "#";
					accessories.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					accessories.Attributes.Add("onClick", "javascript:openWindow('acc_frm_main.aspx?artID=" + artID + "', 'new_window', 900,675)");					
				}
				else{
					accessories.Attributes.Add("Style", "color:#909090;");
					
				}

               /* if (tabProductVideo.Rows.Count > 0)
                {
                    productMovies.NavigateUrl = "#";
                    productMovies.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
                    productMovies.Attributes.Add("onClick", "javascript:openWindow('dsp_vimeo.aspx?artID=" + artID + "', 'vimeo', 600,300)");
                }
                else
                {
                    productMovies.Attributes.Add("Style", "color:#909090;");

                }
                if (tabMontageVideo.Rows.Count > 0)
                {
                    montageMovies.NavigateUrl = "#";
                    montageMovies.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
                    montageMovies.Attributes.Add("onClick", "javascript:openWindow('dsp_vimeo.aspx?artID=" + artID + "', 'vimeo', 600,300)");
                }
                else
                {
                    montageMovies.Attributes.Add("Style", "color:#909090;");

                }*/
				
				
				//Pressemitteilungen Webseite
				string presseURL = System.Configuration.ConfigurationSettings.AppSettings["PresselinkURL"]+"?L="+langID;
				Presse.Attributes.Add("onClick", "javascript:opener.location=('" + presseURL + "'); opener.focus()"); 
				Presse.NavigateUrl = "#";
				//FAQ-Seite Webseite
				string faqURL = System.Configuration.ConfigurationSettings.AppSettings["FAQlinkURL"]+"?L="+langID;
				FAQ.Attributes.Add("onClick", "javascript:opener.location=('" + faqURL + "'); opener.focus()"); 
				FAQ.NavigateUrl = "#";

				//VIP-Room Preise-Liste Webseite
				string priceURL = System.Configuration.ConfigurationSettings.AppSettings["PricelinkURL"]+"?L="+langID;
				Preise.Attributes.Add("onClick", "javascript:opener.location=('" + priceURL + "'); opener.focus()"); 
				Preise.NavigateUrl = "#";

				//VIP-Room  Webseite
				string vipUrl = System.Configuration.ConfigurationSettings.AppSettings["PricelinkURL"]+"?L="+langID;
				viproom.Attributes.Add("onClick", "javascript:opener.location=('" + vipUrl + "'); opener.focus()"); 
				viproom.NavigateUrl = "#";

				//download_fotos.NavigateUrl = "javascript:window.showModalDialog('dsp_download_fotos.aspx?artID=" + artID + "', window, 'dialogHeight:350px;dialogWidth:350px;help:no;resizable:no;status:no;scroll:no;')";
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");
			}
		}
		

		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
