using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for dsp_referenzen.
	/// </summary>
	/// 
	
	public partial class dsp_referenzen_search :  Alpha.Page
	{
		protected System.Web.UI.WebControls.Label lblShort;
		public int iObj;
		public DataTable dtReference = new DataTable();
		string strNextColor = "WhiteSmoke";
		protected Alpha.Controls.String Seite;
		
		string sOrder = "";

		//fill table with search data
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
                if (!Page.IsPostBack) {                                        
                    R.sReferenceSort = "REL_Place";                                  
                }
                if ((R.sReferenceSort.Length>0) && (R.sReferenceSort.Substring(R.sReferenceSort.Length -4) == "DESC"))
                    sOrder = "5";                
                else
                    sOrder = "6";
                
				dgrReference.Columns[0].HeaderText = getString("land",true);
				dgrReference.Columns[1].HeaderText = getString("ort",true);
				dgrReference.Columns[2].HeaderText = getString("objekt",true);
				dgrReference.Columns[3].HeaderText = getString("art",true);
				dgrReference.Columns[4].HeaderText = getString("system",true);
				dgrReference.Columns[5].HeaderText = getString("material",true);
				dgrReference.Columns[6].HeaderText = getString("vorhaben",true);
				btnPrint.ImageUrl="images/btn_print_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";

                //Aufruf �ber Systemdetail
				if(Request.Params.Get("artID")!= null)
				  R.strWhere = "AND ((SELECT COUNT(*) from CLAPP_Article_Reference CAR2  where  CAR2.REF_ID = CAR.REF_ID and CAR2.ART_ID = " + Request.Params.Get("artID").ToString() + ") > 0) " ; 
				dtReference = Model.Reference.getReferenceSearch(R.strWhere).Tables[0];
				dgrReference.DataSource = dtReference;
				dgrReference.DataBind();
                //DataSet dsSort = new DataSet ();
                //dsSort = Model.Bezugsquellen.getBezugsquellenSearch(sWhere);
                DataView vSort = new DataView(dtReference);                
                vSort.Sort = R.sReferenceSort;                                      
                dgrReference.DataSource = vSort;
                dgrReference.DataBind();     
 
				btnPrint.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
				btnPrint.Attributes.Add("onClick", "javascript:openWindow('dsp_referenz_print.aspx', 'print', 650,675)"); 

				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
			

			if(Request.Params.Get("artID")!= null)
			{

				

				this.btn_close.ImageUrl="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				this.btn_close.NavigateUrl="javascript:window.close();";
				this.lnkNewSearch.Visible = false;
				this.lnkOldSearch.Visible = false;
				this.plFooter.Visible = false;
			}
			else
			{
				this.btn_close.ImageUrl="images/btn_back_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				this.btn_close.NavigateUrl="javascript:history.back(-1)";
				this.lnkNewSearch.Visible = true;
				this.lnkOldSearch.Visible = true;
				this.plFooter.Visible = true;
			}
		}
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgrReference.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.OnItemCreated);
			this.dgrReference.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgrReference_PageIndexChanged);
			this.dgrReference.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgrReference_SortCommand);
			this.dgrReference.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrReference_ItemDataBound);

		}
		#endregion
		
		//set paging 
		private void dgrReference_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				Reference R;
				R = (Reference)(HttpContext.Current.Session["reference"]);
					DataSet dsSort = new DataSet ();
				dsSort = Model.Reference.getReferenceSearch(R.strWhere);
				DataView vSort = new DataView(dsSort.Tables[0]);
				vSort.Sort = R.sReferenceSort;
				dgrReference.DataSource = vSort;
				if ((vSort.Sort.Length>0) && (vSort.Sort.Substring(vSort.Sort.Length - 4) == "DESC"))
				{
					sOrder = "5";
				}
				else
				{
					sOrder = "6";
				}
				dgrReference.CurrentPageIndex = e.NewPageIndex ;
				dgrReference.DataBind();
				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}
		//group rows and set color of row and set links for detail
		private void dgrReference_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				int iObject = 0;

				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
				{
					Reference R = (Reference)(HttpContext.Current.Session["reference"]);
					/*
                    for (int i=0; i < dgrReference.Columns.Count ; i++)
					{
						if (R.sReferenceSort == dgrReference.Columns[i].SortExpression || R.sReferenceSort == dgrReference.Columns[i].SortExpression + " DESC")
						{
							Label lblSorted = new Label();
							lblSorted.Font.Name = "webdings";
							lblSorted.Font.Size = FontUnit.XSmall;
							lblSorted.Text = sOrder;
							e.Item.Cells[i].Controls.Add(lblSorted);
						}
					}*/
					strNextColor = "WhiteSmoke";
					return;
				}
			
				iObject = Convert.ToInt32(((TableCell)(e.Item.Controls[10])).Text);
				
				
				

				if (Request.Params.Get("artID") == null)
				{
					iObject = -1;
				}
				int iRow =  e.Item.DataSetIndex;
				if (iObj != iObject || iRow == 0)
				{
					if (strNextColor == "WhiteSmoke")
					{
						strNextColor = "LightGray";
						//e.Item.BackColor = Color.LightGray;
						e.Item.CssClass = "BoxGrayAlternate";
						
					}
					else
					{
						strNextColor = "WhiteSmoke";
						//e.Item.BackColor = Color.WhiteSmoke;
						e.Item.CssClass = "BoxGrayBright";
					}
					iObj = Convert.ToInt32(((TableCell)(e.Item.Controls[10])).Text);

					string refImage = e.Item.Cells[12].Text.ToString();
					
					if(refImage.Length > 10 & refImage != ""){
						e.Item.Cells[2].Text = "<a href=\"dsp_referenzen_detail.aspx?refID=" + e.Item.Cells[10].Text + "\">" + e.Item.Cells[2].Text + "</a>";
					}else{
						e.Item.Cells[2].Text = e.Item.Cells[2].Text;
					}

					


				}
				else
				{
					if (strNextColor == "WhiteSmoke")
					{
						//e.Item.BackColor = Color.WhiteSmoke;
						e.Item.CssClass = "BoxGrayBright";
					}
					else
					{
						//e.Item.BackColor = Color.LightGray;
						e.Item.CssClass = "BoxGrayAlternate";

					}
					((TableCell)(e.Item.Controls[0])).Text = "";
					((TableCell)(e.Item.Controls[1])).Text = "";
					((TableCell)(e.Item.Controls[2])).Text = "";
					((TableCell)(e.Item.Controls[3])).Text = "";
				
					
				}
				//}
			
				
				
				if("True".Equals(e.Item.Cells[13].Text.ToString())){
					e.Item.Cells[4].Text = "<a href=\"dsp_detail.aspx?artID=" + e.Item.Cells[11].Text + "\">" + e.Item.Cells[4].Text + "</a>";
				}
				/// higlited for artID
				if(Request.Params.Get("artID")!= null)
				{
				 
				 if(Request.Params.Get("artID").ToString() == e.Item.Cells[11].Text)
  				 {
					e.Item.Cells[4].Font.Bold = true;
				 }
					
				}
	
				if (e.Item.Cells[7].Text == "&nbsp;" && e.Item.Cells[8].Text == "&nbsp;")
				{
				}
				else
				{
					//    				e.Item.Cells[4].Text = "<a href=\"dsp_referenzen_detail.aspx?refID=" + e.Item.Cells[10].Text + "&artID=" + e.Item.Cells[11].Text + "\">" + e.Item.Cells[4].Text + "</a>";
				}
				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		private void dgrReference_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			try
			{
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
				// Wenn artID vorhandeln dann nur die Referenzen, die artID haben anzeigen
				if(Request.Params.Get("artID")!= null)
					R.strWhere = "AND ((SELECT COUNT(*) from CLAPP_Article_Reference CAR2  where  CAR2.REF_ID = CAR.REF_ID and CAR2.ART_ID = " + Request.Params.Get("artID").ToString() + ") > 0) " ; 
				DataSet dsSort = new DataSet ();
				dsSort = Model.Reference.getReferenceSearch(R.strWhere);
				DataView vSort = new DataView(dsSort.Tables[0]);
				vSort.Sort = e.SortExpression.ToString();	
				if (R.sReferenceSort == e.SortExpression.ToString())
				{
					vSort.Sort = e.SortExpression.ToString() + " DESC";
					sOrder = "5";
				}
				else
				{
					vSort.Sort = e.SortExpression.ToString();
					sOrder = "6";
				}
				R.sReferenceSort = vSort.Sort;
				dgrReference.DataSource = vSort;
				dgrReference.DataBind();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		protected void dgrReference_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void OnItemCreated(object sender, DataGridItemEventArgs e) {
			if(e.Item.ItemType==ListItemType.Pager) {
				Label lblPager = new Label();
				lblPager.Text = "<b>" + getString("page",false)+"</b>" + lblPager.Text;
				e.Item.Controls[0].Controls.AddAt(0,lblPager);
			}
		}

	
	}
}
