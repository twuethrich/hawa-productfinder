using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;


namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for acc_frm_main.
	/// </summary>
	public partial class acc_frm_main : Alpha.Page
	{
		protected Alpha.Controls.String tipText;
		protected System.Web.UI.WebControls.Label lblProductName;
		public static DataTable tabSystem;
		public static DataTable tabRelSystem;
		public static DataTable tabPDF;
		string accName;
		public static DataTable tabBestellangaben;
		public static DataTable tabPlanung;
		public static DataTable tabBerechnungshilfe;
		public static DataTable tabZertifikat;
		public static DataTable tabProspekte;
		public static DataTable tabDownload_dxf;
		public static DataTable tabDownload_eps;
		protected Alpha.Controls.String String4;
		protected System.Web.UI.HtmlControls.HtmlTableRow sysTitle;
		int agrID;
		
		//fill tables with data for accessories
		protected void Page_Load(object sender, System.EventArgs e)
		{

			try 
			{
				int artID = Convert.ToInt32(Request.Params.Get("artID"));
				int matID = Convert.ToInt32(Request.Params.Get("matID"));
				agrID = Convert.ToInt32(Request.Params.Get("agrID"));
				

				if(Request.Params.Get("languageID") != null)
				{
					Session.Remove("languageID");
					Session.Add("languageID", Request.Params.Get("languageID").ToString());
				}
                if(Request.Params.Get("name") != null)
				{
                    Session.Add("productName", Request.Params.Get("name").ToString());
                }
				int langID = 0;
				if(HttpContext.Current.Session["languageID"].ToString() == "EN")
				{
					langID=1;
				}
				if(HttpContext.Current.Session["languageID"].ToString() == "FR")
				{
					langID=2;
				}
				if(HttpContext.Current.Session["languageID"].ToString() == "ES")
				{
					langID=3;
				}

				if(Request.Params.Get("name") != null)
				{
					tabRight.Visible= true;
					space.Visible=false;
					accName = Request.Params.Get("name").ToString();
					lnkDetail.Text = getString("info", false);
					Label1.Text = accName;
				}
				else
				{
					Label1.Text = "";
					tabRight.Visible=false;
					space.Visible=true;
				}

				if(matID > 0)
					Session.Add("material",matID);
			
				// Aufruf von der Produktdetail-Seite (mit Artikel-ID)
				if(artID > 0)
				{
					dgrAccessories.DataSource = Model.Accessories.getAccessoriesSelected(artID);
					btnEn.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=En";
					btnDe.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=De";
					btnFr.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=Fr";
					btnEs.NavigateUrl = Request.Path + "?artID=" + artID + "&languageID=Es";
				}
				// Allgemeiner Aufruf(Ohne Einschränkung)
				else
				{
					dgrAccessories.DataSource = Model.Accessories.getAccessories();
					btnEn.NavigateUrl = Request.Path + "?languageID=En";
					btnDe.NavigateUrl = Request.Path + "?languageID=De";
					btnFr.NavigateUrl = Request.Path + "?languageID=Fr";
					btnEs.NavigateUrl = Request.Path + "?languageID=Es";
				}
					
				
				dgrAccessories.Columns[0].HeaderText = " &nbsp;&nbsp;&nbsp;&nbsp;" + getString("Products",true);
				dgrAccessories.Columns[7].HeaderText = "&nbsp; Material";


				dgrAccessories.DataBind();
				





				//Falls auf der Accessoire-Seite ein bestimmte Gruppe aufgerufen wurde
				if (agrID != 0) 
				{
					

					//Accessoire Gruppen-Symbol
					string FileName = Model.Accessories.getAccessoriesFile(agrID);
					imgSymbol.ImageUrl="dsp_objectdownload.aspx?typ=7&group=34&id=" + FileName ;

					//Accessoire-Systeme
					tabSystem = Model.Accessories.getAccessoriesSystem(agrID,7).Tables[0];

				
					//Falls Productfinder-Detailseite vorhanden
					if (tabSystem.Rows.Count > 0)
					{
						lnkDetail.NavigateUrl = "dsp_detail.aspx?artID=" + tabSystem.Rows[0]["art_ID"].ToString();
						download.Visible = false;
						systemDetail.Visible = true;
						//sysSpace.Visible = true;
						//sysTitle.Visible = true;
					}
					else
					{
						download.Visible = true;						
						systemDetail.Visible = false;
						//sysTitle.Visible = false;
						//sysSpace.Visible = false;
						
						// Bestellangaben 
						tabBestellangaben = Model.ArticleAndGroup.getLanguageObjects(agrID, 36, 1).Tables[0];
						if (tabBestellangaben.Rows.Count > 0)
						{
							string file = tabBestellangaben.Rows[0]["OBJ_FileName"].ToString();
							Bestellangaben.NavigateUrl = "dsp_objectdownload.aspx?typ=2&group=36&id=" + file;
				
						}
						else
						{
							Bestellangaben.Attributes.Add("Style", "color:#909090;");
						}
						// Planung&Monatageanleitung Download
						tabPlanung = Model.ArticleAndGroup.getPlanung(agrID, 46,47,1).Tables[0];
						if (tabPlanung.Rows.Count > 0)
						{
							Planung.NavigateUrl = "#";
							Planung.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
							Planung.Attributes.Add("onClick", "javascript:openWindow('dsp_planung_montage.aspx?agrID=" + agrID + "', 'new_window', 450,450)"); 
						}
						else
						{
							Planung.Attributes.Add("Style", "color:#909090;");
						}
			
						// Prospekte Download
						tabProspekte = Model.ArticleAndGroup.getLanguageObjects(agrID, 45, 1).Tables[0];
						if (tabProspekte.Rows.Count > 0)
						{
							Prospekte.NavigateUrl = "#";
							Prospekte.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
							Prospekte.Attributes.Add("onClick", "javascript:openWindow('dsp_prospekte.aspx?agrID=" + agrID + "', 'new_window', 450,450)"); 
						}
						else
						{
							Prospekte.Attributes.Add("Style", "color:#909090;");
						}

						//DXF Download
						tabDownload_dxf = Model.ArticleAndGroup.getArticleAndGroup(agrID, 38, 1).Tables[0];
						if (tabDownload_dxf.Rows.Count > 0)
						{
							download_dfx.NavigateUrl = "#";
							download_dfx.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
							download_dfx.Attributes.Add("onClick", "javascript:openWindow('dsp_dxfinfo.aspx?agrID=" + agrID + "', 'new_window', 600,500)"); 
						}
						else
						{
							download_dfx.Attributes.Add("Style", "color:#909090;");
						}
						tabDownload_eps = Model.ArticleAndGroup.getArticleAndGroup(agrID, 37, 1).Tables[0];
						if (tabDownload_eps.Rows.Count > 0)
						{
							download_eps.NavigateUrl = "#";
							download_eps.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
							download_eps.Attributes.Add("onClick", "javascript:openWindow('dsp_download_eps.aspx?groupID=" + agrID + "', 'new_window', 600,500)"); 
						}
						else
						{
							download_eps.Attributes.Add("Style", "color:#909090;");
						}
					
						//VIP-Room Preise-Liste Webseite
						string priceURL = System.Configuration.ConfigurationSettings.AppSettings["PricelinkURL"]+"&L="+langID;
						Preise.Attributes.Add("onClick", "javascript:opener.location=('" + priceURL + "'); opener.focus()"); 
						Preise.NavigateUrl = "#";

						//VIP-Room  Webseite
						string vipUrl = System.Configuration.ConfigurationSettings.AppSettings["PricelinkURL"]+"&L="+langID;
						viproom.Attributes.Add("onClick", "javascript:opener.location=('" + vipUrl + "'); opener.focus()"); 
						viproom.NavigateUrl = "#";
					
					
					}


					//Kombinierbar mit diesen Systemen
					dgrRelevantSystem.DataSource = Model.Accessories.getAccessoriesSystem(agrID,0);
					dgrRelevantSystem.DataBind();
					tabRelSystem = Model.Accessories.getAccessoriesSystem(agrID,0).Tables[0];
					
					

				
					if(tabRelSystem.Rows.Count < 1)
						trRelSystem.Visible = false;
					
			



				}
				if (Request.Params.Get("back")!=null)
				{
					this.btn_back.ImageUrl="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
					this.btn_back.NavigateUrl="dsp_searchForm.aspx";
				}
				else
				{
					this.btn_back.ImageUrl="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
					this.btn_back.NavigateUrl="javascript:window.close();";
				}

				
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "AGR_ID=" + Request.Params.Get("agrID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		//add links for every rows to download PDF files (accessories)
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				int artID = Convert.ToInt32(Request.Params.Get("artID"));
				string name;
				string AGR_ParentID;
				int AGR_ID;
				
				ListItemType itemType = (ListItemType)e.Item.ItemType;

				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

				//gewählter Accessoire
				name = ((TableCell)(e.Item.Controls[0])).Text.ToString();
				

				AGR_ParentID = ((TableCell)(e.Item.Controls[3])).Text.ToString();
				e.Item.Cells[0].Height = 25;
				
				//ist keine Untergruppe
				if (AGR_ParentID != "&nbsp;")
				{
					
					//Materialsymbole einblenden
					int holz = 0;
					int glas = 0;
					int metall = 0;
					int hasChild;	
					int hasParent;	


					try 
					{
						holz = Convert.ToInt32(e.Item.Cells[8].Text.ToString());
						glas =Convert.ToInt32(e.Item.Cells[9].Text.ToString());
						metall = Convert.ToInt32(e.Item.Cells[10].Text.ToString());
						hasChild = Convert.ToInt32(e.Item.Cells[12].Text.ToString());
						hasParent = Convert.ToInt32(e.Item.Cells[13].Text.ToString());
				
					}
					catch(FormatException) 
					{
						holz = 0; glas = 0; metall = 0; hasChild=0; hasParent=0;
					}
					

					int openParent = 0;
					if (Request.Params.Get("parentID")!=null)
					{
						openParent = Convert.ToInt32(Request.Params.Get("parentID").ToString());
					}
					
					if(agrID == Convert.ToInt32(((TableCell)(e.Item.Controls[2])).Text.ToString()))
					{
						e.Item.ForeColor = System.Drawing.Color.DimGray;
						e.Item.Cells[0].Text = "<img src='images/arrow_red_large.gif'>" + name;		
						
					}
					else
					{
						e.Item.Cells[0].Text = "<img src='images/arrow_red_large.gif'><a href=\"acc_frm_main.aspx?agrID=" + ((TableCell)(e.Item.Controls[2])).Text.ToString() +"&artID=" + artID + "&name="+name+ "\">" + name + "</a>";
					}
					//Link auf Accessoire setzen
					if(hasChild == 1)
						e.Item.Cells[0].Text = "<img src='images/arrow_red_large.gif'><a href=\"javascript:showChild(" + ((TableCell)(e.Item.Controls[2])).Text.ToString()+")"+"\">" + name + "</a>";
					
					if(hasParent == 1)
					{
						if(agrID == Convert.ToInt32(((TableCell)(e.Item.Controls[2])).Text.ToString()))
						{
							e.Item.Cells[0].Text = "<img src='images/ts.gif' width='10'><img src='images/row.gif'> " + name;
						}
						else
						{
							e.Item.Cells[0].Text = "<img src='images/ts.gif' width='10'><img src='images/row.gif'> <a href=\"acc_frm_main.aspx?agrID=" + ((TableCell)(e.Item.Controls[2])).Text.ToString() +"&artID=" + artID + "&name="+name+  "&parentID=" + AGR_ParentID +"\">" + name + "</a>";
						}
						e.Item.BackColor= System.Drawing.Color.WhiteSmoke;
						e.Item.Attributes.Add("ID",AGR_ParentID);
						int actParent = Convert.ToInt32(AGR_ParentID);
						if(actParent != openParent)
							e.Item.Attributes.Add("Style","Display:none");
					}
					if(holz > 0)
						((TableCell)(e.Item.Controls[7])).FindControl("holz").Visible = true;
            		if(glas > 0) 
						((TableCell)(e.Item.Controls[7])).FindControl("glas").Visible = true;
                
					if(metall > 0)
						((TableCell)(e.Item.Controls[7])).FindControl("metall").Visible = true;
					
				}
				else 
				{
					//Falls Hauptgruppe ==> Nur als Überschrift darstellen
					e.Item.Cells[0].Text = "<b>" + name + "</b>";
					e.Item.Cells[0].Wrap = true;
				
				}
				
				// Gruppen-ID herauslesen
				AGR_ID = Convert.ToInt32(((TableCell)(e.Item.Controls[2])).Text);
				//horizontale Ausrichtung
				e.Item.Cells[1].HorizontalAlign = HorizontalAlign.Center;
				
		
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}
		
		#region fill table with accessories detail and set link for PDF file
		public void OnDataBoundRelevantSystem(object source, DataGridItemEventArgs e) 
		{
			try 
			{
				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

				string name = e.Item.Cells[2].Text.ToString();
				int art_ID = Convert.ToInt32(e.Item.Cells[1].Text.ToString());
				e.Item.Cells[0].Text = "<IMG src='images/arrow_red_large.gif'>";
				e.Item.Cells[0].Width = 5;
				e.Item.Cells[2].Text = "<a href=\"dsp_detail.aspx?artID="+art_ID+"\">" + name + "</a>";
				e.Item.Cells[2].Width = 300;

				

			}
			catch (Exception ex) 
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#endregion


	}
}
