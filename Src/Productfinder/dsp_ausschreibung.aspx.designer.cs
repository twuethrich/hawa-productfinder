﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace Hawa.Src.Productfinder {
    
    
    public partial class Ausschreibungstext {
        
        /// <summary>
        /// String9-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String9;
        
        /// <summary>
        /// btnClose-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image btnClose;
        
        /// <summary>
        /// String1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String1;
        
        /// <summary>
        /// Form1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm Form1;
        
        /// <summary>
        /// dgrAusschreibung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataGrid dgrAusschreibung;
    }
}
