<%@ Page language="c#" Codebehind="dsp_error_download.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_error_download" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td><img src="images/ts.gif" width="10" height="10"></td>
				<td rowspan="3"><img src="images/hawa_logo_small.png"></td>
				<td></td>
				<td><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right" width="100%"><a href="javascript: window.close();"><img src="images/btn_close_de.gif" border="0"></a></td>
				<td><img src="images/ts.gif" width="10"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<TR>
				<TD colspan="4" class="BoxTitle"><ALPHA:STRING ID="String1" RUNAT="server" KEY="Title"></ALPHA:STRING></TD>
			</TR>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
		</TABLE>
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" align="center">
			<tr>
				<td width="10"><img src="images/ts.gif" width="10" height="10"></td>
				<td class="FileNotFound"><ALPHA:STRING ID="String2" RUNAT="server" KEY="ErrorMsg"></ALPHA:STRING></td>
				<td width="10"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
		</TABLE>
		</FORM>
	</body>
</HTML>
