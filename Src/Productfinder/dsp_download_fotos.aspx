<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<%@ Page language="c#" Codebehind="dsp_download_fotos.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.download_fotos" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td><IMG height="10" src="images/ts.gif" width="10"></td>
				<td rowSpan="3"><IMG src="images/hawa_logo_small.png"></td>
				<td></td>
				<td><IMG height="10" src="images/ts.gif" width="10"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right" width="100%"><A href="javascript: window.close();"><asp:image id="btnClose" Runat="server" border="0" imageUrl="images/btn_close_de.gif"></asp:image></A></td>
				<td><IMG src="images/ts.gif" width="10"></td>
				<td></td>
			</tr>
			<tr>
				<td colSpan="4"><IMG height="10" src="images/ts.gif" width="10"></td>
			</tr>
			<TR>
				<TD class="BoxTitle" colSpan="4"><asp:label id="lblShort" runat="server">
						<ALPHA:STRING ID="String1" RUNAT="server" KEY="Title"></ALPHA:STRING>
					</asp:label></TD>
			</TR>
			<tr>
				<td colSpan="4"><IMG height="10" src="images/ts.gif" width="10"></td>
			</tr>
		</TABLE>
		<TABLE cellSpacing="1" cellPadding="0" width="580" align="center" border="0">
			<TR class="BoxGrayAlternate" id="bild1" runat="server">
				<TD align="center" width="150" bgColor="white"><asp:image id="Image1" runat="server"></asp:image></TD>
				<td width="350"><asp:label id="Label1" runat="server">Label</asp:label></td>
				<td align="right" width="80"><asp:label id="lblSize1" runat="server">Label</asp:label>&nbsp;&nbsp;
				</td>
			</TR>
			<TR class="BoxGrayBright" id="bild2" runat="server">
				<TD align="center" bgColor="white"><asp:image id="Image2" runat="server"></asp:image></TD>
				<td><asp:label id="Label2" runat="server">Label</asp:label></td>
				<td align="right"><asp:label id="lblSize2" runat="server">Label</asp:label>&nbsp;&nbsp;
				</td>
			</TR>
			<TR class="BoxGrayAlternate" id="bild3" runat="server">
				<TD align="center" bgColor="white"><asp:image id="Image3" runat="server"></asp:image></TD>
				<td><asp:label id="Label3" runat="server">Label</asp:label></td>
				<td align="right"><asp:label id="lblSize3" runat="server" CssClass="BoxGrayAlternate">Label</asp:label>&nbsp;&nbsp;
				</td>
			</TR>
			<tr>
				<td colSpan="4"><IMG height="10" src="images/ts.gif" width="10"></td>
			</tr>
			<TR id="Tr1" runat="server">
				<TD align="left" bgColor="white" colspan="3">
					<ALPHA:STRING ID="String2" RUNAT="server" KEY="infoText"></ALPHA:STRING>
					<ASP:HYPERLINK id="MarketingService" RUNAT="server" NAVIGATEURL=""><ALPHA:STRING ID="String4" RUNAT="server" KEY="infoText3"></ALPHA:STRING></ASP:HYPERLINK><ALPHA:STRING ID="String3" RUNAT="server" KEY="infoText2"></ALPHA:STRING>
				</TD>
			</TR>
		</TABLE>
		</FORM>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
     <script type="text/javascript">
         try {
             var pageTracker = _gat._getTracker("UA-3201274-1");
             pageTracker._trackPageview();
             pageTracker._setDomainName("none");
             pageTracker._setAllowLinker(true);
             pageTracker._setAllowHash(false);
         } catch (err) { }</script>
	</body>
</HTML>
