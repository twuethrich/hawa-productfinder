using System;

namespace Hawa.Src.Productfinder.Model.Other
{
	/// <summary>
	/// Article is a one row from CLAPP_Article.
	/// </summary>
	public  class Article
	{
// Private
		private int _art_id  ;
		private string _image;

// Properties		
		public  int iArt_ID 
		{
			get
			{
				return _art_id;
			}

			set 
			{
				_art_id = value;
			}
		}
		
		public  string  iImage 
		{
			get
			{
				return _image;
			}

			set 
			{
				_image = value;
			}
		}

		public void  New( int     id,
			        string  image
			      )
		{
			_art_id = id;
	        _image = image;
		}
		public Article()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
