using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Hawa.Src.Productfinder.Model.Menu;

namespace Hawa.Src.Productfinder.Model.Other
{
	// Creates and initializes several BitArrays.

	/// <summary>
	/// Summary description for PlanFuncMat.
	/// </summary>
	public class PlanFuncMat
	{
		static string sConnStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     

		private ArrayList _qwrtyList;
        private int CellSize; 
		private int Numbers; 

		private int tmpPlanID = -1;
		private int tmpFncID  = -1;
		private int tmpMtlID  = -1;
		int cnt = 0;

		public void Load ( Menu.Menu M )
		{ 
			CellSize = M.iPLANES.Count()*M.iFUNCTIONS.Count()*M.iMaterials.Count();
			Numbers = M.iPLANES.Count() + M.iFUNCTIONS.Count() + M.iMaterials.Count();

			SqlConnection sqlConnection = new SqlConnection(sConnStr);
			sqlConnection.Open();

			if (CellSize <= 0) 
			{
				throw new Exception("CellSize cannot be zero or negativ");
			}
			_qwrtyList = new ArrayList();

			string mySelectQuery = 
			"SELECT AP.PLA_ID, AF.FNC_ID, AM.MAT_ID FROM CLAPP_Article_PLan AP " +
			"inner join CLAPP_Article_Function AF on AP.ART_ID = AF.ART_ID " +
			"inner join CLAPP_Article_Material AM on AP.ART_ID=AM.ART_ID " +
            "inner join CLAPP_Article A on A.ART_ID = AP.ART_ID AND A.ART_Active = 1 " +
			"group by AP.PLA_ID, AF.FNC_ID, AM.MAT_ID;";
			
			//SqlCommand sqlCommand = new SqlConnection(sqlConnection);
			//SqlCommand sqlCommand = new SqlCommand("HWA_getPFM",sqlConnection);            
			//sqlCommand.CommandType = CommandType.StoredProcedure;
			SqlCommand sqlCommand = new SqlCommand(mySelectQuery,sqlConnection);
			SqlDataReader sqlDR = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
			setQList(sqlDR); 
    		sqlDR.Close();
		}

		private void setQList(  SqlDataReader  QDR )
		{
			while (QDR.Read()) 
			{
				 QPlanFuncMat qpfc = new QPlanFuncMat();
				if (QDR.IsDBNull(0))
				{
					qpfc.iPLAN_ID = -1;
				}
				else
				{
					qpfc.iPLAN_ID = QDR.GetInt32(0);
				}

				if (QDR.IsDBNull(1))
				{
					qpfc.iFNC_ID = -1;
				}
				else
				{
					qpfc.iFNC_ID =  QDR.GetInt32(1);
				}

				if (QDR.IsDBNull(2))
				{
					qpfc.iMTL_ID = -1;
				}
				else
				{
					qpfc.iMTL_ID =  QDR.GetInt32(2);
				}
				_qwrtyList.Add(qpfc);
			}
		}

		public void setF( int pid, int fid, int mid)
		{
			 int tmpPlanID = pid;
		     int tmpFncID  = fid;
		     int tmpMtlID  = mid;
		}


		public QPlanFuncMat getFirst()
		{
			QPlanFuncMat tmpQ;
			cnt = 0;
			if (_qwrtyList.Count>0)
			{
			tmpQ = (QPlanFuncMat)_qwrtyList[cnt];
			while(tmpQ.isEQ(tmpPlanID,tmpFncID,tmpMtlID) &
						cnt < _qwrtyList.Count-1										  
					  )
				{
					tmpQ = (QPlanFuncMat)_qwrtyList[++cnt];
					
				}
			}
			else
                tmpQ = null; 
		   return tmpQ;
		}

		public QPlanFuncMat getNext()
		{
			QPlanFuncMat tmpQ;
			if (_qwrtyList.Count>0)
			{
				tmpQ = (QPlanFuncMat)_qwrtyList[cnt];
				while(tmpQ.isEQ(tmpPlanID,tmpFncID,tmpMtlID) &
					cnt < _qwrtyList.Count-1										  
					)
				{
					tmpQ = (QPlanFuncMat)_qwrtyList[++cnt];
					
				}
			}
			else
				tmpQ = null; 
			return tmpQ;
		}

		public String getArrayAsText()
		{
			string hStr = "";
			QPlanFuncMat tmpQ;
			if (_qwrtyList.Count>0)
			{
				for(int i = 0;i < _qwrtyList.Count; i++)
				{
					tmpQ = (QPlanFuncMat)_qwrtyList[i];
					if(i!= 0)
					{
						hStr  = hStr + ",[" +  tmpQ.getAsText() + "]";
					}
					else
					{
						hStr  = hStr + "[" + tmpQ.getAsText() + "]";
					}

				}
			}
			return hStr;
		}
		public PlanFuncMat()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static PlanFuncMat getSingelton(Menu.Menu M)  
		{
			PlanFuncMat PFM;
			PFM = new PlanFuncMat();
			PFM.Load(M);
			return PFM;
		}

	}
}
