using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Other
{
	/// <summary>
	/// Articles set miror from CLAPP_Article.
	/// </summary>
	public  class Articles 
	{
		private ArrayList _articles = new ArrayList();
		static string sConnStr = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
		public static Articles getSingelton()  
	    {
            Articles A;
        	A = new Articles();
			A.Load();
			return A;
	    }
		public bool Load()
		{
          bool isOk = false;

			if (sConnStr.Length > 0) 
			{
				_articles.Clear();
			}
			else
			{
				throw new Exception("SQL Connection string cannot be zero-length");
			}
			SqlConnection sqlConnection = new SqlConnection(sConnStr);
			sqlConnection.Open();

			string mySelectQuery = "SELECT ART_ID, ART_Image FROM CLAPP_Article";

			SqlCommand sqlCommand = new SqlCommand(mySelectQuery,sqlConnection);            
			SqlDataReader sqlDR = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
			setArticlesList(sqlDR); 
            sqlConnection.Close(); 
          return isOk;
		}

		private void setArticlesList( SqlDataReader  ArtDR )
		{
			while (ArtDR.Read()) 
			{
				Article a = new  Article();
				if (ArtDR.IsDBNull(0))
				{
					a.iArt_ID = -1;
				}
				else
				{
					a.iArt_ID = ArtDR.GetInt32(0);
				}

				if (ArtDR.IsDBNull(1))
				{
					a.iImage = null;
				}
				else
			    {
					a.iImage = ArtDR.GetString(1);
				}
				
				_articles.Add(a);
			}
		}
		public Articles()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
