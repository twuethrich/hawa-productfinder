using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for ReferenceSystem.
	/// </summary>
	public class ReferenceSystem
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
		
		public static DataSet getReferenceSystem(int countryID,int buildTypeID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getReferenceSystem",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];

				if(countryID > 0){
					sqlParam = sqlCommand.Parameters.Add("@CountryID",SqlDbType.Int); 
					sqlParam.Value = countryID;
				}
				if(buildTypeID > 0){
					sqlParam = sqlCommand.Parameters.Add("@BuildTypeID",SqlDbType.Int); 
					sqlParam.Value = buildTypeID;
				}

				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}			
		}
	}
}
