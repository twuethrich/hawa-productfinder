using System;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public  class TypPath
	{
		public TypPath()
		{
			
		}
		public static String getPath(long typ, int group)
		{
			string path = "";
			string download_path;

			switch (typ) 
			{
				case 0:
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemsymbol\thumbnails\" ;
			        break; 
				case 1:
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemgruppensymbol\" ;
					break; 	
				case 2:
					download_path = Model.DownloadPath.getDownloadPathFolder(group);
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" ;
					break; 
				case 3:
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Accessoiregruppensymbole\thumbnails\" ;
					break; 
				case 4:
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Accessoiregruppensymbole\thumbnails\" ;
					break; 
				case 5:
					download_path = Model.DownloadPath.getDownloadPathFolder(group);
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + @"\" ;
					break; 
				case 6:
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + @"Systemsymbol\" ;
					break; 
				case 7:
					download_path = Model.DownloadPath.getDownloadPathFolder(group);
					path = System.Configuration.ConfigurationSettings.AppSettings["ObjectPath"] + download_path + "\\thumbnails\\" ;
					break; 
				default:
					path = ""; 
					break;
			}
			return path;
	    }

		public static String getEXT(long typ, int group)
		{
			string path = "";
			string download_path;

			switch (typ) 
			{ 
				case 0: return "_Blatt_1.jpg";
				case 2: return ".dxf";
				case 6: return ".eps";
				default: return "";		   
					 
	 
			}
		}

	}
}
