using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Accessories.
	/// base class with return dataset for form accesoories
	/// </summary>
	public class Accessories
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     

		//return dataset with ACCESSORIES
		public static DataSet getAccessories()
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getAccessories",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
		//get specific Accessories selected by artID
		public static DataSet getAccessoriesSelected(int artID) {
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getAccessories",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@artID",SqlDbType.Int); 
				sqlParam.Value = artID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex) {
				throw (ex);
			}
		}

		//return dataset with ACCESSORIES pdf files
		public static DataSet getAccessoriesPDF(int AGR_ID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getAccessoriesPDF",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
			
				sqlParam = sqlCommand.Parameters.Add("@AGR_ID",SqlDbType.Int); 
				sqlParam.Value = AGR_ID;
			
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		//return dataset with ACCESSORIES files
		public static string getAccessoriesFile(int AGR_ID)
		{
			try
			{
				string FileName;
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getAccessoriesFile",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@AGR_ID",SqlDbType.Int); 
				sqlParam.Value = AGR_ID;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				if(datSet.Tables[0].Rows.Count > 0){
					FileName = datSet.Tables[0].Rows[0].ItemArray[0].ToString();
				}else{
					FileName ="";
				}
				return FileName;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
		//Get Accessories-System 
		public static DataSet getAccessoriesSystem(int AGR_ID, int planID) {
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getAccessoriesSystem",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@agrID",SqlDbType.Int); 
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = AGR_ID;
				if(planID > 0){
					sqlParam = sqlCommand.Parameters.Add("@planID",SqlDbType.Int); 
					sqlParam.Direction = ParameterDirection.Input;
					sqlParam.Value = planID;
				}
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
			
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			
			catch (Exception ex) {
				throw (ex);
			}
		}


	}
}
