using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Country.
	/// </summary>
	public class Country
	{
		
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
		
		public static DataSet getCountry()
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getCountry",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		public static DataSet getCountryBezugsquelle(long ArtID )
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getCountryBezugsquellen",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@ArtID",SqlDbType.Int);
				sqlParam.Direction = ParameterDirection.Input;
				if( ArtID > 0)
				{
				sqlParam.Value = ArtID;
				}
				else
				{
					sqlParam.Value = -1;
				}
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}


		public static DataSet getCountryReference(int buildTypeID, int artID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getCountryReference",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				if(buildTypeID > 0){
					sqlParam = sqlCommand.Parameters.Add("@BuildType",SqlDbType.Int); 
					sqlParam.Value = buildTypeID;
				}
				if(artID > 0){
					sqlParam = sqlCommand.Parameters.Add("@ArtID",SqlDbType.Int); 
					sqlParam.Value = artID;
				}

				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}



	}
}
