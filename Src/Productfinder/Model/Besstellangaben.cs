using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Besstellangaben.
	/// </summary>
	public class Besstellangaben
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     
			
		public static DataSet getBesstellangaben(int artID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getProductfinderDetail",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				if(artID > -1)
				{
					sqlParam = sqlCommand.Parameters.Add("@ProductID",SqlDbType.Int); 
					sqlParam.Value = artID;
				}
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
	}
}
