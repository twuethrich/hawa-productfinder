using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;


namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Download_path.
	/// </summary>
	public class DownloadPath
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];     

		public static string getDownloadPath(int obgID)
		{
			try
			{
				string path;
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getDownload_path",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = obgID;

				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				//datSet.Tables[0].
				if (datSet.Tables[0].Rows[0].ItemArray[3].ToString() == "True")
				{
					path = datSet.Tables[0].Rows[0].ItemArray[2].ToString() + @"\thumbnails";
				}
				else 
				{
					path = datSet.Tables[0].Rows[0].ItemArray[2].ToString();
				}
				return path;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		public static string getDownloadPathFolder(int obgID)
		{
			try
			{
				string path;
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getDownload_path",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@GroupID",SqlDbType.Int); 
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = obgID;

				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				//datSet.Tables[0].
				path = datSet.Tables[0].Rows[0].ItemArray[2].ToString();
				return path;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
	}
}
