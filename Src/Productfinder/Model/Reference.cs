using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Hawa.Src.Productfinder.Model
{
	/// <summary>
	/// Summary description for Reference.
	/// </summary>
	public class Reference
	{
		static string sConnection = System.Configuration.ConfigurationSettings.AppSettings["DSN"];   
		public string strWhere = "";
		public string strWhereDetail = "";
		public string sReferenceSort = "";
		public string sBezugsquellenSort = "";
		public string sSearchSort = "";
		public int iCountry;
		public int iBuild;
		public int iSystem;
		public string sOrt;
		public string sObject;
		public string sPlan = "";
		public string sMaterial = "";
		
		public static DataSet getReferenceSearch(string sWhere)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getReferenceSearch",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
				sqlParam = sqlCommand.Parameters.Add("@LanguageID",SqlDbType.Char);
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = HttpContext.Current.Session["LanguageID"];
				sqlParam = sqlCommand.Parameters.Add("@wherePar",SqlDbType.Char);
				sqlParam.Value = sWhere ;
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}

		public static DataSet getReferenceDetail(int RSY_ID)
		{
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getProductfinderDetail",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
			
				sqlParam = sqlCommand.Parameters.Add("@ProductID",SqlDbType.Int); 
				sqlParam.Direction = ParameterDirection.Input;
				sqlParam.Value = RSY_ID;
			
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
		
		public static DataSet getReferenceMaterial(int countryID,int buildTypeID,int artID,int planID) {
			try {
				SqlConnection sqlConnection = new SqlConnection(sConnection);
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand("HWA_getReferenceMaterial",sqlConnection);            
				sqlCommand.CommandType = CommandType.StoredProcedure;
				SqlParameter sqlParam;
			
				if(countryID > 0){
					sqlParam = sqlCommand.Parameters.Add("@CountryID",SqlDbType.Int); 
					sqlParam.Direction = ParameterDirection.Input;
					sqlParam.Value = countryID;
				}
				if(buildTypeID > 0){
					sqlParam = sqlCommand.Parameters.Add("@BuildType",SqlDbType.Int); 
					sqlParam.Direction = ParameterDirection.Input;
					sqlParam.Value = buildTypeID;
				}
				if(artID > 0){
					sqlParam = sqlCommand.Parameters.Add("@ArtID",SqlDbType.Int); 
					sqlParam.Direction = ParameterDirection.Input;
					sqlParam.Value = artID;
				}
				if(planID > 0){
					sqlParam = sqlCommand.Parameters.Add("@PlanID",SqlDbType.Int); 
					sqlParam.Direction = ParameterDirection.Input;
					sqlParam.Value = planID;
				}
			
				SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
				sqlDAdapter.SelectCommand = sqlCommand;
				DataSet datSet = new DataSet();
				sqlDAdapter.Fill(datSet);
				sqlConnection.Close();
				return datSet;
			}
			catch (Exception ex) {
				throw (ex);
			}
		}
			public static DataSet getReferencePlan(int countryID,int buildTypeID,int artID) {
				try {
					SqlConnection sqlConnection = new SqlConnection(sConnection);
					sqlConnection.Open();
					SqlCommand sqlCommand = new SqlCommand("HWA_getReferencePlan",sqlConnection);            
					sqlCommand.CommandType = CommandType.StoredProcedure;
					SqlParameter sqlParam;
			
					if(countryID > 0){
						sqlParam = sqlCommand.Parameters.Add("@CountryID",SqlDbType.Int); 
						sqlParam.Direction = ParameterDirection.Input;
						sqlParam.Value = countryID;
					}
					if(buildTypeID > 0){
						sqlParam = sqlCommand.Parameters.Add("@BuildType",SqlDbType.Int); 
						sqlParam.Direction = ParameterDirection.Input;
						sqlParam.Value = buildTypeID;
					}
					if(artID > 0){
						sqlParam = sqlCommand.Parameters.Add("@ArtID",SqlDbType.Int); 
						sqlParam.Direction = ParameterDirection.Input;
						sqlParam.Value = artID;
					}
			
					SqlDataAdapter sqlDAdapter = new SqlDataAdapter();            
					sqlDAdapter.SelectCommand = sqlCommand;
					DataSet datSet = new DataSet();
					sqlDAdapter.Fill(datSet);
					sqlConnection.Close();
					return datSet;
				}
				catch (Exception ex) {
					throw (ex);
				}
		}
	}
}
