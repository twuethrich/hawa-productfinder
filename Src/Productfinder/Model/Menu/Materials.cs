using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Materials.
	/// </summary>
	public class Materials
	{

		private ArrayList _materials = new ArrayList();

		public void Load (SqlConnection sqlConnection)
		{ 
			string mySelectQuery = "SELECT MAT_ID FROM CLAPP_Material";

			SqlDataAdapter adMAT = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet dsMAT = new DataSet();
			adMAT.Fill(dsMAT,"CLAPP_Material");

			setMaterialsList(sqlConnection, dsMAT); 
		}
		public void Clear()
		{
			_materials.Clear();
		}

		public int Count()
		{
			return _materials.Count;
		}
		public Material getItem(int i)
		{
			if (i < _materials.Count & i >= 0)
			{
				return (Material)_materials[i];			            
			}
			else
			{
				return null;
			}
		}
		private void setMaterialsList( SqlConnection sqlConnection, DataSet  MtrlDS )
		{
			foreach( DataRow row in MtrlDS.Tables["CLAPP_Material"].Rows)
			{
				Material m = new  Material();
				if (row.IsNull(0))
				{
					m.iMAT_ID = -1;
				}
				else
				{
					m.iMAT_ID = (int)row[0];
				}

				m.Load(sqlConnection);
				_materials.Add(m);
			}
		}
		
		public Materials()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
