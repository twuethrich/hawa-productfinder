using System;
using System.Data;
using System.Data.SqlClient;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for PlanLang.
	/// </summary>
	public class PlanLang
	{
		private string  _lng_id  ;
		private string  _pll_name;
		
		// Properties		
		public  string iLNG_ID 
		{
			get
			{
				return _lng_id;
			}

			set 
			{
				_lng_id = value;
			}
		}
		// Properties		
		public  string iPLL_NAME 
		{
			get
			{
				return _pll_name;
			}

			set 
			{
				_pll_name = value;
			}
		}
		public PlanLang()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
