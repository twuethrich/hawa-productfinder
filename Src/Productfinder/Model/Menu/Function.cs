using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Hawa.Src.Productfinder.Model.Menu
{
	/// <summary>
	/// Summary description for Function.
	/// </summary>
	public class Function
	{
		private    int  _fnc_id  ;
		private string  _fnc_name;
		private ArrayList _languages = new ArrayList();

		// Properties		
		public  int iFNC_ID 
		{
			get
			{
				return _fnc_id;
			}

			set 
			{
				_fnc_id = value;
			}
		}
		public  string iFNC_NAME 
		{
			get
			{
				return _fnc_name;
			}

			set 
			{
				_fnc_name = value;
			}
		}


		public void Load(SqlConnection sqlConnection)
		{

            
             
			string mySelectQuery = 
				String.Format("SELECT LNG_ID, FNL_Name FROM CLAPP_FunctionLang WHERE FNC_ID = {0}", _fnc_id);

			SqlDataAdapter adFNCL = new SqlDataAdapter(mySelectQuery,sqlConnection); 
			DataSet dsFNCL = new DataSet();
			adFNCL.Fill(dsFNCL,"CLAPP_FunctionLang");

			setFunctionsLangList(dsFNCL);

		}

		public void setFunctionsLangList(DataSet dsFNCL)
		{
			foreach( DataRow row in dsFNCL.Tables["CLAPP_FunctionLang"].Rows)
			{
				FuncLang FL = new FuncLang();
				if (!row.IsNull(0) &
					!row.IsNull(1) 
					)
				{
					FL.iLNG_ID = (string)row[0];
					FL.iFNL_NAME = (string)row[1];
					_languages.Add(FL);
				}

			}
 		 
		}

		public string getFNL_Name(string LNGID)
		{
			for(int i=0;i<_languages.Count;i++)
			{
				if (((FuncLang)_languages[i]).iLNG_ID.ToUpper() == LNGID.ToUpper())
				{
					return ((FuncLang)_languages[i]).iFNL_NAME; 
				}
			}
			return  "";
		}
		
		public Function()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
