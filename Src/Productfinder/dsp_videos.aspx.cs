using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class Video : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblShort;
		int agrID = 40;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				int accessoire = -1;
				int artID = -1;
				if(Request.Params.Get("artID") != null)
				{
					artID = Convert.ToInt32(Request.Params.Get("artID"));
				}
			
                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }

				btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				
				dgrPlanung.DataSource = Model.ArticleAndGroup.getPlanung(artID,agrID, -1,accessoire);
				
				dgrPlanung.DataBind(); 

                
                
           
	


			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try
				{
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch
				{
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//add links for every rows to download planung and montage
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				string name;
				double size;
                string filename;
                string productname;
				
				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

                productname = ((TableCell)(e.Item.Controls[6])).Text.ToString(); 

				name = ((TableCell)(e.Item.Controls[7])).Text.ToString() + "-" + ((TableCell)(e.Item.Controls[6])).Text.ToString();
				size = Convert.ToInt32(((TableCell)(e.Item.Controls[5])).Text) /1024;
                filename = ((TableCell)(e.Item.Controls[2])).Text;
				int group = Convert.ToInt32(((TableCell)(e.Item.Controls[8])).Text);
				agrID= group;

				//e.Item.Cells[0].Text = "&#160;<img src=\"images/pdf_file.gif\">&#160;&#160;&#160;<a href=\"#\" onclick=\"javascript:window.open('dsp_objectdownload.aspx?typ=2&group="+agrID+"&id=" + ((TableCell)(e.Item.Controls[0])).Text.ToString() + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\" class=\"PRODUCT\">" + name + "</a>";
				

				e.Item.Cells[0].Text = "<img src=\"images/pdf_file.gif\">";

				if(size > 2000)
					e.Item.Cells[0].Text = "<img src=\"images/zip_file.gif\">";

                e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('Planung-/Montageanleitungen','" + productname + "','" + filename + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + ((TableCell)(e.Item.Controls[2])).Text.ToString() + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name + "</a>";
				e.Item.Cells[5].Text =  size + "&nbsp;KB &nbsp;";
				

			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
