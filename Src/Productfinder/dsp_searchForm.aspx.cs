using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model.Menu;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Zusammenfassung f�r dsp_searchForm.
	/// </summary>
	///

	public partial class dsp_searchForm : Alpha.Page
	{
		public static DataTable tabPlan;
		public static DataTable tabFunction;
		protected Alpha.Controls.String Seite;

		public static DataTable tabMaterial;

		public static DataTable tabList;
		public int sPlan = -1;
		public int sFunction = -1;
		public int sMaterial = -1;
		public int seaPlan = 0;
		public int seaFunction = 0;
		public int seaMaterial = 0;
        public string query;
		public int iPlan;
		public int iFunc;
		public int iMat;
		public string sSetFocus;
		public string sOrder;
		protected System.Web.UI.WebControls.TextBox hidPlan;
		protected System.Web.UI.WebControls.TextBox hidFunction;
		protected System.Web.UI.WebControls.TextBox hidMaterial;
		protected System.Web.UI.WebControls.ImageButton btnAcc;
        

		public string entry;

		//fill tables with data
		protected void Page_Load(object sender, System.EventArgs e)
		{            		     
			try{
				dgrProductFinder.Columns[1].HeaderText = getString("product",true);
				dgrProductFinder.Columns[2].HeaderText = getString("maxWeight",true);
				dgrProductFinder.Columns[3].HeaderText = getString("material",true);
				dgrProductFinder.Columns[4].HeaderText = getString("merkmal",true);
				
				btnEn.NavigateUrl = Request.Path + "?languageID=En";
				btnDe.NavigateUrl = Request.Path + "?languageID=De";
				btnFr.NavigateUrl = Request.Path + "?languageID=Fr";
				btnEs.NavigateUrl = Request.Path + "?languageID=Es";

				btnSearchProdukt.ImageUrl= "images/btn_search2_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";

                Reference R = (Reference)(HttpContext.Current.Session["reference"]);
                if (!Page.IsPostBack) {                                        
                    R.sSearchSort = "SortWeight";               
                    notFound.Visible=false;
                }

                if ((R.sSearchSort.Length>0) && (R.sSearchSort.Substring(R.sSearchSort.Length -4) == "DESC")){
                    sOrder = "6";                
                }else{
                    sOrder = "5";
                }
               				
				seaPlan = Convert.ToInt32(Request["plan"]);
				seaFunction = Convert.ToInt32(Request["function"]);
				seaMaterial = Convert.ToInt32(Request["material"]);

                if (Request.Params.Get("search") != null){
                    txtSearch.Text = Request.Params.Get("search").ToString();
                    BindGridList();

                }



				BindGridSearchNew();

                //if(Page.IsPostBack){
                    string overviewURL = System.Configuration.ConfigurationSettings.AppSettings["overviewURL"] + HttpContext.Current.Session["languageID"].ToString() + "/OverviewApplication.html";

                    if (txtSearch.Text.Contains("junior") || txtSearch.Text.Contains("HAWA-Junior"))
                    {
                        juniorPreview.InnerHtml = "<a href='" + overviewURL + "' class='junior' title='HAWA-Junior Sortiment'><img src='images/overview_tool.jpg' border='0'  alt='HAWA-Junior Sortiment'></a>";
                        //juniorPreview.Visible = true;
                        juniorPreview.Style.Add("Visibility", "visible");
                  
                    }else{
                        juniorPreview.InnerHtml = "";
                        //juniorPreview.Visible = false;

                    }
                //}
				// Suche ausf�hren
				if (entry == null)  
				{

              

					tabList =  Model.ProductSearch.getSearchList(sMaterial,sPlan,sFunction).Tables[0];
					for (int m=0;m<tabList.Rows.Count;m++) 
					{
						StringBuilder strBuilder = 
							new StringBuilder((tabList.Rows[m]["Kurz"]).ToString());

						// der folgende Befehl tauscht alle Strings
						
						strBuilder.Replace("\n", " ");
						strBuilder.Replace("\r", " ");
						strBuilder.Replace("'", " ");
			
						// Ausgabe des ver�nderten Strings
						//tabList.Rows[m]["Kurz"] = strBuilder.ToString().Replace("\r\n"," ").Replace("\"","\'");
						tabList.Rows[m]["Kurz"] = strBuilder.ToString();
					}
				}
				entry = null;
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}
		}

		//create checkboxes for plan, system and material	
		public void BindGridSearchNew()
		{
			try
			{
				System.Web.UI.WebControls.Image imgSpace = new System.Web.UI.WebControls.Image();
				imgSpace.ImageUrl = "images/ts.gif";
				//set image for check OFF
				string imgSrc = "images/checkbox_off.gif";
				//create checkboxes for plan
				plPlan.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 width=100%>"));
				for(int i=0; i < Global.M.iPLANES.Count()-1;i++)
				{
					System.Web.UI.WebControls.Image imgPlan = new System.Web.UI.WebControls.Image();
					imgPlan.ImageUrl = imgSrc;					
					imgPlan.Width = 12;
					imgPlan.Height = 12;
					Label lblPlan = new Label(); 
					Plan p = (Plan)Global.M.iPLANES.getItem(i);
					lblPlan.ID = "lblPla_"+ p.iPLAN_ID;
					imgPlan.ID = "imgPla_"+ p.iPLAN_ID;
					imgPlan.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					imgPlan.Attributes.Add("onclick", "onPlan('" + p.iPLAN_ID + "')");
					lblPlan.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					lblPlan.Attributes.Add("onclick", "onPlan('" + p.iPLAN_ID + "')");
					lblPlan.Text = p.getPLL_Name((String)HttpContext.Current.Session["LanguageID"]);
					lblPlan.EnableViewState=true;
					//if (i == 0)
					//{
					//	plPlan.Controls.Add(new LiteralControl("<tr><td width=100% colspan=2 height=4></td></tr>"));
					//}
					
					plPlan.Controls.Add(new LiteralControl("<tr><td width=32 height=41 align='center'>"));
					plPlan.Controls.Add(imgPlan);
					plPlan.Controls.Add(new LiteralControl("</td><td width=158>"));
					plPlan.Controls.Add(lblPlan);
					plPlan.Controls.Add(new LiteralControl("</td></tr>"));
					if(i != Global.M.iPLANES.Count()-1)
					{
						plPlan.Controls.Add(new LiteralControl("<tr><td bgcolor='white' width=100% colspan=2><img src='images/ts.gif' height=1></td></tr>"));
					}
					//else
					//{
					//	plPlan.Controls.Add(new LiteralControl("<tr><td width=100% colspan=2 height=5></td></tr>"));
					//}
				}

				plPlan.Controls.Add(new LiteralControl("</table>"));
				
				plFunction.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 width=100%>"));
				for( int i=0; i < Global.M.iFUNCTIONS.Count();i++)
				{
					System.Web.UI.WebControls.Image imgFunction = new System.Web.UI.WebControls.Image();
					imgFunction.ImageUrl = imgSrc;
					imgFunction.Width = 12;
					imgFunction.Height = 12;					
					Label lblFunction = new Label(); 
					Function f = (Function)Global.M.iFUNCTIONS.getItem(i);
					lblFunction.ID = "lblFun_"+ f.iFNC_ID;
					imgFunction.ID = "imgFun_"+ f.iFNC_ID;
					imgFunction.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					imgFunction.Attributes.Add("onclick", "onFunction('" + f.iFNC_ID + "')");
					lblFunction.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					lblFunction.Attributes.Add("onclick", "onFunction('" + f.iFNC_ID + "')");
					System.Web.UI.WebControls.Image imgFunc = new System.Web.UI.WebControls.Image();
					imgFunc.ImageUrl = "images/pic_function"+ f.iFNC_ID +".gif";
					lblFunction.Text = f.getFNL_Name((String)HttpContext.Current.Session["LanguageID"]);
					lblFunction.EnableViewState=true;
					//if (i == 0)
					//{
					//	plFunction.Controls.Add(new LiteralControl("<tr><td width=100% colspan=4 height=4></td></tr>"));														
					//}
					
					plFunction.Controls.Add(new LiteralControl("<tr><td width=32 height=49 align='center'>"));
					plFunction.Controls.Add(imgFunction);
					plFunction.Controls.Add(new LiteralControl("</td><td width=228>"));
					plFunction.Controls.Add(lblFunction);
					plFunction.Controls.Add(new LiteralControl("</td><td width=220 align=left valign=middle>"));
					plFunction.Controls.Add(imgFunc);
					plFunction.Controls.Add(new LiteralControl("</td><td width=10><img src='images/ts.gif' width=10></td></tr>"));
					if(i != Global.M.iFUNCTIONS.Count()-1)
					{
						plFunction.Controls.Add(new LiteralControl("<tr><td width=100% bgcolor=white colspan=4><img src='images/ts.gif' height=1></td></tr>"));														
					}
					//else
					//{
					//	plFunction.Controls.Add(new LiteralControl("<tr><td width=100% colspan=4 height=5></td></tr>"));														
					//}
				}
				plFunction.Controls.Add(new LiteralControl("</table>"));
				
				plMaterial.Controls.Add(new LiteralControl("<table border=0 cellpadding=0 cellspacing=0 width=190>"));
				for( int i=0; i < Global.M.iMaterials.Count();i++)
				{
					System.Web.UI.WebControls.Image imgMaterial = new System.Web.UI.WebControls.Image();
					imgMaterial.ImageUrl = imgSrc;
					imgMaterial.Width = 12;
					imgMaterial.Height = 12;
					Label lblMaterial = new Label(); 
					Material m = Global.M.iMaterials.getItem(i);
					lblMaterial.ID = "lblMat_"+ m.iMAT_ID;
					imgMaterial.ID = "imgMat_"+ m.iMAT_ID;
					lblMaterial.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					lblMaterial.Attributes.Add("onclick", "onMaterial('" + m.iMAT_ID + "')");
					imgMaterial.Attributes.Add("onMouseOver", "javascript: style.cursor='pointer'");
					imgMaterial.Attributes.Add("onclick", "onMaterial('" + m.iMAT_ID + "')");
					System.Web.UI.WebControls.Image imgMat = new System.Web.UI.WebControls.Image();
					imgMat.ImageUrl = "images/pic_material"+  m.iMAT_ID +".gif";
					imgMat.Width = 20;
					imgMat.Height = 20;
					lblMaterial.Text = m.getMTL_Name((String)HttpContext.Current.Session["LanguageID"]);
					lblMaterial.EnableViewState=true;
					//if (i == 0)
					//{
					//	plMaterial.Controls.Add(new LiteralControl("<tr><td width=100% colspan=5 height=4></td></tr>"));
					//}
					//plMaterial.Controls.Add(new LiteralControl("<tr><td>&#160;&#160;&#160;"));
					
					plMaterial.Controls.Add(new LiteralControl("</td><td width=32 height=32 align=middle>"));
					plMaterial.Controls.Add(imgMaterial);
					plMaterial.Controls.Add(new LiteralControl("</td><td width=114>"));
					plMaterial.Controls.Add(lblMaterial);
					plMaterial.Controls.Add(new LiteralControl("<td width=44>"));
					plMaterial.Controls.Add(imgMat);
					//imgSpace.Width = 20;
					//plMaterial.Controls.Add(new LiteralControl("</td><td>"));
					//plMaterial.Controls.Add(imgSpace);
					plMaterial.Controls.Add(new LiteralControl("</td></tr>"));
					if(i != Global.M.iMaterials.Count())
					{
						plMaterial.Controls.Add(new LiteralControl("<tr><td width=100% colspan=3 bgcolor=white><img src='images/ts.gif' height=1></td></tr>"));
					}
					//else
					//{
					//	plMaterial.Controls.Add(new LiteralControl("<tr><td width=100% colspan=5 height=5></td></tr>"));
					//}
				}
				//plMaterial.Controls.Add(new LiteralControl("<tr><td height=5></td></tr></table>"));		
				plMaterial.Controls.Add(new LiteralControl("</table>"));		

				iPlan = Global.M.iPLANES.Count();
				iFunc = Global.M.iFUNCTIONS.Count();
				iMat = Global.M.iMaterials.Count();
				query =Global.PFM.getArrayAsText();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
				throw ex;
			}
		}
		
		//SuchListe aufbereiten
		public void BindGridList()
		{
			try
			{
				DataTable tmpDS; 
				if(sMaterial < 0 && sPlan < 0 && sFunction < 0 && txtSearch.Text.Trim() != "")
				{
					string searchText = txtSearch.Text.ToString();
					searchText = searchText.Replace(" ","").ToString();
					tmpDS=Model.ProductSearch.getTextSearchList(txtSearch.Text).Tables[0];
				}
				else
				{
					tmpDS=Model.ProductSearch.getSearchList(sMaterial,sPlan,sFunction).Tables[0];
				}
				dgrProductFinder.DataSource=tmpDS;
				if (tmpDS.Rows.Count == 0){
					notFound.Visible=true;
				}else{
					notFound.Visible=false;
				}

                Reference R = (Reference)(HttpContext.Current.Session["reference"]);
                if ((R.sSearchSort.Length>0) && (R.sSearchSort.Substring(R.sSearchSort.Length -4) == "DESC"))
                    sOrder = "5";                
                else
                    sOrder = "6";                
				dgrProductFinder.Visible = true;
				// LBU 15032005 wegen curentpage > PagesCount so bearbeiten	
				try
				{ 
					dgrProductFinder.DataBind();	                    
                    DataView vSort = new DataView(tmpDS);                    
                    vSort.Sort = R.sSearchSort;                                      
                    dgrProductFinder.DataSource = vSort;
                    dgrProductFinder.DataBind();      
				}
				catch
				{
					if	(dgrProductFinder.PageCount< dgrProductFinder.CurrentPageIndex+1)
					{
						dgrProductFinder.CurrentPageIndex = dgrProductFinder.PageCount-1;
						dgrProductFinder.DataBind();	
                        DataView vSort = new DataView(tmpDS);                       
                        vSort.Sort = R.sSearchSort;                                      
                        dgrProductFinder.DataSource = vSort;
                        dgrProductFinder.DataBind();    
					}
				}
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				//Response.Redirect("dsp_error_page.aspx");
			}
		}
		
	
		public void onProductFinderSort(object source, DataGridSortCommandEventArgs e)
		{
			try
			{
//				string currentColumn = SortColumn;
//				SortColumn = e.SortExpression;
//				if(currentColumn == SortColumn)
//					SortAscend = !SortAscend;
//				else
//					SortAscend = true;
				sPlan = seaPlan;
				sFunction = seaFunction;
				sMaterial = seaMaterial;
				DataSet dsSort = new DataSet ();
				if(sMaterial < 0 && sPlan < 0 && sFunction < 0 && txtSearch.Text != "")
				{
					dsSort = Model.ProductSearch.getTextSearchList(txtSearch.Text);
				}
				else
				{
					dsSort = Model.ProductSearch.getSearchList(sMaterial,sPlan,sFunction);
				}
		
				DataView vSort = new DataView(dsSort.Tables[0]);
				Reference R = (Reference)(HttpContext.Current.Session["reference"]);
		/*		if (R.sSearchSort == e.SortExpression.ToString())
				{
					vSort.Sort = e.SortExpression.ToString() + " DESC";
					sOrder = "5";
				}
				else
				{
					vSort.Sort = e.SortExpression.ToString();
					sOrder = "6";
				}
        */      if(sOrder == "5")
                    vSort.Sort = e.SortExpression.ToString() + " DESC";
                else
                    vSort.Sort = e.SortExpression.ToString();
				R.sSearchSort = vSort.Sort;
				dgrProductFinder.DataSource = vSort;
				dgrProductFinder.DataBind();	
				
				dgrProductFinder.DataSource = vSort;
				//dgrProductFinder.CurrentPageIndex = e.NewPageIndex ;
				dgrProductFinder.DataBind();
				dgrProductFinder.Visible = true;

				//BindGridList();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		public void pager(object source, DataGridPageChangedEventArgs e)
		{
           
		}
		
		//add links for show detail of selected produkt
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try
			{
				int holz;
				int glas;
				int metall;
				int artID = -1;
				string product;

				ListItemType itemType = (ListItemType)e.Item.ItemType;

				if(itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
				{
					Reference R = (Reference)(HttpContext.Current.Session["reference"]);
					/*
                    for (int i=0; i < dgrProductFinder.Columns.Count ; i++)
					{
						if (R.sSearchSort == dgrProductFinder.Columns[i].SortExpression || R.sSearchSort == dgrProductFinder.Columns[i].SortExpression + " DESC")
						{
							Label lblSorted = new Label();
							lblSorted.Font.Name = "webdings";
							lblSorted.Font.Size = FontUnit.XSmall;
							lblSorted.Text = sOrder;
							e.Item.Cells[i].Controls.Add(lblSorted);
						}
					}*/	
					return;
				}

				product = ((TableCell)(e.Item.Controls[e.Item.Controls.Count-4])).Text.ToString();

				try
				{
					holz = Convert.ToInt32(((TableCell)(e.Item.Controls[e.Item.Controls.Count-3])).Text.ToString());
					glas = Convert.ToInt32(((TableCell)(e.Item.Controls[e.Item.Controls.Count-2])).Text.ToString());
					metall = Convert.ToInt32(((TableCell)(e.Item.Controls[e.Item.Controls.Count-1])).Text.ToString());
					artID = Convert.ToInt32(((TableCell)(e.Item.Controls[0])).Text.ToString());
				}
				catch(FormatException)
				{
					holz = 0; glas = 0; metall = 0;
				}
				if(holz > 0)
					((TableCell)(e.Item.Controls[4])).FindControl("holz").Visible = true;
            
				if(glas > 0) 
					((TableCell)(e.Item.Controls[4])).FindControl("glas").Visible = true;
                
				if(metall > 0)
					((TableCell)(e.Item.Controls[4])).FindControl("metall").Visible = true;

				// Link
				((HyperLink)(((TableCell)(e.Item.Controls[0])).FindControl("product"))).NavigateUrl = "dsp_detail.aspx?artID=" + artID;
				((HyperLink)(((TableCell)(e.Item.Controls[0])).FindControl("product"))).Text = product;
				((HyperLink)(((TableCell)(e.Item.Controls[0])).FindControl("product"))).Attributes.Add ("onMouseOver", "javascript:style.cursor='pointer';changeGif('visible',"+artID+");");
				((HyperLink)(((TableCell)(e.Item.Controls[0])).FindControl("product"))).Attributes.Add ("onMouseOut", "changeGif('hidden',0);");

				// Info-Button
				((Label)(((TableCell)(e.Item.Controls[2])).FindControl("info"))).Text = "<IMG SRC=\"images/icon_merkmal.gif\" WIDTH=\"15\" HEIGHT=\"15\" BORDER=\"0\" ONMOUSEOVER=\"javascript:style.cursor='pointer';changeGif('visible',"+artID+");\" ONMOUSEOUT=\"changeGif('hidden',0);\">";
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//Customize Paging display
		public void OnItemCreated(object sender, DataGridItemEventArgs e) {
			if(e.Item.ItemType==ListItemType.Pager) {
				Label lblPager = new Label();
				
				
				lblPager.Text = "<b>" + getString("page",false)+"</b>" + lblPager.Text;
				e.Item.Controls[0].Controls.AddAt(0,lblPager);
			}
		}
	

		#region Vom Web Form-Designer generierter Code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: Dieser Aufruf ist f�r den ASP.NET Web Form-Designer erforderlich.
			//
			InitializeComponent();
			base.OnInit(e);
        	
		}
		
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung. 
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		private void InitializeComponent()
		{   
			this.dgrProductFinder.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.OnItemCreated);
			this.dgrProductFinder.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgrProductFinder_PageIndexChanged);
			this.dgrProductFinder.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.onProductFinderSort);

		}
		#endregion

		//reset button - reset all data
		protected void btnReset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				//ResetForm();
                juniorPreview.InnerHtml = "";
				txtSearch.Text = "";
				BindGridList();
				dgrProductFinder.Visible = false;
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}

		}
		
		//search button
		protected void btnSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
                
                //ResetForm();
				BindGridList();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		
		}
		//search product
		protected void btnSearchProdukt_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			try
			{
				//ResetForm();
				sPlan =  int.Parse(Request["plan"]);
				sFunction = int.Parse(Request["function"]);
				sMaterial = int.Parse(Request["material"]);

				DataSet selMaterial = Model.ProductSearch.getAvailableMaterial(sPlan,sFunction);
				DataSet selFunction = Model.ProductSearch.getAvailableFunction(sPlan,sMaterial);
				DataSet selPlan = Model.ProductSearch.getAvailablePlan(sFunction,sMaterial);

					BindGridList();
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		protected void txtSearch_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				//ResetForm();
				BindGridList();
				sSetFocus = "document.getElementById(\"txtSearch\").focus()";
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		private void dgrProductFinder_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			sPlan = seaPlan;
			sFunction = seaFunction;
			sMaterial = seaMaterial;
			DataSet dsSort = new DataSet ();
			if(sMaterial < 0 && sPlan < 0 && sFunction < 0 && txtSearch.Text != "")
			{
				dsSort = Model.ProductSearch.getTextSearchList(txtSearch.Text);
			}
			else
			{
				dsSort = Model.ProductSearch.getSearchList(sMaterial,sPlan,sFunction);
			}
		
			DataView vSort = new DataView(dsSort.Tables[0]);
			Reference R = (Reference)(HttpContext.Current.Session["reference"]);
			vSort.Sort = R.sSearchSort;
			if (vSort.Sort.Length > 4)
			{
				if (vSort.Sort.Substring(vSort.Sort.Length -4) == "DESC")
				{
					sOrder = "5";
				}
				else
				{
					sOrder = "6";
				}
			}
			dgrProductFinder.DataSource = vSort;
			dgrProductFinder.CurrentPageIndex = e.NewPageIndex ;
			dgrProductFinder.DataBind();
		}


		




	}
 

}


