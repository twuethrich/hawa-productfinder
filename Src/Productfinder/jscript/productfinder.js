﻿i0 = new Image;
i0.src='';
kurz0 ='';
flash0='';

row1 = new Image;
row1.src='images/checkbox_off.gif';
row2 = new Image;
row2.src='images/checkbox_on.gif';

var strQuery = new Array(<%= query %>);
var ColorGrey = "#909090";
var ColorBlack = "#000000";
sPlan = <%= sPlan %>;
sFunction = <%= sFunction %>;
sMaterial = <%= sMaterial %>;
intPlan = <%= iPlan %>;
intFunc = <%= iFunc %>;
intMat = <%= iMat %>;

function onLoadPage(){
	document.getElementById("plan").value = sPlan;
	document.getElementById("function").value = sFunction;
	document.getElementById("material").value = sMaterial;
	
	if (sPlan != -1){
		document.getElementById("imgPla_" + sPlan).src = row2.src;
		document.getElementById("plan").value = sPlan;
		onPlan(sPlan);
	}
	if (sFunction != -1){
		document.getElementById("imgFun_" + sFunction).src = row2.src;
		document.getElementById("function").value = sFunction;
		onFunction(sFunction);
	}
	if (sMaterial != -1){
		document.getElementById("imgMat_" + sMaterial).src = row2.src;
		document.getElementById("material").value = sMaterial;
		//if (sFunction == -1 && sPlan == -1)
		onMaterial(sMaterial);
	}
	<%= sSetFocus %>
}

function onPlan(row_ID){
	
	if (document.getElementById("lblPla_" + row_ID).style.color != ColorGrey){
		disablePlan();
		disableFunctionColor();
		disableMaterialColor();
		disableFunction();
		disableMaterial();
		document.getElementById("imgPla_" + row_ID).src = row2.src;
		document.getElementById("plan").value = row_ID;
		
		selPlan = row_ID;
		
        for (i=0; i<strQuery.length; i++){
            if (strQuery[i][0] == row_ID){
            
                //document.getElementById("imgFun_" + strQuery[i][1]).src = row1.src;                    
                //document.getElementById("imgMat_" + strQuery[i][2]).src = row1.src;
              
                //Kein Funktion und kein Material ausgewählt
                if (document.getElementById("function").value == -1 && document.getElementById("material").value == -1){
                    document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                }
                
                //Funktion ausgewählt und kein Material
                else if (document.getElementById("function").value != -1 && document.getElementById("material").value == -1){
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    }
                    if (strQuery[i][1] == document.getElementById("function").value){
                       document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                       document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src; 
                    }
                }
                
                // Keine Funktion aber Material ausgewählt 
                else if (document.getElementById("function").value == -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                    }
                     if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
                    }
                }
                
                // Funktion und Material auswählt                                                         
                else if (document.getElementById("function").value != -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][0] == document.getElementById("plan").value && strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        if (strQuery[i][1] == document.getElementById("function").value){
							document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
						}
                        
                    }
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                        if (strQuery[i][2] == document.getElementById("material").value){
							document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
                        }
                       
                    }
                }
			}
		}
	}
}

function onFunction(row_ID){
	
    if (document.getElementById("lblFun_" + row_ID).style.color != ColorGrey){
        disableFunction();
        
        disablePlanColor();
        disableMaterialColor();
		disablePlan();
		disableMaterial();
        document.getElementById("imgFun_" + row_ID).src = row2.src;
        selFunction = row_ID;
        document.getElementById("function").value = row_ID;
        for (i=0; i<strQuery.length; i++){
            //alert("Array:" + strQuery[i] + "\n row_ID:" + row_ID + " strQuery[i][1]:" + strQuery[i][1]);
            if (strQuery[i][1] == row_ID){ 
                
             
                if (document.getElementById("plan").value == -1 && document.getElementById("material").value == -1){
                //alert("kein vorhaben und kein material");
                    document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                   
                }
                else if (document.getElementById("plan").value != -1 && document.getElementById("material").value == -1){
                    if (strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    }
                    if (strQuery[i][0] == document.getElementById("plan").value){
						//alert("nur Vorhaben " + document.getElementById("plan").value);
                        document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
                        document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                    }
                   
                }
                else if (document.getElementById("plan").value == -1 && document.getElementById("material").value != -1){
					if (strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
					}
					if (strQuery[i][2] == document.getElementById("material").value){
						//alert("nur Material " + document.getElementById("plan").value);
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
						document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
					}
					
                }                                                         
                else if (document.getElementById("plan").value != -1 && document.getElementById("material").value != -1){
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                        if(strQuery[i][0] == document.getElementById("plan").value){
						
							document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                        }
                        
                    }
                    if (strQuery[i][1] == document.getElementById("function").value && strQuery[i][0] == document.getElementById("plan").value){
						document.getElementById("lblMat_" + strQuery[i][2]).style.color = ColorBlack;
						if (strQuery[i][2] == document.getElementById("material").value){
							document.getElementById("imgMat_" + strQuery[i][2]).src = row2.src;
						}
                    }
                }
            }
        }
    }
}
 
function onMaterial(row_ID){
		
	   if (document.getElementById("lblMat_" + row_ID).style.color != ColorGrey){
        /*if (document.getElementById("plan").value != -1 && document.getElementById("function").value != -1){
        }
        else
        {
			//alert('disableMat');
			disablePlanColor();
			disableFunctionColor();
        }*/
		disableMaterial();
		disablePlanColor();
		disableFunctionColor();
		disablePlan();
		disableFunction();
		document.getElementById("imgMat_" + row_ID).src = row2.src;
        selMaterial = row_ID;
        document.getElementById("material").value = row_ID;
        
     
        
        
        for (i=0; i<strQuery.length; i++){
            if (strQuery[i][2] == row_ID){ 
                
                
                
                if (document.getElementById("plan").value == -1 && document.getElementById("function").value == -1){
                    document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                }
                else if (document.getElementById("plan").value != -1 && document.getElementById("function").value == -1){
                    if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                    }
                    if (strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                    }
                }
                else if (document.getElementById("plan").value == -1 && document.getElementById("function").value != -1){
                    if (strQuery[i][1] == document.getElementById("function").value){
                        document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
                        document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
                        
                    }
                    if (strQuery[i][2] == document.getElementById("material").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                    }
                }                                                         
                
               
                else if (document.getElementById("plan").value != -1 && document.getElementById("function").value != -1){
                 
                    if (strQuery[i][2] == document.getElementById("material").value && strQuery[i][0] == document.getElementById("plan").value){
                        document.getElementById("lblFun_" + strQuery[i][1]).style.color = ColorBlack;
                        if(strQuery[i][0] == document.getElementById("plan").value){
							document.getElementById("imgPla_" + strQuery[i][0]).src = row2.src;
                        }
                        
                    }
                    if (strQuery[i][2] == document.getElementById("material").value && strQuery[i][1] == document.getElementById("function").value){
						document.getElementById("lblPla_" + strQuery[i][0]).style.color = ColorBlack;
						if (strQuery[i][1] == document.getElementById("function").value){
							document.getElementById("imgFun_" + strQuery[i][1]).src = row2.src;
						}
                    }
                }
                
                
                
                
                
                
                
                
             }
        }  
        
       
        
    }
}

function enablePlan(){
	for (i=1; i<intPlan+1; i++){
		//document.getElementById("imgPla_" + i).src = row1.src;
		document.getElementById("lblPla_" + i).style.color = ColorBlack;
	}
}

function enableFunction(){
	for (i=1; i<intFunc+1; i++){
		//document.getElementById("imgFun_" + i).src = row1.src;
		document.getElementById("lblFun_" + i).style.color = ColorBlack;
	}
}

function enableMaterial(){
	for (i=1; i<intMat+1; i++){
		//document.getElementById("imgMat_" + i).src = row1.src;
		document.getElementById("lblMat_" + i).style.color = ColorBlack;
	}
}

function disablePlanColor(){
	for (i=1; i<intPlan; i++){
		document.getElementById("lblPla_" + i).style.color = ColorGrey;
	}
}

function disableFunctionColor(){
	for (i=1; i<intFunc+1; i++){
		document.getElementById("lblFun_" + i).style.color = ColorGrey;
	}
}

function disableMaterialColor(){
	for (i=1; i<intMat+1; i++){
		document.getElementById("lblMat_" + i).style.color = ColorGrey;
		
	}
}

function disablePlan(){
	for (i=1; i<intPlan; i++){
		document.getElementById("imgPla_" + i).src = row1.src;
	}
}

function disableFunction(){
	for (i=1; i<intFunc+1; i++){
		document.getElementById("imgFun_" + i).src = row1.src;
	}
}

function disableMaterial(){
	for (i=1; i<intMat+1; i++){
		document.getElementById("imgMat_" + i).src = row1.src;
	}
}
<% 
int tabCount = 0;
if (tabList != null) tabCount = tabList.Rows.Count;
for(int i=0;i<tabCount;i++) { 
%>
 //i<%= tabList.Rows[i]["Art_ID"] %> = new Image;
 i<%= tabList.Rows[i]["Art_ID"] %>='dsp_objectdownload.aspx?id=<%= tabList.Rows[i]["Systemsymbol"]%>&typ=0';
 rr<%= tabList.Rows[i]["Art_ID"] %> ='<%= tabList.Rows[i]["Kurz"]%>';
 flash<%= tabList.Rows[i]["Art_ID"] %> = 'dsp_objectdownload.aspx?id=<%= tabList.Rows[i]["Groupsymbol"]%>&typ=11';
<% } %>

function changeGif(show,file){
	str = '<img src="'+ eval("i" + file)+'" >';
	document.all["bild"].innerHTML = str;
	if (file>0){
		str = eval("rr" + file);
		//document.all["merkmal"].firstChild.nodeValue  = str;
		document.all["merkmal"].innerHTML  = str;
	}else{
		document.all["merkmal"].firstChild.nodeValue  = "";
	}
	if (file>0){
	    str = '<img src="'+ eval("flash" + file)+'">';
	 	document.all["flash"].innerHTML = str;				
	}
    if(show == "hidden")
        juniorPreview.style.visibility = "visible";
    else
        juniorPreview.style.visibility = "hidden";

    merkmal.style.visibility = show;
    detail.style.visibility = show;
    flash.style.visibility = show;
	bild.style.visibility = show;
}

function changeFlash(show,file){
	document.getElementById("symbol").src= eval("i" + file + ".src");
    document.getElementById("merkmal").firstChild.nodeValue = eval("kurz" + file);
    str = '<OBJECT id="obj" codeBase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0"';
    str += 'height="145" width="150" border="1" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" VIEWASTEXT>';
    str += '<PARAM NAME="_cx" VALUE="3969"> <PARAM NAME="_cy" VALUE="3836"> <PARAM NAME="FlashVars" VALUE="">';
	str += '<PARAM NAME="Movie" VALUE="'+eval("flash" + file)+'">';
	str += '<PARAM NAME="Src" VALUE="'+eval("flash" + file)+'">';
	//str += '<PARAM NAME="WMode" VALUE="Window">';
	//str += '<PARAM NAME="Play" VALUE="0">';
	//str += '<PARAM NAME="Loop" VALUE="-1">';
	str += '<PARAM NAME="Quality" VALUE="High">';
	//str += '<PARAM NAME="SAlign" VALUE="">';
	//str += '<PARAM NAME="Menu" VALUE="-1">';
	//str += '<PARAM NAME="Base" VALUE="">';
	//str += '<PARAM NAME="AllowScriptAccess" VALUE="always">';
	//str += '<PARAM NAME="Scale" VALUE="ShowAll">';
	//str += '<PARAM NAME="DeviceFont" VALUE="0">';
	//str += '<PARAM NAME="EmbedMovie" VALUE="0">';
	//str += '<PARAM NAME="BGColor" VALUE="FFFFFF">';
	//str += '<PARAM NAME="SWRemote" VALUE="">';
	//str += '<PARAM NAME="MovieData" VALUE="">';
	//str += '<PARAM NAME="SeamlessTabbing" VALUE="1">';
	str += '<EMBED SRC="'+eval("flash" + file)+'" QUALITY="high" BGCOLOR="#FFFFFF" WIDTH="150" HEIGHT="145" NAME="81382"';
	str += 'ALIGN="" BORDER="0" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer">';
	str += '</EMBED>';
	str += '</OBJECT>';
    document.all["flash"].innerHTML = str;
    merkmal.style.visibility = show;
    detail.style.visibility = show;
    flash.style.visibility = show;
	bild.style.visibility = show;
}
