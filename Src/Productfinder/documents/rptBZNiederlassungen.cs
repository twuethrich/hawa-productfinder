using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;
using System.Data;

namespace Hawa
{
	/// <summary>
	/// Summary description for rptBZNiederlassungen.
	/// </summary>
	public class rptBZNiederlassungen : DataDynamics.ActiveReports.ActiveReport3
	{
		private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.Line Line1;
        private DataDynamics.ActiveReports.TextBox Firma;
        private DataDynamics.ActiveReports.TextBox Strasse;
        private DataDynamics.ActiveReports.TextBox Ort;
        private DataDynamics.ActiveReports.TextBox Land;
        private DataDynamics.ActiveReports.TextBox Telefon;
        private DataDynamics.ActiveReports.TextBox Fax;
        private DataDynamics.ActiveReports.TextBox Website;
        private DataDynamics.ActiveReports.Label Label1;

        public Alpha.Page page;
        public string langId = "de";
        public string reportName = "niederlassungen";
        public int sosID = 0;
		

		public rptBZNiederlassungen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

       
		}





		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			}
			base.Dispose( disposing );
		}

		#region Report Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBZNiederlassungen));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.Line1 = new DataDynamics.ActiveReports.Line();
            this.Firma = new DataDynamics.ActiveReports.TextBox();
            this.Strasse = new DataDynamics.ActiveReports.TextBox();
            this.Ort = new DataDynamics.ActiveReports.TextBox();
            this.Land = new DataDynamics.ActiveReports.TextBox();
            this.Telefon = new DataDynamics.ActiveReports.TextBox();
            this.Fax = new DataDynamics.ActiveReports.TextBox();
            this.Website = new DataDynamics.ActiveReports.TextBox();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Firma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Strasse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Land)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Website)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Line1,
            this.Firma,
            this.Strasse,
            this.Ort,
            this.Land,
            this.Telefon,
            this.Fax,
            this.Website,
            this.Label1});
            this.Detail.Height = 0.21875F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format_1);
            // 
            // Line1
            // 
            this.Line1.Border.BottomColor = System.Drawing.Color.Black;
            this.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.LeftColor = System.Drawing.Color.Black;
            this.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.RightColor = System.Drawing.Color.Black;
            this.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.TopColor = System.Drawing.Color.Black;
            this.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.2121063F;
            this.Line1.Width = 13F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 13F;
            this.Line1.Y1 = 0.2121063F;
            this.Line1.Y2 = 0.2121063F;
            // 
            // Firma
            // 
            this.Firma.Border.BottomColor = System.Drawing.Color.Black;
            this.Firma.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.LeftColor = System.Drawing.Color.Black;
            this.Firma.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.RightColor = System.Drawing.Color.Black;
            this.Firma.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.TopColor = System.Drawing.Color.Black;
            this.Firma.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.DataField = "SOS_Name";
            this.Firma.Height = 0.1875F;
            this.Firma.Left = 0.09842521F;
            this.Firma.Name = "Firma";
            this.Firma.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Firma.Text = "Firma";
            this.Firma.Top = 0F;
            this.Firma.Width = 2.651575F;
            // 
            // Strasse
            // 
            this.Strasse.Border.BottomColor = System.Drawing.Color.Black;
            this.Strasse.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.LeftColor = System.Drawing.Color.Black;
            this.Strasse.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.RightColor = System.Drawing.Color.Black;
            this.Strasse.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.TopColor = System.Drawing.Color.Black;
            this.Strasse.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.DataField = "SOS_Street";
            this.Strasse.Height = 0.1875F;
            this.Strasse.Left = 2.75F;
            this.Strasse.Name = "Strasse";
            this.Strasse.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Strasse.Text = "Strasse";
            this.Strasse.Top = 0F;
            this.Strasse.Width = 2.0625F;
            // 
            // Ort
            // 
            this.Ort.Border.BottomColor = System.Drawing.Color.Black;
            this.Ort.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Ort.Border.LeftColor = System.Drawing.Color.Black;
            this.Ort.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Ort.Border.RightColor = System.Drawing.Color.Black;
            this.Ort.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Ort.Border.TopColor = System.Drawing.Color.Black;
            this.Ort.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Ort.DataField = "SOS_City";
            this.Ort.Height = 0.1875F;
            this.Ort.Left = 4.8125F;
            this.Ort.Name = "Ort";
            this.Ort.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Ort.Text = "Ort";
            this.Ort.Top = 0F;
            this.Ort.Width = 2.3125F;
            // 
            // Land
            // 
            this.Land.Border.BottomColor = System.Drawing.Color.Black;
            this.Land.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.LeftColor = System.Drawing.Color.Black;
            this.Land.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.RightColor = System.Drawing.Color.Black;
            this.Land.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.TopColor = System.Drawing.Color.Black;
            this.Land.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.DataField = "CNL_Name";
            this.Land.Height = 0.1875F;
            this.Land.Left = 7.1875F;
            this.Land.Name = "Land";
            this.Land.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Land.Text = "Land";
            this.Land.Top = 0F;
            this.Land.Width = 1.1875F;
            // 
            // Telefon
            // 
            this.Telefon.Border.BottomColor = System.Drawing.Color.Black;
            this.Telefon.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.LeftColor = System.Drawing.Color.Black;
            this.Telefon.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.RightColor = System.Drawing.Color.Black;
            this.Telefon.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.TopColor = System.Drawing.Color.Black;
            this.Telefon.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.DataField = "SOS_Phone";
            this.Telefon.Height = 0.1875F;
            this.Telefon.Left = 8.4375F;
            this.Telefon.Name = "Telefon";
            this.Telefon.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Telefon.Text = "Telefon";
            this.Telefon.Top = 0F;
            this.Telefon.Width = 1.125F;
            // 
            // Fax
            // 
            this.Fax.Border.BottomColor = System.Drawing.Color.Black;
            this.Fax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.LeftColor = System.Drawing.Color.Black;
            this.Fax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.RightColor = System.Drawing.Color.Black;
            this.Fax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.TopColor = System.Drawing.Color.Black;
            this.Fax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.DataField = "SOS_Fax";
            this.Fax.Height = 0.1875F;
            this.Fax.Left = 9.625F;
            this.Fax.Name = "Fax";
            this.Fax.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Fax.Text = "Fax";
            this.Fax.Top = 0F;
            this.Fax.Width = 1.1875F;
            // 
            // Website
            // 
            this.Website.Border.BottomColor = System.Drawing.Color.Black;
            this.Website.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Website.Border.LeftColor = System.Drawing.Color.Black;
            this.Website.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Website.Border.RightColor = System.Drawing.Color.Black;
            this.Website.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Website.Border.TopColor = System.Drawing.Color.Black;
            this.Website.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Website.DataField = "SOS_web";
            this.Website.Height = 0.1875F;
            this.Website.Left = 10.8125F;
            this.Website.Name = "Website";
            this.Website.Style = "ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align: top; ";
            this.Website.Text = "Website";
            this.Website.Top = 0F;
            this.Website.Width = 2.1875F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.1968504F;
            this.Label1.HyperLink = null;
            this.Label1.Left = -0.0492126F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "color: Red; ddo-char-set: 1; font-family: Webdings; ";
            this.Label1.Text = "4";
            this.Label1.Top = 1.46665E-09F;
            this.Label1.Width = 0.1230315F;
            // 
            // rptBZNiederlassungen
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 13F;
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Firma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Strasse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Land)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Website)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
		#endregion

       
        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {

        }
        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            DataSet tblOffice = Hawa.Src.Productfinder.Model.Bezugsquellen.getBezugsquellenSearchDet(this.sosID.ToString());


        }

        private void Detail_Format_1(object sender, EventArgs e)
        {

        }

	}
}
