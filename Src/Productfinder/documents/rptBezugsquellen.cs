using System;
using System.Drawing;
using System.Collections;
using System.Configuration;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Web;

namespace Hawa
{
	/// <summary>
	/// Summary description for rptBezugsquellen.
	/// </summary>
	public class rptBezugsquellen : DataDynamics.ActiveReports.ActiveReport3
	{
		private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.TextBox Firma;
        private DataDynamics.ActiveReports.TextBox Strasse;
        private DataDynamics.ActiveReports.TextBox lblCity;
        private DataDynamics.ActiveReports.TextBox Land;
        private DataDynamics.ActiveReports.TextBox Telefon;
        private DataDynamics.ActiveReports.TextBox Fax;
        private DataDynamics.ActiveReports.TextBox Webseite;
        private DataDynamics.ActiveReports.TextBox txt_sosID;
        private DataDynamics.ActiveReports.SubReport SubReport;
        private DataDynamics.ActiveReports.Line Line1;
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.Label lblFirma;
        private DataDynamics.ActiveReports.Label lblStrasse;
        private DataDynamics.ActiveReports.Label lblPLZ;
        private DataDynamics.ActiveReports.Label lblCountry;
        private DataDynamics.ActiveReports.Label lblPhone;
        private DataDynamics.ActiveReports.Label lblFax;
        private DataDynamics.ActiveReports.Label lblWebseite;
        private DataDynamics.ActiveReports.Line Line;
        private DataDynamics.ActiveReports.TextBox lblTitle;
        private DataDynamics.ActiveReports.PageBreak PageBreak;
        private DataDynamics.ActiveReports.Label lblProductName;
        private DataDynamics.ActiveReports.Picture Picture;
        private DataDynamics.ActiveReports.PageFooter PageFooter;
        private DataDynamics.ActiveReports.TextBox Footer;
        private DataDynamics.ActiveReports.TextBox TextBox;
        private DataDynamics.ActiveReports.Label lblSeite;
        private DataDynamics.ActiveReports.TextBox Seite;

        public Alpha.Page page; // to get the strings
        public string langId = "en";
        public string reportName = "bezugsquellen";
   
       
       


		public rptBezugsquellen()
		{
			//
			// Required for Windows Form Designer support
			//
            //this.SetLicense("Tobias Wuethrich,Hawa AG,DD-ARN-30-C000871,FO47HSJO8F9OMFHH88SF");
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

            DataBind();

		}

        private void DataBind()
        {

            DataDynamics.ActiveReports.DataSources.SqlDBDataSource m_ds = new DataDynamics.ActiveReports.DataSources.SqlDBDataSource();
            m_ds.ConnectionString = ConfigurationSettings.AppSettings["DSN"];

            string sWhere = HttpContext.Current.Session["sWhere"].ToString();
            string sort = HttpContext.Current.Session["sortOrder"].ToString();
            langId = HttpContext.Current.Session["languageID"].ToString();
            string countryID = HttpContext.Current.Session["CountryID"].ToString();

            m_ds.SQL = "SELECT CSOS.SOS_ID, CSOS.SOS_Parent_ID, (CSOS.SOS_Name + ' ' + ISNULL(CSOS.SOS_AdditionalName1,'')+ ' (' + ISNULL(CSOSTL.STL_Name,'')+')') as SOS_Name,(ISNULL(CSOS.SOS_Street,'') +' '+ISNULL(CSOS.SOS_Street2,''))as SOS_Street, " +
                "(ISNULL(CSOS.SOS_ZIP,'') +' '+ ISNULL(CSOS.SOS_City,''))as SOS_City,CSOS.SOS_Phone, CSOS.SOS_Fax, CSOS.SOS_Web, CCL.CNL_Name " +
                "FROM   CLAPP_SourceOfSupplay CSOS " +
                "LEFT JOIN CLAPP_SourceOfSupplay_Country CSOSCY ON CSOS.SOS_ID=CSOSCY.SOS_ID " +
                "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayCategory CSOSSOSC ON CSOS.SOS_ID = CSOSSOSC.SOS_ID " +
                "INNER JOIN CLAPP_SourceOfSupplay_SourceOfSupplayType CSOSSOST ON CSOS.SOS_ID = CSOSSOST.SOS_ID " +
                "INNER JOIN CLAPP_SourceOfSupplayCategory_Article CSOSCA " +
                "INNER JOIN CLAPP_SourceOfSupplayCategory CSOSC ON CSOSCA.SCA_ID = CSOSC.SCA_ID ON CSOSSOSC.SCA_ID = CSOSC.SCA_ID " +
                "INNER JOIN CLAPP_SourceOfSupplayTypeLang CSOSTL ON CSOSSOST.SST_ID = CSOSTL.SST_ID AND CSOSTL.LNG_ID ='" + this.langId + "'" +
                 "INNER JOIN CLAPP_CountryLang CCL ON CSOS.CNT_ID = CCL.CNT_ID AND CCL.LNG_ID ='" + this.langId + "'" +
                
                "WHERE  1=1 " + sWhere + " " +
                "GROUP BY  CSOS.SOS_ID, CSOS.SOS_Parent_ID, CSOS.SOS_Name, CSOS.SOS_AdditionalName1, CSOS.SOS_Street, " +
                "CSOS.SOS_Street2,CSOS.SOS_City, CSOS.SOS_ZIP,CSOSTL.STL_Name, CSOS.SOS_Phone, CSOS.SOS_Fax, CSOS.SOS_Web, CCL.CNL_Name " +
                "ORDER BY " + sort;






            this.DataSource = m_ds;

        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
             
            this.lblProductName.Visible = false;
            this.lblTitle.Text = this.page.getString("DisplayTitle", true);
            this.lblFirma.Text = this.page.getString("firma", true);
            this.lblStrasse.Text = this.page.getString("strasse", true);
            this.lblPLZ.Text = this.page.getString("plz", true) + "/" + this.page.getString("ort", true);
            this.lblCountry.Text = this.page.getString("land", true);
            this.lblPhone.Text = this.page.getString("tel", true);
            this.lblFax.Text = this.page.getString("fax", true);
            this.lblWebseite.Text = this.page.getString("web", true);



            if (HttpContext.Current.Session["productName"] != null)
            {
                this.lblTitle.Text = this.page.getString("TipText", true);
                this.lblProductName.Text = HttpContext.Current.Session["productName"].ToString();
                this.lblProductName.Visible = true;
            }

        }

        private void PageFooter_Format(object sender, System.EventArgs eArgs)
        {
            this.lblSeite.Text = this.page.getString("seite", true);

        }
        
        
        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			}
			base.Dispose( disposing );
		}

		#region Report Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBezugsquellen));
            DataDynamics.ActiveReports.DataSources.OleDBDataSource oleDBDataSource1 = new DataDynamics.ActiveReports.DataSources.OleDBDataSource();
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.Firma = new DataDynamics.ActiveReports.TextBox();
            this.Strasse = new DataDynamics.ActiveReports.TextBox();
            this.lblCity = new DataDynamics.ActiveReports.TextBox();
            this.Land = new DataDynamics.ActiveReports.TextBox();
            this.Telefon = new DataDynamics.ActiveReports.TextBox();
            this.Fax = new DataDynamics.ActiveReports.TextBox();
            this.Webseite = new DataDynamics.ActiveReports.TextBox();
            this.txt_sosID = new DataDynamics.ActiveReports.TextBox();
            this.SubReport = new DataDynamics.ActiveReports.SubReport();
            this.Line1 = new DataDynamics.ActiveReports.Line();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.lblFirma = new DataDynamics.ActiveReports.Label();
            this.lblStrasse = new DataDynamics.ActiveReports.Label();
            this.lblPLZ = new DataDynamics.ActiveReports.Label();
            this.lblCountry = new DataDynamics.ActiveReports.Label();
            this.lblPhone = new DataDynamics.ActiveReports.Label();
            this.lblFax = new DataDynamics.ActiveReports.Label();
            this.lblWebseite = new DataDynamics.ActiveReports.Label();
            this.Line = new DataDynamics.ActiveReports.Line();
            this.lblTitle = new DataDynamics.ActiveReports.TextBox();
            this.PageBreak = new DataDynamics.ActiveReports.PageBreak();
            this.lblProductName = new DataDynamics.ActiveReports.Label();
            this.Picture = new DataDynamics.ActiveReports.Picture();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.Footer = new DataDynamics.ActiveReports.TextBox();
            this.TextBox = new DataDynamics.ActiveReports.TextBox();
            this.lblSeite = new DataDynamics.ActiveReports.Label();
            this.Seite = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Firma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Strasse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Land)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Webseite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sosID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFirma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStrasse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWebseite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Firma,
            this.Strasse,
            this.lblCity,
            this.Land,
            this.Telefon,
            this.Fax,
            this.Webseite,
            this.txt_sosID,
            this.SubReport,
            this.Line1});
            this.Detail.Height = 0.375F;
            this.Detail.Name = "Detail";
            // 
            // Firma
            // 
            this.Firma.Border.BottomColor = System.Drawing.Color.Black;
            this.Firma.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.LeftColor = System.Drawing.Color.Black;
            this.Firma.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.RightColor = System.Drawing.Color.Black;
            this.Firma.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.Border.TopColor = System.Drawing.Color.Black;
            this.Firma.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Firma.DataField = "SOS_Name";
            this.Firma.Height = 0.1875F;
            this.Firma.Left = 0F;
            this.Firma.Name = "Firma";
            this.Firma.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Firma.Text = "Firma";
            this.Firma.Top = 0.1875F;
            this.Firma.Width = 2.6875F;
            // 
            // Strasse
            // 
            this.Strasse.Border.BottomColor = System.Drawing.Color.Black;
            this.Strasse.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.LeftColor = System.Drawing.Color.Black;
            this.Strasse.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.RightColor = System.Drawing.Color.Black;
            this.Strasse.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.Border.TopColor = System.Drawing.Color.Black;
            this.Strasse.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Strasse.DataField = "SOS_Street";
            this.Strasse.Height = 0.1875F;
            this.Strasse.Left = 2.75F;
            this.Strasse.Name = "Strasse";
            this.Strasse.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Strasse.Text = "Strasse";
            this.Strasse.Top = 0.1875F;
            this.Strasse.Width = 2.0625F;
            // 
            // lblCity
            // 
            this.lblCity.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCity.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCity.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCity.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCity.Border.RightColor = System.Drawing.Color.Black;
            this.lblCity.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCity.Border.TopColor = System.Drawing.Color.Black;
            this.lblCity.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCity.DataField = "SOS_City";
            this.lblCity.Height = 0.1875F;
            this.lblCity.Left = 4.8125F;
            this.lblCity.Name = "lblCity";
            this.lblCity.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.lblCity.Text = "Ort";
            this.lblCity.Top = 0.1875F;
            this.lblCity.Width = 2.3125F;
            // 
            // Land
            // 
            this.Land.Border.BottomColor = System.Drawing.Color.Black;
            this.Land.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.LeftColor = System.Drawing.Color.Black;
            this.Land.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.RightColor = System.Drawing.Color.Black;
            this.Land.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.Border.TopColor = System.Drawing.Color.Black;
            this.Land.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Land.DataField = "CNL_Name";
            this.Land.Height = 0.1875F;
            this.Land.Left = 7.1875F;
            this.Land.Name = "Land";
            this.Land.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Land.Text = "Land";
            this.Land.Top = 0.1875F;
            this.Land.Width = 1.1875F;
            // 
            // Telefon
            // 
            this.Telefon.Border.BottomColor = System.Drawing.Color.Black;
            this.Telefon.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.LeftColor = System.Drawing.Color.Black;
            this.Telefon.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.RightColor = System.Drawing.Color.Black;
            this.Telefon.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.Border.TopColor = System.Drawing.Color.Black;
            this.Telefon.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Telefon.DataField = "SOS_Phone";
            this.Telefon.Height = 0.1875F;
            this.Telefon.Left = 8.4375F;
            this.Telefon.Name = "Telefon";
            this.Telefon.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Telefon.Text = "Telefon";
            this.Telefon.Top = 0.1875F;
            this.Telefon.Width = 1.125F;
            // 
            // Fax
            // 
            this.Fax.Border.BottomColor = System.Drawing.Color.Black;
            this.Fax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.LeftColor = System.Drawing.Color.Black;
            this.Fax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.RightColor = System.Drawing.Color.Black;
            this.Fax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.Border.TopColor = System.Drawing.Color.Black;
            this.Fax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Fax.DataField = "SOS_Fax";
            this.Fax.Height = 0.1875F;
            this.Fax.Left = 9.625F;
            this.Fax.Name = "Fax";
            this.Fax.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Fax.Text = "Fax";
            this.Fax.Top = 0.1875F;
            this.Fax.Width = 1.1875F;
            // 
            // Webseite
            // 
            this.Webseite.Border.BottomColor = System.Drawing.Color.Black;
            this.Webseite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Webseite.Border.LeftColor = System.Drawing.Color.Black;
            this.Webseite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Webseite.Border.RightColor = System.Drawing.Color.Black;
            this.Webseite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Webseite.Border.TopColor = System.Drawing.Color.Black;
            this.Webseite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Webseite.DataField = "SOS_web";
            this.Webseite.Height = 0.1875F;
            this.Webseite.Left = 10.8125F;
            this.Webseite.Name = "Webseite";
            this.Webseite.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; vertical-align: top; ";
            this.Webseite.Text = "Webseite";
            this.Webseite.Top = 0.1875F;
            this.Webseite.Width = 2.125F;
            // 
            // txt_sosID
            // 
            this.txt_sosID.Border.BottomColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.LeftColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.RightColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.Border.TopColor = System.Drawing.Color.Black;
            this.txt_sosID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txt_sosID.DataField = "SOS_ID";
            this.txt_sosID.Height = 0.2F;
            this.txt_sosID.Left = 0F;
            this.txt_sosID.Name = "txt_sosID";
            this.txt_sosID.Style = "";
            this.txt_sosID.Text = "SOS_ID";
            this.txt_sosID.Top = 0.5624999F;
            this.txt_sosID.Visible = false;
            this.txt_sosID.Width = 1F;
            // 
            // SubReport
            // 
            this.SubReport.Border.BottomColor = System.Drawing.Color.Black;
            this.SubReport.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.LeftColor = System.Drawing.Color.Black;
            this.SubReport.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.RightColor = System.Drawing.Color.Black;
            this.SubReport.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.Border.TopColor = System.Drawing.Color.Black;
            this.SubReport.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.SubReport.CloseBorder = false;
            this.SubReport.Height = 0.15625F;
            this.SubReport.Left = 0F;
            this.SubReport.Name = "SubReport";
            this.SubReport.Report = null;
            this.SubReport.Top = 0.40625F;
            this.SubReport.Width = 12.9375F;
            // 
            // Line1
            // 
            this.Line1.Border.BottomColor = System.Drawing.Color.Black;
            this.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.LeftColor = System.Drawing.Color.Black;
            this.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.RightColor = System.Drawing.Color.Black;
            this.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.TopColor = System.Drawing.Color.Black;
            this.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.375F;
            this.Line1.Width = 12.9375F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 12.9375F;
            this.Line1.Y1 = 0.375F;
            this.Line1.Y2 = 0.375F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblFirma,
            this.lblStrasse,
            this.lblPLZ,
            this.lblCountry,
            this.lblPhone,
            this.lblFax,
            this.lblWebseite,
            this.Line,
            this.lblTitle,
            this.PageBreak,
            this.lblProductName,
            this.Picture});
            this.PageHeader.Height = 1.583333F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblFirma
            // 
            this.lblFirma.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFirma.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFirma.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.RightColor = System.Drawing.Color.Black;
            this.lblFirma.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Border.TopColor = System.Drawing.Color.Black;
            this.lblFirma.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFirma.Height = 0.2F;
            this.lblFirma.HyperLink = null;
            this.lblFirma.Left = 0F;
            this.lblFirma.Name = "lblFirma";
            this.lblFirma.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblFirma.Text = "Firma";
            this.lblFirma.Top = 1.308563F;
            this.lblFirma.Width = 1F;
            // 
            // lblStrasse
            // 
            this.lblStrasse.Border.BottomColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.LeftColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.RightColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Border.TopColor = System.Drawing.Color.Black;
            this.lblStrasse.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblStrasse.Height = 0.2F;
            this.lblStrasse.HyperLink = null;
            this.lblStrasse.Left = 2.75F;
            this.lblStrasse.Name = "lblStrasse";
            this.lblStrasse.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblStrasse.Text = "Strasse";
            this.lblStrasse.Top = 1.308563F;
            this.lblStrasse.Width = 1F;
            // 
            // lblPLZ
            // 
            this.lblPLZ.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.RightColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Border.TopColor = System.Drawing.Color.Black;
            this.lblPLZ.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPLZ.Height = 0.2F;
            this.lblPLZ.HyperLink = null;
            this.lblPLZ.Left = 4.8125F;
            this.lblPLZ.Name = "lblPLZ";
            this.lblPLZ.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblPLZ.Text = "PLZ/Ort";
            this.lblPLZ.Top = 1.308563F;
            this.lblPLZ.Width = 2.29872F;
            // 
            // lblCountry
            // 
            this.lblCountry.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCountry.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCountry.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.RightColor = System.Drawing.Color.Black;
            this.lblCountry.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Border.TopColor = System.Drawing.Color.Black;
            this.lblCountry.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCountry.Height = 0.2F;
            this.lblCountry.HyperLink = null;
            this.lblCountry.Left = 7.1875F;
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblCountry.Text = "Land";
            this.lblCountry.Top = 1.308563F;
            this.lblCountry.Width = 0.8125F;
            // 
            // lblPhone
            // 
            this.lblPhone.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPhone.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPhone.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.RightColor = System.Drawing.Color.Black;
            this.lblPhone.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Border.TopColor = System.Drawing.Color.Black;
            this.lblPhone.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPhone.Height = 0.2F;
            this.lblPhone.HyperLink = null;
            this.lblPhone.Left = 8.4375F;
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblPhone.Text = "Telefon";
            this.lblPhone.Top = 1.308563F;
            this.lblPhone.Width = 1.01132F;
            // 
            // lblFax
            // 
            this.lblFax.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.RightColor = System.Drawing.Color.Black;
            this.lblFax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.TopColor = System.Drawing.Color.Black;
            this.lblFax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Height = 0.2F;
            this.lblFax.HyperLink = null;
            this.lblFax.Left = 9.625F;
            this.lblFax.Name = "lblFax";
            this.lblFax.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblFax.Text = "Fax";
            this.lblFax.Top = 1.308563F;
            this.lblFax.Width = 0.625F;
            // 
            // lblWebseite
            // 
            this.lblWebseite.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.LeftColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.RightColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Border.TopColor = System.Drawing.Color.Black;
            this.lblWebseite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWebseite.Height = 0.2F;
            this.lblWebseite.HyperLink = null;
            this.lblWebseite.Left = 10.8125F;
            this.lblWebseite.Name = "lblWebseite";
            this.lblWebseite.Style = "font-weight: bold; font-size: 12pt; ";
            this.lblWebseite.Text = "Webseite";
            this.lblWebseite.Top = 1.308563F;
            this.lblWebseite.Width = 1.125F;
            // 
            // Line
            // 
            this.Line.Border.BottomColor = System.Drawing.Color.Black;
            this.Line.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.LeftColor = System.Drawing.Color.Black;
            this.Line.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.RightColor = System.Drawing.Color.Black;
            this.Line.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Border.TopColor = System.Drawing.Color.Black;
            this.Line.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line.Height = 0F;
            this.Line.Left = 0F;
            this.Line.LineWeight = 2F;
            this.Line.Name = "Line";
            this.Line.Top = 1.558563F;
            this.Line.Width = 12.9375F;
            this.Line.X1 = 0F;
            this.Line.X2 = 12.9375F;
            this.Line.Y1 = 1.558563F;
            this.Line.Y2 = 1.558563F;
            // 
            // lblTitle
            // 
            this.lblTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTitle.CanShrink = true;
            this.lblTitle.Height = 0.3198819F;
            this.lblTitle.Left = -1.46665E-09F;
            this.lblTitle.MultiLine = false;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "color: Black; ddo-char-set: 1; font-weight: bold; font-size: 18pt; font-family: A" +
                "rial; white-space: nowrap; ";
            this.lblTitle.Text = "Hawa-Bezugsquellen";
            this.lblTitle.Top = 0.4429134F;
            this.lblTitle.Width = 9.104334F;
            // 
            // PageBreak
            // 
            this.PageBreak.Border.BottomColor = System.Drawing.Color.Black;
            this.PageBreak.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.LeftColor = System.Drawing.Color.Black;
            this.PageBreak.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.RightColor = System.Drawing.Color.Black;
            this.PageBreak.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Border.TopColor = System.Drawing.Color.Black;
            this.PageBreak.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PageBreak.Height = 0.05555556F;
            this.PageBreak.Left = 0F;
            this.PageBreak.Name = "PageBreak";
            this.PageBreak.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
            this.PageBreak.Top = 1.119444F;
            this.PageBreak.Width = 6.5F;
            // 
            // lblProductName
            // 
            this.lblProductName.Border.BottomColor = System.Drawing.Color.Black;
            this.lblProductName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblProductName.Border.LeftColor = System.Drawing.Color.Black;
            this.lblProductName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblProductName.Border.RightColor = System.Drawing.Color.Black;
            this.lblProductName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblProductName.Border.TopColor = System.Drawing.Color.Black;
            this.lblProductName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblProductName.Height = 0.246063F;
            this.lblProductName.HyperLink = null;
            this.lblProductName.Left = -7.186583E-08F;
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Style = "ddo-char-set: 1; font-weight: bold; font-size: 14pt; font-family: Arial; ";
            this.lblProductName.Text = "Produkt";
            this.lblProductName.Top = 0.7874014F;
            this.lblProductName.Width = 4.601378F;
            // 
            // Picture
            // 
            this.Picture.Border.BottomColor = System.Drawing.Color.Black;
            this.Picture.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.LeftColor = System.Drawing.Color.Black;
            this.Picture.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.RightColor = System.Drawing.Color.Black;
            this.Picture.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Border.TopColor = System.Drawing.Color.Black;
            this.Picture.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Picture.Height = 0.6456693F;
            this.Picture.Image = ((System.Drawing.Image)(resources.GetObject("Picture.Image")));
            this.Picture.ImageData = ((System.IO.Stream)(resources.GetObject("Picture.ImageData")));
            this.Picture.Left = 11.5F;
            this.Picture.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Picture.LineWeight = 0F;
            this.Picture.Name = "Picture";
            this.Picture.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.Picture.Top = 0.0625F;
            this.Picture.Width = 1.574803F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Footer,
            this.TextBox,
            this.lblSeite,
            this.Seite});
            this.PageFooter.Height = 0.6875F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // Footer
            // 
            this.Footer.Border.BottomColor = System.Drawing.Color.Black;
            this.Footer.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.LeftColor = System.Drawing.Color.Black;
            this.Footer.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.RightColor = System.Drawing.Color.Black;
            this.Footer.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Border.TopColor = System.Drawing.Color.Black;
            this.Footer.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Footer.Height = 0.1666667F;
            this.Footer.Left = 0F;
            this.Footer.Name = "Footer";
            this.Footer.Style = "ddo-char-set: 1; font-size: 8pt; font-family: Microsoft Sans Serif; ";
            this.Footer.Text = "Hawa AG Schiebebeschlagsysteme  Sliding Hardware Systems  Ferrures coulissantes";
            this.Footer.Top = 0.3333333F;
            this.Footer.Width = 7.874014F;
            // 
            // TextBox
            // 
            this.TextBox.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Height = 0.1875F;
            this.TextBox.Left = 0F;
            this.TextBox.Name = "TextBox";
            this.TextBox.Style = "ddo-char-set: 1; font-size: 8pt; font-family: Microsoft Sans Serif; ";
            this.TextBox.Text = "Untere Fischbachstrasse 4, Postfach, CH-8932 Mettmenstetten, Telefon +41 44 767 9" +
                "1 91, Telefax +41 44 767 91 78, www.hawa.ch";
            this.TextBox.Top = 0.5F;
            this.TextBox.Width = 7.874014F;
            // 
            // lblSeite
            // 
            this.lblSeite.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSeite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSeite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.RightColor = System.Drawing.Color.Black;
            this.lblSeite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Border.TopColor = System.Drawing.Color.Black;
            this.lblSeite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSeite.Height = 0.188F;
            this.lblSeite.HyperLink = null;
            this.lblSeite.Left = 12.375F;
            this.lblSeite.Name = "lblSeite";
            this.lblSeite.RightToLeft = true;
            this.lblSeite.Style = "ddo-char-set: 1; font-size: 8pt; ";
            this.lblSeite.Text = "Seite";
            this.lblSeite.Top = 0.5F;
            this.lblSeite.Width = 0.3125F;
            // 
            // Seite
            // 
            this.Seite.Border.BottomColor = System.Drawing.Color.Black;
            this.Seite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.LeftColor = System.Drawing.Color.Black;
            this.Seite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.RightColor = System.Drawing.Color.Black;
            this.Seite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Border.TopColor = System.Drawing.Color.Black;
            this.Seite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Seite.Height = 0.188F;
            this.Seite.Left = 12.6875F;
            this.Seite.Name = "Seite";
            this.Seite.RightToLeft = true;
            this.Seite.Style = "ddo-char-set: 1; font-size: 8pt; ";
            this.Seite.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All;
            this.Seite.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount;
            this.Seite.Text = "#";
            this.Seite.Top = 0.5F;
            this.Seite.Width = 0.25F;
            // 
            // rptBezugsquellen
            // 
            this.MasterReport = false;
            oleDBDataSource1.ConnectionString = "";
            oleDBDataSource1.SQL = "Select * from";
            this.DataSource = oleDBDataSource1;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.1819444F;
            this.PageSettings.Margins.Left = 0.5784722F;
            this.PageSettings.Margins.Right = 0.5784722F;
            this.PageSettings.Margins.Top = 0.1847222F;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperName = "Custom paper";
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 13.0748F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Firma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Strasse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Land)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Webseite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sosID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFirma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStrasse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWebseite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Footer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
		#endregion

      
	}
}
