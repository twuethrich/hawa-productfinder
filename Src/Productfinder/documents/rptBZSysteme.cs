using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;

namespace Hawa
{
	/// <summary>
	/// Summary description for rptBZSysteme.
	/// </summary>
	public class rptBZSysteme : DataDynamics.ActiveReports.ActiveReport3
	{
		private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.Line Line1;
        private DataDynamics.ActiveReports.TextBox Systems;
        private DataDynamics.ActiveReports.Label Label1;
        private DataDynamics.ActiveReports.Label Kurzbeschreibung;
        private DataDynamics.ActiveReports.Label artID;

        public Alpha.Page page;
        public string langId = "de";
        public string reportName = "systeme";
      
        public int sosID = 0;
		
		


		public rptBZSysteme()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
        
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			}
			base.Dispose( disposing );
		}

		#region Report Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBZSysteme));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.Line1 = new DataDynamics.ActiveReports.Line();
            this.Systems = new DataDynamics.ActiveReports.TextBox();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.Kurzbeschreibung = new DataDynamics.ActiveReports.Label();
            this.artID = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Systems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kurzbeschreibung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.artID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Line1,
            this.Systems,
            this.Label1,
            this.Kurzbeschreibung,
            this.artID});
            this.Detail.Height = 0.21875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Line1
            // 
            this.Line1.Border.BottomColor = System.Drawing.Color.Black;
            this.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.LeftColor = System.Drawing.Color.Black;
            this.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.RightColor = System.Drawing.Color.Black;
            this.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Border.TopColor = System.Drawing.Color.Black;
            this.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.1875F;
            this.Line1.Width = 13F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 13F;
            this.Line1.Y1 = 0.1875F;
            this.Line1.Y2 = 0.1875F;
            // 
            // Systems
            // 
            this.Systems.Border.BottomColor = System.Drawing.Color.Black;
            this.Systems.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Systems.Border.LeftColor = System.Drawing.Color.Black;
            this.Systems.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Systems.Border.RightColor = System.Drawing.Color.Black;
            this.Systems.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Systems.Border.TopColor = System.Drawing.Color.Black;
            this.Systems.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Systems.DataField = "ARL_Name";
            this.Systems.Height = 0.1875F;
            this.Systems.Left = 0.09842521F;
            this.Systems.Name = "Systems";
            this.Systems.Style = "color: Red; ddo-char-set: 1; font-weight: normal; font-size: 9pt; vertical-align:" +
                " top; ";
            this.Systems.Text = "Systeme";
            this.Systems.Top = 0F;
            this.Systems.Width = 2.18996F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.1968504F;
            this.Label1.HyperLink = null;
            this.Label1.Left = -0.0492126F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "color: Red; ddo-char-set: 1; font-family: Webdings; ";
            this.Label1.Text = "4";
            this.Label1.Top = 1.46665E-09F;
            this.Label1.Width = 0.1230315F;
            // 
            // Kurzbeschreibung
            // 
            this.Kurzbeschreibung.Border.BottomColor = System.Drawing.Color.Black;
            this.Kurzbeschreibung.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Kurzbeschreibung.Border.LeftColor = System.Drawing.Color.Black;
            this.Kurzbeschreibung.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Kurzbeschreibung.Border.RightColor = System.Drawing.Color.Black;
            this.Kurzbeschreibung.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Kurzbeschreibung.Border.TopColor = System.Drawing.Color.Black;
            this.Kurzbeschreibung.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Kurzbeschreibung.DataField = "ARL_Short";
            this.Kurzbeschreibung.Height = 0.2F;
            this.Kurzbeschreibung.HyperLink = null;
            this.Kurzbeschreibung.Left = 2.780512F;
            this.Kurzbeschreibung.Name = "Kurzbeschreibung";
            this.Kurzbeschreibung.Style = "";
            this.Kurzbeschreibung.Text = "Kurzbeschreibung";
            this.Kurzbeschreibung.Top = 0F;
            this.Kurzbeschreibung.Width = 10.1378F;
            // 
            // artID
            // 
            this.artID.Border.BottomColor = System.Drawing.Color.Black;
            this.artID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.artID.Border.LeftColor = System.Drawing.Color.Black;
            this.artID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.artID.Border.RightColor = System.Drawing.Color.Black;
            this.artID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.artID.Border.TopColor = System.Drawing.Color.Black;
            this.artID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.artID.DataField = "ART_ID";
            this.artID.Height = 0.2F;
            this.artID.HyperLink = null;
            this.artID.Left = 0F;
            this.artID.Name = "artID";
            this.artID.Style = "white-space: nowrap; ";
            this.artID.Text = "artID";
            this.artID.Top = 0.2214567F;
            this.artID.Width = 1F;
            // 
            // rptBZSysteme
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 13F;
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Systems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kurzbeschreibung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.artID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
		#endregion

        

   
        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            this.Systems.HyperLink = "http://217.114.113.34/productfinder/Src/Productfinder/dsp_detail.aspx?artID=" + Convert.ToInt32(this.artID.Text);

        }

   
	}
}
