<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<%@ Page language="c#" Codebehind="dsp_berechnungshilfe.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_berechnungshilfe" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td><img src="images/ts.gif" width="10" height="10"></td>
				<td rowspan="3"><img src="images/hawa_logo_small.png"></td>
				<td></td>
				<td><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right" width="100%"><a href="javascript:window.close();"><asp:image imageurl="images/btn_close_de.gif" border="0" Runat=server ID="btnClose"></asp:Image></a></td>
				<td><img src="images/ts.gif" width="10"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<TR>
				<TD colspan="4" class="BoxTitle"><ALPHA:STRING ID="String1" RUNAT="server" KEY="tuergewicht"></ALPHA:STRING></TD>
			</TR>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
		</TABLE>
		<!-- detail -->
		<TABLE BORDER="0" ID="Table1" CELLSPACING="1" CELLPADDING="1" WIDTH="400" >
			<FORM ID="Form1" METHOD="post" RUNAT="server">
				<TBODY>
					<TR>
						<TD rowspan="99"><img src="images/ts.gif" Width="10" height="5"></TD>
						<TD COLSPAN="2"><ALPHA:STRING ID="String2" RUNAT="server" KEY="TipText"></ALPHA:STRING></TD>
					</TR>
					<TR>
						<TD><img src="images/ts.gif" height="5"></TD>
					</TR>
					<TR>
						<TD width="100"><ALPHA:STRING ID="String3" RUNAT="server" KEY="material"></ALPHA:STRING></TD>
						<TD>
							<SELECT CLASS="inputField" style="WIDTH:100px" NAME="material" onchange="setValue();">
								<OPTION VALUE="1" selected>-</OPTION>
								<OPTION VALUE="2"><ALPHA:STRING ID="String10" RUNAT="server" KEY="Mat1"></ALPHA:STRING></OPTION>
								<OPTION VALUE="3"><ALPHA:STRING ID="String11" RUNAT="server" KEY="Mat2"></ALPHA:STRING></OPTION>
								<OPTION VALUE="4"><ALPHA:STRING ID="String12" RUNAT="server" KEY="Mat3"></ALPHA:STRING></OPTION>
								<OPTION VALUE="5"><ALPHA:STRING ID="String13" RUNAT="server" KEY="Mat4"></ALPHA:STRING></OPTION>
								<OPTION VALUE="6"><ALPHA:STRING ID="String14" RUNAT="server" KEY="Mat5"></ALPHA:STRING></OPTION>
								<OPTION VALUE="7"><ALPHA:STRING ID="String15" RUNAT="server" KEY="Mat6"></ALPHA:STRING></OPTION>
								<OPTION VALUE="8"><ALPHA:STRING ID="String16" RUNAT="server" KEY="Mat7"></ALPHA:STRING></OPTION>
								<OPTION VALUE="9"><ALPHA:STRING ID="String17" RUNAT="server" KEY="Mat8"></ALPHA:STRING></OPTION>
							</SELECT></TD>
					<TR>
						<TD height="14"><ALPHA:STRING ID="String8" RUNAT="server" KEY="spez_weight"></ALPHA:STRING></TD>
						<TD height="14"><INPUT class="inputField" onchange="checkInput()" id="spez_weight" style="WIDTH: 100px; TEXT-ALIGN: right"
								type="text" name="spez_weight" value="0" maxLength="10">&nbsp;&nbsp;kg/m3</TD>
					</TR>
					<TR>
						<TD><ALPHA:STRING ID="String4" RUNAT="server" KEY="laenge"></ALPHA:STRING></TD>
						<TD><INPUT STYLE="WIDTH:100px;TEXT-ALIGN:right" onchange="checkInput()" CLASS="inputField"
								value="0" MAXLENGTH="10" ID="l" TYPE="text">&nbsp;&nbsp;mm</TD>
					</TR>
					<TR>
						<TD><ALPHA:STRING ID="String5" RUNAT="server" KEY="breite"></ALPHA:STRING></TD>
						<TD><INPUT STYLE="WIDTH:100px;TEXT-ALIGN:right" onchange="checkInput()" CLASS="inputField"
								value="0" MAXLENGTH="10" ID="b" TYPE="text">&nbsp;&nbsp;mm</TD>
					</TR>
					<TR>
						<TD><ALPHA:STRING ID="String6" RUNAT="server" KEY="dicke"></ALPHA:STRING></TD>
						<TD><INPUT onchange="checkMat()" STYLE="WIDTH:100px;TEXT-ALIGN:right" CLASS="inputField" value="0"
								MAXLENGTH="10" ID="d" TYPE="text">&nbsp;&nbsp;mm</TD>
					</TR>
					<TR>
						<TD><ALPHA:STRING ID="String7" RUNAT="server" KEY="resultat"></ALPHA:STRING></TD>
						<TD><INPUT STYLE="WIDTH:100px;TEXT-ALIGN:right" CLASS="inputField" ID="Text1" TYPE="text" NAME="resultat"
								READONLY maxLength="10">&nbsp;&nbsp;kg</TD>
					</TR>
					<TR>
						<TD><img src="images/ts.gif" height="20"></TD>
					</TR>
					<TR>
						<TD COLSPAN="2" ALIGN="right">
							<a href="javascript:calc()"><asp:IMaGe imageUrl="images/btn_calc_de.gif" runat=server ID="btnCalc"></asp:Image></a><img src="images/ts.gif" width="10">
						</TD>
					</TR>
			</FORM>
			</TBODY>
		</TABLE>
		<SCRIPT LANGUAGE="javascript">
        // Gewichtsdefinitionen
        var gewicht = new Array(10);
			gewicht[1] = 700;
			gewicht[2] = 850;
			gewicht[3] = 400;
			gewicht[4] = 500;
			gewicht[5] = 500;
			gewicht[6] = 2500;
			gewicht[7] = 7800;
			gewicht[8] = 2700;
			
		function setValue(){
		   
			var material = document.Form1.material.selectedIndex ;
			document.Form1.spez_weight.value=gewicht[material];

		
			// Röhrenspann Maximaldicke 40mm
			var material = document.Form1.material.selectedIndex ;
			if (material == 3) { 
				d = 40; 
				document.Form1.d.value = d;
				document.Form1.d.readOnly = true;
				document.Form1.d.disabled = true;
			} else {
				document.Form1.d.readOnly = false;
				document.Form1.d.disabled = false;
			};
			checkMat();
		}
			
			function changeMat(){
				document.Form1.material.selectedIndex=0;
				checkInput()
			}

			function checkInput(){
                var b = document.Form1.b.value;
                var l = document.Form1.l.value;
                var d = document.Form1.d.value;
                var sw = document.Form1.spez_weight.value;
                
                b = b.replace(/mm/,"");
                l = l.replace(/mm/,"");
                d = d.replace(/mm/,"");
                sw = sw.replace(/mm/,"");
                
                if (b == '') { b = 0; document.Form1.b.value = 0; }
                if (l == '') { l = 0; document.Form1.l.value = 0; }
                if (d == '') { d = 0; document.Form1.d.value = 0; }
                if (sw == '') { sw = 0; document.Form1.spez_weight.value = 0; }
                
                if (isNaN(b)) { document.Form1.b.value = 0; alert('<ALPHA:STRING ID="String31" RUNAT="server" KEY="Msg2"></ALPHA:STRING>!');};
                if (isNaN(l)) { document.Form1.l.value = 0; alert('<ALPHA:STRING ID="String32" RUNAT="server" KEY="Msg3"></ALPHA:STRING>');};
                if (isNaN(d)) { document.Form1.d.value = 0; alert('<ALPHA:STRING ID="String33" RUNAT="server" KEY="Msg4"></ALPHA:STRING>');};
                if (isNaN(sw)) { document.Form1.spez_weight.value = 0; alert('<ALPHA:STRING ID="String30" RUNAT="server" KEY="Msg1"></ALPHA:STRING>');};
			}

			function checkMat(){
				// Sonderfall Spanplatten
				var material = document.Form1.material.selectedIndex ;				                    
					
                if (material == 1) {
       				var d = document.Form1.d.value;
					d = d.replace(/mm/,"");
					if (d == '') { d = 0; document.Form1.d.value = 0; }
					if (isNaN(d)) { d = 0; alertValue('Dicke'); } else { document.Form1.d.value = 1*d; }
                
                    if (d >=  8 && d <=  9) gewicht[1] = 740;
                    if (d >= 10 && d <= 15) gewicht[1] = 720;
                    if (d >= 16 && d <= 18) gewicht[1] = 680;
                    if (d >= 19 && d <= 21) gewicht[1] = 670;
                    if (d >= 22 && d <= 24) gewicht[1] = 640;
                    if (d >= 25 && d <= 29) gewicht[1] = 630;
                    if (d >= 30 && d <= 35) gewicht[1] = 620;
                    if (d >= 36 && d <= 39) gewicht[1] = 610;
                    if (d >= 40 && d <= 45) gewicht[1] = 590;
                    if (d >= 46 && d <= 50) gewicht[1] = 570;
                    if (d >= 51 && d <= 55) gewicht[1] = 570;
					document.Form1.spez_weight.value=gewicht[material];
                }
                checkInput();
			}

            function calc() {
                // Werte lesen

                var material = document.Form1.material.selectedIndex ;

                var b = document.Form1.b.value;
                var l = document.Form1.l.value;
                var d = document.Form1.d.value;
                var sw = document.Form1.spez_weight.value;

                
                // Berechnung
                var r = (b*l*d*sw)/1000000000;
                r = number_format(r);
                document.Form1.resultat.value =  r;
                                
            }
                
                function alertDicke() {
                    document.Form1.d.value = '40';
                    alert('<ALPHA:STRING ID="String34" RUNAT="server" KEY="Msg5"></ALPHA:STRING>');
                }
                
				function number_format(n) {
					n = Math.round(n * 1000) / 1000;
					n = (n + 0.0001) + "";
					return n.substring(0, n.indexOf(".") + 4);
				}


            
		</SCRIPT>
		<P></P>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-3201274-1");
            pageTracker._trackPageview();
            pageTracker._setDomainName("none");
            pageTracker._setAllowLinker(true);
            pageTracker._setAllowHash(false);
        } catch (err) { }</script>
	</body>
</HTML>
