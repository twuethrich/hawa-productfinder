<%@ Page language="c#" Codebehind="dsp_planung_montage.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.Planung_montage" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<SCRIPT LANGUAGE="JavaScript" SRC="jscript/general.js" TYPE="text/javascript"></SCRIPT>
	</HEAD>
	<body style="MARGIN: 0px" onload="window.focus()">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<tr>
				<td><img src="images/ts.gif" width="10" height="10"></td>
				<td rowspan="3"><img src="images/hawa_logo_small.png"></td>
				<td></td>
				<td><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right" width="100%"><a href="javascript: window.close();"><asp:image ImageUrl="images/btn_close_de.gif" border="0" Runat="server" ID="btnClose"></asp:image></a></td>
				<td><img src="images/ts.gif" width="10"></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
			<TR>
				<TD colspan="4" class="BoxTitle"><ALPHA:STRING ID="String1" RUNAT="server" KEY="Title"></ALPHA:STRING></TD>
			</TR>
			<tr>
				<td colspan="4"><img src="images/ts.gif" width="10" height="10"></td>
			</tr>
		</TABLE>
		<TABLE cellSpacing="0" cellPadding="0" width="430" border="0" align="center">
			<form id="Form1" method="post" runat="server">
				<TBODY>
					<tr class="BoxGray" id="sp" runat="server">
						<TD height="20" width="20" valign="middle"><img src="images/ts.gif" width="10" height="10"><IMG HEIGHT="8" ALT="" SRC="images/arrow_red_large.gif" WIDTH="10" BORDER="0"></TD>
						<td width="100%">
							<ASP:HYPERLINK id="Systemplanner" RUNAT="server" NAVIGATEURL="">
								<b>
									<ALPHA:STRING ID="String19" RUNAT="server" KEY="Systemplanner"></ALPHA:STRING></b>
							</ASP:HYPERLINK>
						</td>
					</tr>
					<TR class="BoxText" id="sp2" runat="server">
						<td colspan="2" valign="top"><ALPHA:STRING ID="SPDescription" RUNAT="server" KEY="SPDescription"></ALPHA:STRING></td>
					</TR>
					<TR>
						<td colspan="2"><img src="images/ts.gif" width="10" height="10"></td>
					</TR>
					<TR vAlign="top">
						<TD colspan="2">
							<DIV class="TA" id="bestell_pdf" style="OVERFLOW: auto; scrollbar: ">
								<asp:datagrid id="dgrPlanung" runat="server" BorderStyle="Solid" BORDERWIDTH="1px" CELLPADDING="0" ONITEMDATABOUND="OnDataBound" PageSize="10" ShowHeader="False" Width="100%" CssClass="BoxGrayAlternate" AutoGenerateColumns="False" BorderColor="White">
									<AlternatingItemStyle CssClass="BoxGrayBright"></AlternatingItemStyle>
									<Columns>
										<asp:BoundColumn ReadOnly="True" HeaderText="image">
											<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn ReadOnly="True" HeaderText="Link">
											<ItemStyle Width="300px" VerticalAlign="Middle"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="OBJ_FileName" HeaderText="OBJ_FileName"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="OBJ_Name" HeaderText="OBJ_Name"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="OBJ_ID" ReadOnly="True" HeaderText="ID">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="OBJ_Size" HeaderText="OBJ_Size">
											<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="ARL_Name" HeaderText="ARL_Name"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="ART_Code" HeaderText="ART_Code"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="OBG_ID" HeaderText="OBG_ID"></asp:BoundColumn>
									</Columns>
									<PagerStyle VerticalAlign="Middle" NextPageText="N&#228;chste Seite" PrevPageText="Vorherige Seite"></PagerStyle>
								</asp:datagrid></DIV>
						</TD>
					</TR>
			</form></TBODY>
		</TABLE>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
   <script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-3201274-1");
    pageTracker._trackPageview();
    pageTracker._setDomainName("none");
    pageTracker._setAllowLinker(true);
    pageTracker._setAllowHash(false);
    } catch(err) {}</script>
	</body>
</HTML>