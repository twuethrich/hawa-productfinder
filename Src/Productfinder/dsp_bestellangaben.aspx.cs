using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class Besstellangaben : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		int agrID = 35; 
		//fill table with data for downlaod besstellangaben
		protected void Page_Load(object sender, System.EventArgs e) {
			try {
				int artID = Convert.ToInt32(Request.Params.Get("artID"));
                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }
				btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				dgrBestellangaben.DataSource = Model.ArticleAndGroup.getLanguageObjects(artID, agrID,-1);
				dgrBestellangaben.DataBind(); 
			}
			catch (Exception ex) {
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try {
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch {
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		//add links for every rows to download PDF files (Bestellangaben)
		public void OnDataBound(object source, DataGridItemEventArgs e)
		{
			try 
			{
				string name;
				double size;
				
				ListItemType itemType = (ListItemType)e.Item.ItemType;
				if (itemType == ListItemType.Header || itemType == ListItemType.Footer || itemType == ListItemType.Separator)
					return;

				name = ((TableCell)(e.Item.Controls[2])).Text.ToString();
				size = Convert.ToInt32(((TableCell)(e.Item.Controls[4])).Text) /1024;
				
				
				//tc.Text = "&#160;<img src=\"images/pdf_file.gif\">&#160;&#160;&#160;<a href=\"dsp_objectdownload.aspx?typ=7&id=" + ((TableCell)(e.Item.Controls[0])).Text.ToString() + "\" class=\"PRODUCT\">" + name + "</a>";
				e.Item.Cells[0].Text = "<img src=\"images/pdf_file.gif\">";
				e.Item.Cells[1].Text = "&nbsp;<a href=\"#\" onclick=\"javascript:window.open('dsp_objectdownload.aspx?typ=2&group="+agrID+"&id=" + ((TableCell)(e.Item.Controls[2])).Text.ToString() + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name + "</a>";
				e.Item.Cells[4].Text =  size + "kB &nbsp;";
				//e.Item.Cells[1].Text = name ;
			}
			catch (Exception ex)
			{
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				E.sParam = "";
				Response.Redirect("dsp_error_page.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
