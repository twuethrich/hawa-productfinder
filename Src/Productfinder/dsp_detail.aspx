<%@ Page CodeBehind="dsp_detail.aspx.cs" Language="c#" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_detail" %>
<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<HTML>
	<HEAD id="Header" runat="server">
		<TITLE>
            
            </TITLE>
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="Styles/colorbox.css" media="screen" />
            <SCRIPT language="JavaScript" src="jscript/general.js" type="text/javascript"></SCRIPT>
             <script type="text/javascript" src="jscript/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="jscript/jquery.colorbox.js"></script>
            

            
	</HEAD>
	<BODY style="MARGIN: 0px" onload="window.focus()">
		<FORM name="detail" RUNAT="server">
			<TABLE cellSpacing="0" cellPadding="0" width="900" border="0">
				<TR>
					<TD class="logo"><a href="dsp_searchForm.aspx" style="cursor:pointer;"><IMG src="images/hawa_logo.png" alt="" border="0"></a></TD>
					<TD vAlign="top" align="right">
						<!-- Title & Language, start-->
						<TABLE cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></TD>
								<TD><IMG height="1" src="images/ts.gif" width="12"></TD></TR>
							<TR>
								<TD align="right" colSpan="2"><a href="dsp_searchForm.aspx" style="cursor:pointer;"><ALPHA:STRING id="String2" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></a></TD>
								<TD></TD></TR>
							<TR>
								<TD colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></TD></TR>
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnFr" RUNAT="server">Fran�ais</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEs" RUNAT="server">Espa�ol</ASP:HYPERLINK>
								<td></td></tr></TABLE>
						<!-- Title & Language, start--></TD></TR>
				<!-- header, start-->
				<TR class="TopTitle">
					<TD vAlign="middle" height="30">
                        
                    </TD>
					<TD align="right">
                        <asp:ImageButton ID="btnBack" runat="server" ImageUrl="images/btn_back_de.gif" />
                       </TD></TR>
				<TR>
					<TD colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="1" border="0"></TD></TR></TABLE>
			<!-- header, end-->
			<!-- detail, start-->
			<DIV class="TA" id="tree" style="OVERFLOW: auto; WIDTH: 900; HEIGHT: 565px; scrollbar: ;" DESIGNTIMEDRAGDROP="517">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="10"><IMG height="1" alt="" src="images/ts.gif" width="10" border="0"></TD>
						<!-- product, start -->
						<TD width="620">
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="BoxTitle" colSpan="3" height="20"><ASP:LABEL id="lblProductName" RUNAT="server"></ASP:LABEL></TD>
								<TR class="BoxText">
									<TD vAlign="top" width="200" style="padding:10px;"><ASP:IMAGE id="imgProductPicture" RUNAT="server"></ASP:IMAGE></TD>
									<TD vAlign="top" width="100%" style="padding:10px;"><ASP:LABEL id="lblShort" RUNAT="server">Kurzbeschreibung</ASP:LABEL><BR><BR><B><ALPHA:STRING id="ZumProdukt" RUNAT="server" KEY="ZumProdukt"></ALPHA:STRING></B><BR><BR><ASP:LABEL id="lblProduct" RUNAT="server">zum Produkt</ASP:LABEL></TD></TR>
								<TR>
									<TD colSpan="2" height="10"><IMG height="10" alt="" src="images/ts.gif" width="10" border="0"></TD></TR></TABLE>
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<!-- anwendungsbereich, start -->
									<TD vAlign="top" width="425">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0"><asp:panel id="plTypisch" Runat="server">
												<TBODY>
													<TR vAlign="top">
														<TD class="BoxTitle"><ALPHA:STRING id="String1" RUNAT="server" KEY="Typisch"></ALPHA:STRING>&nbsp;<ASP:LABEL id="lblProductN" RUNAT="server"></ASP:LABEL></TD></TR>
													<TR>
														<TD class="BoxText" id="typisch" vAlign="top" height="160" runat="server"><ASP:LABEL id="lblTypisch" RUNAT="server">Typisch</ASP:LABEL></TD></TR>
													<TR id="spacer" runat="server">
														<TD><IMG height="10" src="images/ts.gif"></TD></TR></asp:panel><asp:panel id="plAnwendung" Runat="server">
												<TR>
													<TD class="BoxTitle" id="anwendung" runat="server"><ALPHA:STRING id="Anwendungsbereich" RUNAT="server" KEY="Anwendungsbereich"></ALPHA:STRING></TD></TR>
												<TR>
													<TD class="BoxText" vAlign="top" height="130"><ASP:LABEL id="lblAnwendung" RUNAT="server">Anwendungsbereich</ASP:LABEL></TD></TR></asp:panel></TABLE></TD>
									<!-- anwendungsbereich, end -->
									<TD width="10"><IMG src="images/ts.gif" width="10" border="0"></TD>
									<!-- weitereinfos, start -->
									<TD class="BoxGrayBright" vAlign="top" width="185" height="310">
										<TABLE class="BoxGrayBright" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD class="BoxTitle" colSpan="3" height="20"><ALPHA:STRING id="String6" RUNAT="server" KEY="info"></ALPHA:STRING></TD></TR>
											<TR class="BoxSubTitle">
												<TD colSpan="3"><B><ALPHA:STRING id="String8" RUNAT="server" KEY="tit_document"></ALPHA:STRING></B></TD></TR>
											<TR>
												<TD><IMG src="images/ts.gif" width="10"></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top" align="left" width="100%"><ASP:HYPERLINK id="Bestellangaben" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String5" RUNAT="server" KEY="Bestellangaben"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Planung" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String10" RUNAT="server" KEY="Planung_Montage"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Berechnungshilfe" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String11" RUNAT="server" KEY="Berechnungshilfe"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Zertifikate" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String12" RUNAT="server" KEY="Zertifikate"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD height="15"></TD>
												<TD height="15"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top" height="15"><ASP:HYPERLINK id="Prospekte" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String13" RUNAT="server" KEY="Prospekte"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
                                           
                                            <TR>
												<TD height="15"></TD>
												<TD height="15"><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top" height="15"><ASP:HYPERLINK id="Ausschreibungstexte" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String9" RUNAT="server" KEY="Ausschreibungstexte"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR class="BoxSubTitle">
												<TD vAlign="top" colSpan="3"><B><ALPHA:STRING id="String3" RUNAT="server" KEY="tit_Link"></ALPHA:STRING></B></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Bezugsquellen" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String14" RUNAT="server" KEY="Bezugsquellen"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Referenzen" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String15" RUNAT="server" KEY="Referenzen"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Presse" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String17" RUNAT="server" KEY="Pressezentrum"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="FAQ" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String18" RUNAT="server" KEY="FAQ"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
                                            <TR class="BoxSubTitle">
												<TD vAlign="top" colSpan="3"><B><ALPHA:STRING id="String4" RUNAT="server" KEY="tit_download"></ALPHA:STRING></B></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="download_dfx" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String20" RUNAT="server" KEY="dxf"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="download_eps" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String21" RUNAT="server" KEY="eps"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="accessories" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String19" RUNAT="server" KEY="accessories"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="download_fotos" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String22" RUNAT="server" KEY="Fotos"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
                                   

											<TR class="BoxSubTitle">
												<TD vAlign="top" colSpan="3"><B><ALPHA:STRING id="String23" RUNAT="server" KEY="tit_vip"></ALPHA:STRING></B></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="viproom" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String16" RUNAT="server" KEY="viproom"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD></TD>
												<TD><IMG height="8" alt="" src="images/arrow_red_large.gif" width="10" border="0"></TD>
												<TD vAlign="top"><ASP:HYPERLINK id="Preise" RUNAT="server" NAVIGATEURL="">
														<ALPHA:STRING ID="String24" RUNAT="server" KEY="Preise"></ALPHA:STRING>
													</ASP:HYPERLINK></TD></TR>
											<TR>
												<TD colSpan="3" height="10"><IMG height="10" alt="" src="images/ts.gif" width="10" border="0"></TD></TR></TABLE></TD>
									<!-- weitereinfos, end --></TR></TABLE></TD>
						<!-- product, end -->
						<TD width="10" rowSpan="20"><IMG height="1" alt="" src="images/ts.gif" width="10" border="0"></TD>
						<!-- right side, start -->
						<TD vAlign="top" width="250">
							<TABLE class="BoxGrayBright" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="BoxTitle" height="17"><ALPHA:STRING id="String7" RUNAT="server" KEY="Anwendung"></ALPHA:STRING></TD></TR>
								<TR>
									<TD><IMG height="10" src="images/ts.gif"></TD></TR>
								<TR>
									<TD align="center" width="250">
										<TABLE cellSpacing="0" cellPadding="0" width="230" bgColor="white" border="0">
											<TR>
												<TD align="center"><IMG src="dsp_objectdownload.aspx?id=<%= flashID %>&amp;typ=11" ></TD></TR></TABLE></TD></TR>
								<TR>
									<TD height="10"><IMG height="10" alt="" src="images/ts.gif" width="10" border="0"></TD></TR>
								<TR>
									<TD align="center" width="250">
										<TABLE cellSpacing="0" cellPadding="0" width="230" bgColor="white" border="0">
											<TR>
												<TD align="center"><ASP:IMAGE id="imgProductSymbol" RUNAT="server" IMAGEURL=""></ASP:IMAGE></TD></TR></TABLE></TD></TR>
								<TR>
									<TD height="10"><IMG height="10" alt="" src="images/ts.gif" width="10" border="0"></TD>
                               </TR>
                             </TABLE>
                            <div id="juniorPreview" runat="server" style="visibility:hidden;margin-top:10px;">xxx</div>
                            
                            </TD>

						<!-- right side, end -->
						<TD width="10" rowSpan="20"><IMG height="1" alt="" src="images/ts.gif" width="10" border="0"></TD></TR>
					</TBODY></TABLE></DIV></FORM>
    <!--Google Analytics relevant -->			
    <script type="text/javascript"> 

        jQuery.noConflict();
        jQuery(document).ready(function () {
            jQuery(".junior").colorbox({ opacity: "0.75" });
            jQuery(".junior").colorbox({ iframe: true, width: "900px", height: "680px" });
     
        });



    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-3201274-1");
            pageTracker._trackPageview();
            pageTracker._setDomainName("none");
            pageTracker._setAllowLinker(true);
            pageTracker._setAllowHash(false);
        } catch (err) { }</script>

	</BODY>
</HTML>
