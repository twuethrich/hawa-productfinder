using System;
using  System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Hawa.Src.Productfinder.Model;

namespace Hawa.Src.Productfinder
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class download_fotos : System.Web.UI.Page
	{
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		public static DataTable tabDownload;
		public static DataTable tabDownload2;
		public static DataTable tabDownload3;
		int agrID = 29; 
		int agrID2 = 30;
		int agrID3 = 31; 
		
	
		//fill table with data for download ESP files
		protected void Page_Load(object sender, System.EventArgs e) {
			try {
				int artID = Convert.ToInt32(Request.Params.Get("artID"));
                if (Request.Params.Get("languageID") != null)
                {
                    Session.Add("languageID", Request.Params.Get("languageID").ToString());
                }
                btnClose.ImageUrl ="images/btn_close_"+HttpContext.Current.Session["languageID"].ToString() + ".gif";
				string name1="";
				string name2="";
				string name3="";
				int size1=0;
				int size2=0;
				int size3=0;
				string filename1;
				string filename2;
				string filename3;
                string productname="";
                
            
	
				
				tabDownload = Model.ArticleAndGroup.getArticleAndGroup(artID, agrID,-1).Tables[0];
				if(tabDownload.Rows.Count > 0){
                    productname = tabDownload.Rows[0]["ARL_Name"].ToString();
                    name1 = tabDownload.Rows[0]["OBJ_Name"].ToString();
					filename1 = tabDownload.Rows[0]["OBJ_Filename"].ToString();
					Image1.ImageUrl = "dsp_objectdownload.aspx?typ=4&group=29&id=" + name1;
                    Label1.Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('Fotos','" + productname + "','" + filename1 + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID + "&id=" + filename1 + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name1 + "</a>";
					if(filename1.Length >0){
						size1 = Convert.ToInt32(tabDownload.Rows[0]["OBJ_Size"]);
						size1 = size1 /1024;
					}
					lblSize1.Text = size1 +"&nbsp;KB";
				}else{
					bild1.Visible = false;
				}

				
				tabDownload2 = Model.ArticleAndGroup.getArticleAndGroup(artID, agrID2,-1).Tables[0];
				if(tabDownload2.Rows.Count > 0){
                    productname = tabDownload2.Rows[0]["ARL_Name"].ToString();
					name2 = tabDownload2.Rows[0]["OBJ_Name"].ToString();
					filename2 = tabDownload2.Rows[0]["OBJ_Filename"].ToString();
					Image2.ImageUrl = "dsp_objectdownload.aspx?typ=4&group=30&id=" + name2;
                    Label2.Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('Fotos','" + productname + "','" + filename2 + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID2 + "&id=" + filename2 + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name2 + "</a>";
					if(filename2.Length >0){
						size2 = Convert.ToInt32(tabDownload2.Rows[0]["OBJ_Size"]);
						size2 = size2 /1024;
					}
					lblSize2.Text = size1 +"&nbsp;KB";
				}else{
					bild2.Visible = false;
				}
				tabDownload3 = Model.ArticleAndGroup.getArticleAndGroup(artID, agrID3,-1).Tables[0];
				if(tabDownload3.Rows.Count > 0){
                    productname = tabDownload2.Rows[0]["ARL_Name"].ToString();
					name3 = tabDownload3.Rows[0]["OBJ_Name"].ToString();
					filename3 = tabDownload3.Rows[0]["OBJ_Filename"].ToString();
					Image3.ImageUrl = "dsp_objectdownload.aspx?typ=4&group=31&id=" + name3;
                    Label3.Text = "&nbsp;<a href=\"#\" onclick=\"javascript:pageTracker._trackEvent('Fotos','" + productname + "','" + filename3 + "');window.open('dsp_objectdownload.aspx?typ=2&group=" + agrID3 + "&id=" + filename3 + "','new_window', 'height=250px, width=450px, toolbar=no, menubar=no, scrollbars=no, status=no, noresize')\">" + name3 + "</a>";
					if(filename3.Length >0){
						size3 = Convert.ToInt32(tabDownload3.Rows[0]["OBJ_Size"]);
						size3 = size3 /1024;
					}
					lblSize3.Text = size3 +"&nbsp;KB";
				}else{
					bild3.Visible = false;
				}
				
				int langID = 0;
				if(HttpContext.Current.Session["languageID"].ToString() == "EN"){
					langID=1;
				}

				//Link auf Marketing-Services
				string MarketingServiceURL = System.Configuration.ConfigurationSettings.AppSettings["MarketingServiceURL"]+"&L="+langID;
				MarketingService.Attributes.Add("onClick", "javascript:opener.opener.location=('" + MarketingServiceURL + "'); opener.opener.focus()"); 
				MarketingService.NavigateUrl = "#";
			
			 
			}
			catch (Exception ex) {
				ErrorMessage E = (ErrorMessage)(HttpContext.Current.Session["error"]);
				E.sErrorMessage = ex.Message.ToString();
				E.sErrorStackTrace = ex.StackTrace.ToString();
				try {
					E.sParam = "ART_ID=" + Request.Params.Get("artID").ToString();
				}
				catch {
					E.sParam = "";
				}
				Response.Redirect("dsp_error_page.aspx");
			}
		}




		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
