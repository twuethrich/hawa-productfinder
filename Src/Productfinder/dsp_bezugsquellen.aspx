<%@ Register TagPrefix="alpha" Namespace="Alpha.Controls" Assembly="Alpha" %>
<%@ Page language="c#" Codebehind="dsp_bezugsquellen.aspx.cs" AutoEventWireup="True" Inherits="Hawa.Src.Productfinder.dsp_bezugsquellen" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>
			<ALPHA:STRING ID="String9" RUNAT="server" KEY="PageTitle"></ALPHA:STRING></title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<LINK href="Styles/ie.css" type="text/css" rel="stylesheet">
			<Script language="JavaScript" src="jscript/general.js" type="text/javascript"></SCRIPT>
			<script>
		function confirmPDF(){
			alert('<%= alertText %>');
		}
			</script>
</HEAD>
	<body style="MARGIN: 0px" onload="window.focus(); setFocus('txtName');">
		<form name="Form1" id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td class="logo"><IMG src="images/hawa_logo.png"></td>
					<td vAlign="top" align="right">
						<!-- Title & Language, start-->
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td colSpan="2"><IMG height="12" src="images/ts.gif" width="1"></td>
								<td><IMG height="1" src="images/ts.gif" width="12"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ALPHA:STRING ID="String4" RUNAT="server" KEY="DisplayTitle"></ALPHA:STRING></td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="10" src="images/ts.gif" width="1"></td>
							</tr>
							<tr>
								<td align="right" colSpan="2"><ASP:HYPERLINK class="Language" id="btnDe" RUNAT="server">Deutsch</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEn" RUNAT="server">English</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnFr" RUNAT="server">Fran�ais</ASP:HYPERLINK>&nbsp;|&nbsp;
									<ASP:HYPERLINK class="Language" id="btnEs" RUNAT="server">Espa�ol</ASP:HYPERLINK>
								<td></td>
							</tr>
						</table>
						<!-- Title & Language, start-->
					</td>
				</tr>
				<!-- header, start-->
				<tr class="TopTitle">
					<td vAlign="middle" height="30">
						<IMG height="10" src="images/ts.gif" width="7">
						<ALPHA:STRING id="lblTitle" runat="server" Key="Title"></ALPHA:STRING><ALPHA:STRING id="String1" runat="server" Key="TipText"></ALPHA:STRING><b>
							<asp:label id="lblProductName" runat="server"></asp:label></b></td>
					<td vAlign="middle" align="right" style="padding-right:10px"><ASP:imagebutton IMAGEURL="images/btn_print_DE.gif" Runat="server" ID="btnPrint" border="0" onclick="btnPrint_Click"></ASP:imagebutton>
						<ASP:IMAGE id="btnClose" border="0" Runat="server" IMAGEURL="images/btn_close_DE.gif"></ASP:IMAGE></td>
				</tr>
				<tr>
					<td colSpan="2" height="10"><IMG height="1" alt="" src="images/ts.gif" width="1" border="0"></td>
				</tr>
				<!-- header, end-->
			</table>
			<!-- suche, start-->
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td width="10" height="20"><IMG src="images/ts.gif" width="10"></td>
					<td class="BoxTitle" height="20"><ALPHA:STRING id="String7" runat="server" Key="search"></ALPHA:STRING></td>
					<td width="10" height="20"><IMG src="images/ts.gif" width="10"></td>
				</tr>
				<TR>
					<td></td>
					<TD vAlign="top" class="BoxGrayBright">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="260" border="0">
							<TR>
								<TD width="10"><IMG height="10" src="images/ts.gif" width="10"></TD>
								<td colSpan="5"></td>
							</TR>
							<TR>
								<td></td>
								<TD width="100"><alpha:string id="String2" runat="server" Key="lblCountry"></alpha:string>&nbsp;&nbsp;</TD>
								<TD><asp:dropdownlist class="inputField" id="cmbCountry" runat="server" Width="170px"></asp:dropdownlist></TD>
								<td colSpan="3"></td>
							</TR>
							<TR>
								<TD><IMG height="2" src="images/ts.gif" width="1"></TD>
							</TR>
							<TR>
								<td></td>
								<TD width="24"><alpha:string id="String3" runat="server" Key="lblName"></alpha:string>&nbsp;&nbsp;</TD>
								<TD width="170"><asp:textbox id="txtName" runat="server" Width="170px" ontextchanged="txtName_TextChanged"></asp:textbox></TD>
								<TD width="10"><IMG src="images/ts.gif" width="10"></TD>
								<td align="left"><asp:imagebutton id="btnSearch" runat="server" ImageUrl="images/btn_search2_de.gif" onclick="btnSearch_Click"></asp:imagebutton></td>
                                <TD width="100%"><IMG src="images/ts.gif" width="10"></TD>
							</TR>
							<TR>
								<td width="35" colSpan="2"></td>
								<TD nowrap colspan="4">
                                    <asp:Panel ID="pTypes" runat="server"></asp:Panel>
								</TD>
							</TR>
							<TR>
								<TD><IMG height="10" src="images/ts.gif" width="10"></TD>
							</TR>
						</TABLE>
					</TD>
					<!-- suche, end-->
					<!--<TD class="BoxGrayBright" vAlign="bottom" align="right">
					<IMG src="images/listentitel.gif"></TD>-->
				</TR>
				<TR>
					<TD><IMG height="10" src="images/ts.gif" width="10"></TD>
				</TR>
			</table>
			<!-- liste, start-->
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" align="center">
				<tr>
					<td><img src="images/ts.gif" width="10"></td>
					<td align="left">
						<!--<DIV id="tree" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 460px">-->
						<ASP:DATAGRID id="dgrBezugsquellen" RUNAT="server" ALLOWSORTING="True" WIDTH="100%" HORIZONTALALIGN="Center" PAGESIZE="20" BORDERCOLOR="White" BORDERSTYLE="Solid" BORDERWIDTH="1px" CELLPADDING="0" AUTOGENERATECOLUMNS="False" AllowPaging="True" CssClass="BoxGrayAlternate" onselectedindexchanged="dgrBezugsquellen_SelectedIndexChanged">
							<AlternatingItemStyle CssClass="BoxGrayBright"></AlternatingItemStyle>
							<HeaderStyle ForeColor="Black" CssClass="BoxGrayBright"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="SOS_ID" HeaderText="ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_Name" SortExpression="SOS_Name" HeaderText="Firma">
									<HeaderStyle Wrap="False" Width="200px" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle Wrap="False" CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STL_Name" SortExpression="STL_Name" HeaderText="Art">
									<HeaderStyle Wrap="False" Width="70px" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_Street" SortExpression="SOS_Street" HeaderText="Strasse">
									<HeaderStyle Wrap="False" Width="180px" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_ZIP" SortExpression="SOS_ZIP" HeaderText="PLZ" HeaderStyle-Wrap="False">
									<HeaderStyle Wrap="False" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle Wrap="False" CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOS_City" SortExpression="SOS_City" HeaderText="Ort">
									<HeaderStyle Wrap="False" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=35&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" HeaderText="&lt;img src=&quot;images/ts.gif&quot; height=1 width=20&gt;"></asp:BoundColumn>
								<asp:BoundColumn DataField="CNL_Name" SortExpression="CNL_Name" HeaderText="Land">
									<HeaderStyle Wrap="False" Width="100" CssClass="TableHeader"></HeaderStyle>
									<ItemStyle CssClass="TableText"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="right" Position="Top" CssClass="pager" Mode="NumericPages" >                         
                        </PagerStyle>
						</ASP:DATAGRID><br>
						<alpha:String id="strError" runat="server" Visible="False" Key="errorText"></alpha:String>
						<!--</DIV>-->
					</td>
					<td><img src="images/ts.gif" width="10"></td>
				</tr>
			</table>
		</form>
		 <!--Google Analytics relevant -->			
    <script type="text/javascript"> 
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
   <script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-3201274-1");
    pageTracker._trackPageview();
    pageTracker._setDomainName("none");
    pageTracker._setAllowLinker(true);
    pageTracker._setAllowHash(false);
    } catch(err) {}</script>
	</body>
</HTML>
