﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace Hawa.Src.Productfinder {
    
    
    public partial class acc_frm_main {
        
        /// <summary>
        /// lblTitle-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String  lblTitle;
        
        /// <summary>
        /// Form1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm Form1;
        
        /// <summary>
        /// String1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String1;
        
        /// <summary>
        /// btnDe-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnDe;
        
        /// <summary>
        /// btnEn-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnEn;
        
        /// <summary>
        /// btnFr-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnFr;
        
        /// <summary>
        /// btnEs-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btnEs;
        
        /// <summary>
        /// String2-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String2;
        
        /// <summary>
        /// btn_back-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink btn_back;
        
        /// <summary>
        /// dgrAccessories-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataGrid dgrAccessories;
        
        /// <summary>
        /// space-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell space;
        
        /// <summary>
        /// tabRight-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableCell tabRight;
        
        /// <summary>
        /// Label1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// Panel1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;
        
        /// <summary>
        /// imgSymbol-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgSymbol;
        
        /// <summary>
        /// download-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable download;
        
        /// <summary>
        /// String8-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String8;
        
        /// <summary>
        /// Bestellangaben-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Bestellangaben;
        
        /// <summary>
        /// String5-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String5;
        
        /// <summary>
        /// Planung-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Planung;
        
        /// <summary>
        /// String10-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String10;
        
        /// <summary>
        /// Prospekte-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Prospekte;
        
        /// <summary>
        /// String13-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String13;
        
        /// <summary>
        /// String16-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String16;
        
        /// <summary>
        /// download_dfx-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink download_dfx;
        
        /// <summary>
        /// String20-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String20;
        
        /// <summary>
        /// download_eps-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink download_eps;
        
        /// <summary>
        /// String21-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String21;
        
        /// <summary>
        /// String23-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String23;
        
        /// <summary>
        /// viproom-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink viproom;
        
        /// <summary>
        /// String24-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String24;
        
        /// <summary>
        /// Preise-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink Preise;
        
        /// <summary>
        /// String25-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String25;
        
        /// <summary>
        /// systemDetail-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable systemDetail;
        
        /// <summary>
        /// lnkDetail-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkDetail;
        
        /// <summary>
        /// sysSpace-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable sysSpace;
        
        /// <summary>
        /// trRelSystem-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable trRelSystem;
        
        /// <summary>
        /// String6-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::Alpha.Controls.String String6;
        
        /// <summary>
        /// dgrRelevantSystem-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataGrid dgrRelevantSystem;
    }
}
